<?php

namespace Common;

use Illuminate\Support\Facades\File;

class UploadName
{
	public static function uploadFile($name, $request, $path) {
	        if ($request->hasFile($name)) {
	            $file = $request->file($name);
	            
	            //create import success directory
	            if (!File::exists(UPLOAD_FOLDER . $path)) {
	                File::makeDirectory(UPLOAD_FOLDER . $path, 0777, true, true);
	            }
	            // Upload file
	            $filename = $file->getClientOriginalName();
	            
	            $file->move(UPLOAD_FOLDER . $path, $filename);

	            return $filename;
	        }
	        
	        return '';
	    }

}
