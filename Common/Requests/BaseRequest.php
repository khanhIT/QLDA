<?php

namespace Common\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
    /**
     * Define path to folder json config
     */
    protected $path = PATH_TO_VALIDATE_FOLDER;
    /**
     * Define file name to folder json config
     */
    protected $file_name = '';
    /**
     * Define file content to folder json config
     */
    protected $file_content = '';
    /**
     * Define rules
     */
    protected $_rules = [];
    /**
     * Define message
     */
    protected $_message = [];
    

    public function __construct($fileName = null){
        if(!empty($fileName)){
            $this->file_name = $fileName;
        }
    }

        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function responseRules()
    {
        if(!empty($this->file_content)){
            foreach ($this->file_content as $key => $value){
                $this->_rules[$key] = '';
                if(!empty($value)){
                    foreach ($value as $vk => $rule){
                        if(!empty($rule['value'])){
                            $this->_rules[$key] .= $vk != 0 ? '|'. $rule['type'] . ':'. $rule['value'] : $rule['type'] . ':'. $rule['value'];
                        }else{
                            $this->_rules[$key] .= ($vk == 0 ? $rule['type'] : '|'.$rule['type']);
                        }
                    }
                }
            }
        }
        return $this->_rules;
    }
    
    public function responseMessages()
    {
        if(!empty($this->file_content)){
            foreach ($this->file_content as $key => $value){
                if(!empty($value)){
                    foreach ($value as $message){
                        $this->_message = array_merge($this->_message, [
                            $key.'.'.str_replace('nullable|','',$message['type']) => self::showMessage($message['message'], $message['name'], $message['value'])
                        ]);
                    }
                }
            }
        }
        return $this->_message;
    }
    
    public static function showMessage($message, $field1, $field2){
        if (!$message) {
            return false;
        }
        if (defined($message)) {
            return sprintf(constant($message), $field1, $field2);
        } else {
            return sprintf($message, $field1, $field2);
        }
    }

    public function readFileConfig($file) {
        if(file_exists($file)){
            return json_decode(file_get_contents($file), true);
        }
        return false;
    }

}
