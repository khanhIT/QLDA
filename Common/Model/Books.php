<?php
namespace Common\Model;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = 'book';
    protected $fillable = [ 'id','title','edition','author','publisher','year','isbn','abstract','cover','inside'];
    protected $date = [ 'created_at', 'updated_at' ];
}