<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class Custormer extends Model
{
    protected $table = 'custormer';

    protected $fillable = ['fullname', 'email', 'msv', 'phone', 'address', 'project_name','c_id', 's_id', 'l_id'];

    protected $date = ['birthday','created_at', 'updated_at'];


}
