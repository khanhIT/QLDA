<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class ClassName extends Model
{
    protected $table = 'class_name';
    protected $fillable = [ 'id','c_name','description'];
    protected $date = [ 'created_at', 'updated_at' ];


    public function Custormer()

    {
        return $this->hasMany(Custormer::class);
    }

}
