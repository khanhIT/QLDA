<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class CadresRead extends Model
{
    protected $table = 'cadres_read';
}
