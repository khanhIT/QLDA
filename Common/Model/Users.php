<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class Users extends Authenticatable
{
	use Notifiable;
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = ['u_fullname', 'u_name','u_email','address','mobile','skype','avatar','gender','married','password','role_id','birthday'];
    protected $hidden =['password', 'remember_token'];
    protected $dates = ['created_at', 'updated_at','birthday'];
}
