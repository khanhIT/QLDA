<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';

    protected $fillable = ['cus_id','c_id', 's_id', 'l_id', 'name_project', 'file_upload_word', 'upload_source_code', 'point', 'status', 'group','cadres_read'];

    protected $date = [ 'created_at', 'updated_at' ];
}
