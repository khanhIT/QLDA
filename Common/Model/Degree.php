<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    protected $table = 'degree';
    protected $fillable = ['id','name'];
    protected $date = [ 'created_at', 'updated_at' ];
}
