<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    protected $fillable = [ 'id','name','email','address','content','status','reply'];
}
