<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Slide extends Model
{
    protected $table = 'slide';
    protected $fillable = ['name', 'image'];
    protected $date = [ 'created_at', 'updated_at' ];

}
