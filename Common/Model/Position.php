<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'position';
    protected $fillable = ['id','name'];
    protected $date = [ 'created_at', 'updated_at' ];
}
