<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class EmailRegister extends Model
{
    protected $table = 'email_register';
    protected $fillable = ['id','email'];
    protected $date = [ 'created_at', 'updated_at' ];
}
