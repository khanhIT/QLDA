<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class MemorialPhoto extends Model
{
    protected $table = 'memorial_photo';
    protected $fillable = ['name', 'image'];
    protected $date = [ 'created_at', 'updated_at' ];
}
