<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class Lecturers extends Model
{
    protected $table = 'lecturers';
      protected $fillable = ['id', 'l_fullname', 'l_username','l_email','l_address','l_phone','p_id','l_birthday','image','gender','marride','d_id','staff','l_address','info', 'link_url'];
    protected $date = [ 'created_at', 'updated_at', 'l_birthday' ];
}
