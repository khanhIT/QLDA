<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
    protected $table = 'school_year';
    protected $fillable = [ 'id', 's_key', 'year_start', 'year_end', 'total_student'];
    protected $date = [ 'created_at', 'updated_at' ];
}
