<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';
    protected $fillable = ['title', 'content', 'image', 'image_content'];
    protected $date = [ 'created_at', 'updated_at' ];
}
