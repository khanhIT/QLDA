<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class ListProject extends Model
{
    protected $table = 'list_project';
    protected $fillable = ['l_id', 'name','del_flag'];
    protected $date = ['expried_at', 'created_at', 'updated_at'];
}
