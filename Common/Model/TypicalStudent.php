<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class TypicalStudent extends Model
{
    protected $table = 'typical_student';
    protected $fillable = ['name', 'info', 'image', 'link_url'];
    protected $date = [ 'created_at', 'updated_at' ];
}
