<?php
namespace Common\Repositories\Project;

use Common\Model\Project;
use Common\Model\Lecturers;
use Common\Model\Custormer;
use Common\Model\ClassName;
use Common\Model\SchoolYear;
use Common\Repositories\BaseRepository;
use DB;

class ProjectRepository extends BaseRepository
{

    public function getModel()
    {
        return Project::class;
    }

    public function listIndex()
    {
        return $this->model
            ->leftJoin('custormer','project.cus_id','=','custormer.id')
            ->leftJoin('class_name','project.c_id','=','class_name.id')
            ->leftJoin('lecturers','project.l_id','=','lecturers.id')
            ->leftJoin('school_year','project.s_id','=','school_year.id')
            ->leftJoin('list_project','project.name_project','=','list_project.id')
            ->select('project.*','custormer.fullname','lecturers.l_fullname','class_name.c_name','school_year.s_key', 'list_project.name')
            ->orderBy('id','desc')
            ->get();
    }

    public function listShow($id)
    {
        return $this->model
            ->leftJoin('custormer','project.cus_id','=','custormer.id')
            ->leftJoin('class_name','project.c_id','=','class_name.id')
            ->leftJoin('lecturers','project.l_id','=','lecturers.id')
            ->leftJoin('school_year','project.s_id','=','school_year.id')
            ->leftJoin('list_project','project.name_project','=','list_project.id')
            ->where('project.id', $id)
            ->select('project.*','custormer.msv', 'custormer.fullname','lecturers.l_fullname','class_name.c_name','school_year.s_key', 'list_project.name')
            ->first();
    }

    public function listEdit($id)
    {
        return $this->model
            ->leftJoin('custormer','project.cus_id','=','custormer.id')
            ->leftJoin('list_project','project.name_project','=','list_project.id')
            ->where('project.id', $id)
            ->select('project.*','custormer.fullname','list_project.name')
            ->first();
    }

    public function view($id)
    {
        return $this->model
            ->where('project.id', $id)
            ->select('project.file_upload_word')
            ->get();
    }

    public function listProjectNew(){
        return $this->model
            ->leftJoin('custormer','project.cus_id','=','custormer.id')
            ->leftJoin('class_name','project.c_id','=','class_name.id')
            ->leftJoin('lecturers','project.l_id','=','lecturers.id')
            ->leftJoin('school_year','project.s_id','=','school_year.id')
            ->leftJoin('list_project','project.name_project','=','list_project.id')
            ->where('project.status','=',0)
            ->select('project.*','custormer.fullname','lecturers.l_fullname','class_name.c_name','school_year.s_key','list_project.name as pro_name')
            ->orderBy('id','DESC')
            ->paginate(20);
    }
    public function listProject(){
        return $this->model
            ->leftJoin('custormer','project.cus_id','=','custormer.id')
            ->leftJoin('cadres_read','custormer.id','=','cadres_read.cus_id')
            ->leftJoin('class_name','project.c_id','=','class_name.id')
            ->leftJoin('lecturers','project.l_id','=','lecturers.id')
            ->leftJoin('school_year','project.s_id','=','school_year.id')
            ->leftJoin('list_project','project.name_project','=','list_project.id')
            ->where('project.status','=',1)
            ->select('project.*','custormer.fullname','lecturers.l_fullname','class_name.c_name','school_year.s_key','cadres_read.name','list_project.name as pro_name')
            ->orderBy('id','DESC')
            ->paginate(20);
    }
    public function detailProject($id){
        return $this->model
            ->leftJoin('custormer','project.cus_id','=','custormer.id')
            ->leftJoin('cadres_read','custormer.id','=','cadres_read.cus_id')
            ->leftJoin('class_name','project.c_id','=','class_name.id')
            ->leftJoin('lecturers','project.l_id','=','lecturers.id')
            ->leftJoin('school_year','project.s_id','=','school_year.id')
            ->leftJoin('list_project','project.name_project','=','list_project.id')
            ->where('project.status','=',1)
            ->where('project.id',$id)
            ->select('project.*','custormer.fullname','custormer.msv','lecturers.l_fullname','class_name.c_name','school_year.s_key','cadres_read.name','list_project.name as pro_name')
            ->first();
    }

    public function exportHĐ1()
    {
        return $this->model
               ->leftJoin('custormer','project.cus_id', '=', 'custormer.id')
               ->leftJoin('lecturers','project.l_id','=','lecturers.id')
               ->where('project.group', '=', 1)
                ->whereYear('project.updated_at', '=', date('Y'))
               ->select('custormer.msv as Mã sinh viên',
                        'custormer.fullname as Sinh viên thực hiện',
                        'project.name_project as Tên đề tài',
                        'lecturers.l_fullname as Giảng viên hướng dẫn',
                        'project.cadres_read as Giáo viên phản biện',
                        'project.group as Hội đồng'
                        )

               ->limit(15)
               ->get();
    }

    public function exportHĐ2()
    {
        return $this->model
               ->leftJoin('custormer','project.cus_id', '=', 'custormer.id')
               ->leftJoin('lecturers','project.l_id','=','lecturers.id')
               ->where('project.group', '=', 2)
                ->whereYear('project.updated_at', '=', date('Y'))
               ->select('custormer.msv as Mã sinh viên',
                        'custormer.fullname as Sinh viên thực hiện',
                        'project.name_project as Tên đề tài',
                        'lecturers.l_fullname as Giảng viên hướng dẫn',
                        'project.cadres_read as Giáo viên phản biện',
                        'project.group as Hội đồng'
                        )
               ->limit(15)
               ->get();
    }
    public function exportHĐ3()
    {
        return $this->model
            ->leftJoin('custormer','project.cus_id', '=', 'custormer.id')
            ->leftJoin('lecturers','project.l_id','=','lecturers.id')
            ->where('project.group', '=', 3)
            ->whereYear('project.updated_at', '=', date('Y'))
            ->select('custormer.msv as Mã sinh viên',
                'custormer.fullname as Sinh viên thực hiện',
                'project.name_project as Tên đề tài',
                'lecturers.l_fullname as Giảng viên hướng dẫn',
                'project.cadres_read as Giáo viên phản biện',
                'project.group as Hội đồng'
            )
            ->limit(15)
            ->get();
    }

    public function exportHĐ4()
    {
        return $this->model
            ->leftJoin('custormer','project.cus_id', '=', 'custormer.id')
            ->leftJoin('lecturers','project.l_id','=','lecturers.id')
            ->where('project.group', '=', 4)
            ->whereYear('project.updated_at', '=', date('Y'))
            ->select('custormer.msv as Mã sinh viên',
                'custormer.fullname as Sinh viên thực hiện',
                'project.name_project as Tên đề tài',
                'lecturers.l_fullname as Giảng viên hướng dẫn',
                'project.cadres_read as Giáo viên phản biện',
                'project.group as Hội đồng'
            )
            ->limit(15)
            ->get();
    }

    public function exportHĐ5()
    {
        return $this->model
            ->leftJoin('custormer','project.cus_id', '=', 'custormer.id')
            ->leftJoin('lecturers','project.l_id','=','lecturers.id')
            ->where('project.group', '=', 5)
            ->whereYear('project.updated_at', '=', date('Y'))
            ->select('custormer.msv as Mã sinh viên',
                'custormer.fullname as Sinh viên thực hiện',
                'project.name_project as Tên đề tài',
                'lecturers.l_fullname as Giảng viên hướng dẫn',
                'project.cadres_read as Giáo viên phản biện',
                'project.group as Hội đồng'
            )
            ->limit(15)
            ->get();
    }

    public function searchQuery($request)
    {
        $name = $request->name_project;
        $result = $this->model->leftJoin('custormer', 'project.cus_id', '=', 'custormer.id')
            ->leftJoin('cadres_read', 'custormer.id', '=', 'cadres_read.cus_id')
            ->leftJoin('class_name', 'project.c_id', '=', 'class_name.id')
            ->leftJoin('lecturers', 'project.l_id', '=', 'lecturers.id')
            ->leftJoin('school_year', 'project.s_id', '=', 'school_year.id')
            ->leftJoin('list_project','project.name_project','=','list_project.id')
            ->where('project.status', '=', 0)
            ->select('project.*', 'custormer.fullname', 'class_name.c_name', 'lecturers.l_fullname', 'school_year.s_key', 'cadres_read.name','list_project.name as pro_name');
        if (isset($request->name_project) && !empty($request->name_project)) {
            $result->Where('project.name_project', 'like', '%' . $name . '%');
            $result->orWhere('custormer.fullname', 'like', '%' . $name . '%');
            $result->orWhere('lecturers.l_fullname', 'like', '%' . $name . '%');
            $result->orWhere('class_name.c_name', 'like', '%' . $name . '%');
            $result->orWhere('school_year.s_key', 'like', '%' . $name . '%');
            $result->orWhere('cadres_read.name', 'like', '%' . $name . '%');
            $result->orWhere('list_project.name', 'like', '%' . $name . '%');
        }
        if (isset($request->lecturers) && !empty($request->lecturers)) {
            $result->where('project.l_id', $request->lecturers);
            return $result->paginate(10);
        }
        if (isset($request->Lop) && !empty($request->Lop)) {
            $result->where('project.c_id', $request->Lop);
            return $result->paginate(10);
        }
        if (isset($request->khoa) && !empty($request->khoa)) {
            $result->where('project.s_id', $request->khoa);
            return $result->paginate(10);
        }
        if (isset($request->CBDC) && !empty($request->CBDC)) {
            $result->where('cadres_read.id', $request->CBDC);
            return $result->paginate(10);
        }

        return $result->paginate(10);
    }
        public function searchQueryProject($request){
            $name=$request->name_project;
            $result = $this->model->leftJoin('custormer','project.cus_id','=','custormer.id')
                ->leftJoin('cadres_read','custormer.id','=','cadres_read.cus_id')
                ->leftJoin('class_name','project.c_id','=','class_name.id')
                ->leftJoin('lecturers','project.l_id','=','lecturers.id')
                ->leftJoin('school_year','project.s_id','=','school_year.id')
                ->leftJoin('list_project','project.name_project','=','list_project.id')
                ->where('project.status','=',1)
                ->select('project.*','custormer.fullname','class_name.c_name','lecturers.l_fullname','school_year.s_key','cadres_read.name','list_project.name as pro_name');
            if(isset($request->name_project) && !empty($request->name_project)) {
                $result->Where('project.name_project', 'like', '%' . $name . '%');
                $result->orWhere('custormer.fullname', 'like', '%' . $name . '%');
                $result->orWhere('lecturers.l_fullname', 'like', '%' . $name . '%');
                $result->orWhere('class_name.c_name', 'like', '%' . $name . '%');
                $result->orWhere('school_year.s_key', 'like', '%' . $name . '%');
                $result->orWhere('project.group', 'like', '%' . $name . '%');
                $result->orWhere('list_project.name', 'like', '%' . $name . '%');
        }
            if(isset($request->lecturers) && !empty($request->lecturers)) {
                $result->where('project.l_id', $request->lecturers);
                return $result->paginate(10);
            }
            if(isset($request->nameRead) && !empty($request->nameRead)) {
                $result->where('project.cadres_read','like','%' . $request->nameRead . '%');
                return $result->paginate(10);
            }
            if(isset($request->Lop) && !empty($request->Lop)) {
                $result->where('project.c_id', $request->Lop);
                return $result->paginate(10);
            }
            if(isset($request->khoa) && !empty($request->khoa)) {
                $result->where('project.s_id', $request->khoa);
                return $result->paginate(10);
            }
            if (!empty($request->group)) {
                switch ($request->group) {
                    case '1':
                        $result->where('project.group','=',1);
                        break;
                    case '2':
                        $result->where('project.group','=',2);
                        break;
                    case '3':
                        $result->where('project.group','=',3);
                        break;
                    case '4':
                        $result->where('project.group','=',4);
                        break;
                }
                return $result->paginate(10);
            }
            return $result->paginate(10);

    }


    public function getCountPass()
    {
        return $this->model
               ->where('project.point', '>=', 5)
               ->select('project.*')
               ->count();
    }

    public function getCountNoPass()
    {
        return $this->model
               ->where('project.point', '<', 5)
               ->select('project.*')
               ->count();
    }

}