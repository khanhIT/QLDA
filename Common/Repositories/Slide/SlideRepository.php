<?php
namespace Common\Repositories\Slide;

use Common\Model\Slide;
use Common\Repositories\BaseRepository;

class SlideRepository extends BaseRepository
{

    public function getModel()
    {
        return Slide::class;
    }
}