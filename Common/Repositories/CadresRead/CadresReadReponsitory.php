<?php
namespace Common\Repositories\CadresRead;

use Common\Model\CadresRead;
use Common\Repositories\BaseRepository;
use Yajra\DataTables\Facades\DataTables;
class CadresReadReponsitory extends BaseRepository
{

    public function getModel()
    {
        return CadresRead::class;
    }
}