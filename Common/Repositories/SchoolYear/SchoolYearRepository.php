<?php
namespace Common\Repositories\SchoolYear;

use Common\Model\SchoolYear;
use Common\Repositories\BaseRepository;
use DB;

class SchoolYearRepository extends BaseRepository
{

    public function getModel()
    {
        return SchoolYear::class;
    }

    public function listIndex()
    {
    	return $this->model
    	       ->select('school_year.*',
    	       	DB::raw('DATE_FORMAT(school_year.year_start, "%d-%m-%Y") as year_start'),
    	       	DB::raw('DATE_FORMAT(school_year.year_end, "%d-%m-%Y") as year_end')
    	       )
               ->orderBy('id','desc')
               ->get();
    }
}