<?php
namespace Common\Repositories\Custormer;

use Common\Model\Custormer;
use Common\Model\ClassName;
use Common\Model\SchoolYear;
use Common\Model\Lecturers;
use Common\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use DateTime;

class CustormerRepository extends BaseRepository
{

    public function getModel()
    {
        return Custormer::class;
    }

    public function listIndex()
    {
         return $this->model
            ->leftJoin('class_name','custormer.c_id','=','class_name.id')
            ->leftJoin('lecturers','custormer.l_id','=','lecturers.id')
            ->leftJoin('school_year','custormer.s_id','=','school_year.id')
            ->leftJoin('list_project','custormer.project_name','=','list_project.id')
            ->select('custormer.*','lecturers.l_fullname','class_name.c_name','school_year.s_key','list_project.name')
            ->orderBy('id','desc')
            ->get();   
    }

    public function listExport()
    {
        return $this->model
            ->leftJoin('class_name','custormer.c_id','=','class_name.id')
            ->leftJoin('lecturers','custormer.l_id','=','lecturers.id')
            ->leftJoin('school_year','custormer.s_id','=','school_year.id')
            ->select('custormer.id as STT',
                'custormer.msv as Mã sinh viên',
                'custormer.fullname as Sinh viên thực hiện',
                'custormer.phone as Số điện thoại',
                'custormer.address as Địa chỉ','custormer.email as Email',
                'custormer.project_name as Tên đề tài',
                'lecturers.l_fullname as Giảng viên hướng dẫn',
                'class_name.c_name as Lớp chuyên ngành',
                'school_year.s_key as Khóa học'
            )
            ->get();
    }

    public function listView($id)
    {
    	return $this->model
            ->leftJoin('list_project','custormer.project_name','=','list_project.id')  
            ->where('custormer.id', $id)
            ->select('custormer.*', 'list_project.name')
            ->first();
    }
    public function joinLecturers($request)
    {
        return $this->model->leftJoin('lecturers', 'custormer.l_id', '=', 'lecturers.id')
            ->leftJoin('class_name', 'custormer.c_id', '=', 'class_name.id')
            ->leftJoin('school_year', 'custormer.s_id', '=', 'school_year.id')
            ->leftJoin('list_project', 'custormer.project_name', '=', 'list_project.id')
            ->leftJoin('project', 'custormer.id', '=', 'project.cus_id')
            ->where('custormer.l_id', '=', $request->l_id)
            ->where('custormer.msv', '=', $request->msv)
            ->select('custormer.*', 'lecturers.l_email', 'lecturers.l_fullname', 'lecturers.l_phone', 'class_name.c_name', 'school_year.s_key', 'list_project.name')
            ->first();
    }
    public function listShow($id)
    {
        return $this->model
            ->leftJoin('class_name','custormer.c_id','=','class_name.id')
            ->leftJoin('lecturers','custormer.l_id','=','lecturers.id')
            ->leftJoin('school_year','custormer.s_id','=','school_year.id')
            ->leftJoin('list_project','custormer.project_name','=','list_project.id')  
            ->where('custormer.id', $id)
            ->select('custormer.*','lecturers.l_fullname','class_name.c_name','school_year.s_key','list_project.name')
            ->first();
    }
    public function checkCreate($msv){
        return $this->model
            ->where('msv',$msv)
            ->first();
    }
    public function checkEmail($email){
        return $this->model
            ->where('email',$email)
            ->first();
    }
    public function checkProject($project){
        return $this->model
            ->where('project_name',$project)
            ->first();
    }
    public function checkLecturers($lecturers){
        return $this->model
            ->where('l_id',$lecturers)
            ->get();
    }

    public function checkEmailCustomer($email){
        return $this->model
            ->leftJoin('lecturers','lecturers.id','=','custormer.l_id')
            ->leftJoin('list_project','list_project.id','=','custormer.project_name')
            ->where('email',$email)
            ->select('custormer.*','lecturers.l_email','lecturers.l_fullname','list_project.name as pro_name')
            ->first();
    }

    private function searchLikeByKeyword($tblName, $column, $keyword, $limit) {
        $result = DB::table($tblName)
                ->where($column, 'LIKE', '%'.$keyword.'%');
    
        if ($limit == 1)
            $result = $result->first();
        elseif ($limit > 1)
            $result = $result->paginate($limit);
        else
            $result = $result->get();
        return $result;
    }

    private function getIdByKeyword($tblName, $column, $keyword) {
        if($keyword != '') {
            $tblData = self::searchLikeByKeyword($tblName, $column, $keyword, 1);
            if(count($tblData) > 0)
                $cityId = $tblData->id;
            else 
                $cityId = 0;
        } else {
            $cityId = 0;
        }
        return $cityId;
    }
    

    public function importFromExcelCustormer($excelData, $filename)    
    {
        $dataInsert = [];
        $totalRow = 0;
        $countRowInsertSuccess = 0;
        if(count($excelData))
        {
            foreach ($excelData as $data) {
                $totalRow++;

                $class = self::getIdByKeyword(\TblName::CLASS_NAME, 
                    'c_name', $data['class']);

                $school = self::getIdByKeyword(\TblName::SCHOOL_YEAR, 
                    's_key', $data['school']);

                $lecturers = self::getIdByKeyword(\TblName::LECTURERS, 
                    'l_fullname', $data['lecturers']);

                $custormer = new Custormer;
                $custormer->MSV = $data['msv'];
                $custormer->c_id = $class;
                $custormer->s_id = $school;
                $custormer->l_id = $lecturers;
                $custormer->fullname = $data['fullname'];
                $custormer->phone = $data['phone'];
                $custormer->address = $data['address'];
                $custormer->email = $data['email'];
                $custormer->project_name = $data['project_name'];
                $custormer->birthday = date_format(new DateTime($data['birthday']),'d-m-Y');
                $custormer->save();

                $countRowInsertSuccess++;
            }
        }
        $result = [

            'totalRow' => $totalRow,
            'countRowInsertSuccess' => $countRowInsertSuccess,
        ];
        return $result;
    }
}