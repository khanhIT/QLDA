<?php
namespace Common\Repositories\MemorialPhoto;

use Common\Model\MemorialPhoto;
use Common\Repositories\BaseRepository;

class MemorialPhotoRepository extends BaseRepository
{

    public function getModel()
    {
        return MemorialPhoto::class;
    }
}