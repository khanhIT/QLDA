<?php
namespace Common\Repositories\Books;

use Common\Model\Books;
use Common\Repositories\BaseRepository;
use Yajra\DataTables\Facades\DataTables;
class BooksResponsitory extends BaseRepository
{

    public function getModel()
    {
        return Books::class;
    }
    public function searchQuery($request)
    {
        $name = $request->nameBooks;
        if (isset($request->nameBooks) && !empty($request->nameBooks)) {
        $result = $this->model->select('book.*');
            $result->Where('book.title', 'like', '%' . $name . '%');
            $result->orWhere('book.author', 'like', '%' . $name . '%');
            $result->orWhere('book.year', 'like', '%' . $name . '%');
            $result->orWhere('book.edition', 'like', '%' . $name . '%');
            return $result->paginate(12);
        }

    }
}