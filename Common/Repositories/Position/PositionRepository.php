<?php
namespace Common\Repositories\Position;

use Common\Model\Position;
use Common\Repositories\BaseRepository;

class PositionRepository extends BaseRepository
{

    public function getModel()
    {
        return Position::class;
    }
}