<?php
namespace Common\Repositories\Role;

use Common\Model\Role;
use Common\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{

    public function getModel()
    {
        return Role::class;
    }
}