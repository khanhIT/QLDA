<?php
namespace Common\Repositories\Lecturers;

use Common\Model\Lecturers;
use Common\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;

class LecturersRepository extends BaseRepository
{

    public function getModel()
    {
        return Lecturers::class;
    }
    public function getManyLecturerMaster(){
        return $this->model
                    ->leftJoin('degree','lecturers.d_id','=','degree.id')
                    ->leftJoin('position','lecturers.p_id','=','position.id')
                    ->where('status',1)
                    ->select('lecturers.*','degree.name','position.name as p_name')
                    ->orderBy('id','asc')
                    ->get();
    }
    public function getManyLecturers(){
        return $this->model
            ->leftJoin('degree','lecturers.d_id','=','degree.id')
            ->leftJoin('position','lecturers.p_id','=','position.id')
            ->where('status',0)
            ->select('lecturers.*','degree.name','position.name as p_name')
            ->orderBy('id','asc')
            ->get();
    }
    public function getManyIntroduceLecturers(){
        return $this->model
            ->leftJoin('degree','lecturers.d_id','=','degree.id')
            ->leftJoin('position','lecturers.p_id','=','position.id')
            ->where('status',0)
            ->select('lecturers.*','degree.name','position.name as p_name')
            ->orderBy('id','desc')
            ->get();
    }

    public function getShow($id)
    {
        return $this->model
               ->leftJoin('position','lecturers.p_id','=','position.id')
               ->leftJoin('degree','lecturers.d_id','=','degree.id')
               ->where('lecturers.id',$id)
               ->select('lecturers.*','position.name as p_name','degree.name as d_name',
                         DB::raw('DATE_FORMAT(lecturers.l_birthday, "%d-%m-%Y") as l_birthday')
                )
               ->first();
    }

}