<?php
namespace Common\Repositories\ListProject;

use Common\Model\ListProject;
use Common\Model\Lecturers;
use Common\Repositories\BaseRepository;
use DB;

class ListProjectRepository extends BaseRepository
{

    public function getModel()
    {
        return ListProject::class;
    }

    public function getIndex()
    {
    	return $this->model
            ->leftJoin('lecturers','list_project.l_id','=','lecturers.id')
            ->select('list_project.*','lecturers.l_fullname')
            ->orderBy('id','desc')
            ->get();
    }
    public function listProjectName()
    {
        return $this->model
            ->leftJoin('lecturers','list_project.l_id','=','lecturers.id')
            ->select('list_project.*','lecturers.l_fullname')
            ->where('list_project.status',0)
            ->orderBy('id','desc')
            ->paginate(15);
    }
    public function listProjectNameSale()
    {
        return $this->model
            ->leftJoin('lecturers','list_project.l_id','=','lecturers.id')
            ->select('list_project.*','lecturers.l_fullname')
            ->where('list_project.status',1)
            ->orderBy('id','desc')
            ->paginate(15);
    }
    public function listProjectNameSaleFirst($id)
    {
        return $this->model
            ->leftJoin('lecturers','list_project.l_id','=','lecturers.id')
            ->select('list_project.*','lecturers.l_fullname')
            ->where('list_project.del_flag',0)
            ->where('list_project.l_id',$id)
            ->get();
    }
    public function listProjectNameFirstCheck($id)
    {
        return $this->model
            ->leftJoin('lecturers','list_project.l_id','=','lecturers.id')
            ->select('list_project.*','lecturers.l_fullname')
            ->where('list_project.del_flag',0)
            ->where('list_project.l_id',$id)
            ->orWhere('lecturers.id',$id)
            ->first();
    }
    public function listProjectNameDelFlag1($id)
    {
        return $this->model
            ->leftJoin('lecturers','list_project.l_id','=','lecturers.id')
            ->select('list_project.*','lecturers.l_fullname')
            ->where('list_project.del_flag',1)
            ->where('list_project.l_id',$id)
            ->first();
    }
    public function checkListProject($id)
    {
        return $this->model
            ->where('l_id',$id)
            ->where('del_flag',1)
            ->get();
    }
    public function listProjectNameFirst($id)
    {
        return $this->model
            ->leftJoin('lecturers','list_project.l_id','=','lecturers.id')
            ->select('list_project.*','lecturers.l_fullname')
            ->where('list_project.id',$id)
            ->first();
    }
    public function getManyWhereProject(){
        return $this->model
            ->where('list_project.del_flag',0)
//            ->where('list_project.status',0)
            ->get();
    }

    private function searchLikeByKeyword($tblName, $column, $keyword, $limit) {
        $result = DB::table($tblName)
                ->where($column, 'LIKE', '%'.$keyword.'%');
    
        if ($limit == 1)
            $result = $result->first();
        elseif ($limit > 1)
            $result = $result->paginate($limit);
        else
            $result = $result->get();
        return $result;
    }

    private function getIdByKeyword($tblName, $column, $keyword) {
        if($keyword != '') {
            $tblData = self::searchLikeByKeyword($tblName, $column, $keyword, 1);
            if(count($tblData) > 0)
                $cityId = $tblData->id;
            else 
                $cityId = 0;
        } else {
            $cityId = 0;
        }
        return $cityId;
    }

    public function importFromExcelListProject($excelData, $filename)    
    {
        $dataInsert = [];
        $totalRow = 0;
        $countRowInsertSuccess = 0;
        if(!empty($excelData))
        {
            foreach ($excelData as $data) {
                $totalRow++;
                
                $lecturers = self::getIdByKeyword(\TblName::LECTURERS, 
                    'l_fullname', $data['lecturers']);

                $list_project = new ListProject;
                $list_project->l_id = $lecturers;
                $list_project->name = $data['name'];
                $list_project->description = $data['description'];
                $list_project->save();

                $countRowInsertSuccess++;
            }
        }
        $result = [

            'totalRow' => $totalRow,
            'countRowInsertSuccess' => $countRowInsertSuccess,
        ];
        return $result;
    } 

}