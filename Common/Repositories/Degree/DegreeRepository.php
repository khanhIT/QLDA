<?php
namespace Common\Repositories\Degree;

use Common\Model\Degree;
use Common\Repositories\BaseRepository;

class DegreeRepository extends BaseRepository
{

    public function getModel()
    {
        return Degree::class;
    }
}