<?php
namespace Common\Repositories\News;

use Common\Model\News;
use Common\Repositories\BaseRepository;

class NewsRepository extends BaseRepository
{

    public function getModel()
    {
        return News::class;
    }
}