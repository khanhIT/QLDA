<?php
namespace Common\Repositories\ClassName;

use Common\Model\ClassName;
use Common\Repositories\BaseRepository;

class ClassNameRepository extends BaseRepository
{

    public function getModel()
    {
        return ClassName::class;
    }
}