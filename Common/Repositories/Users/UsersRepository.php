<?php
namespace Common\Repositories\Users;

use Common\Model\Users;
use Common\Repositories\BaseRepository;

class UsersRepository extends BaseRepository
{

    public function getModel()
    {
        return Users::class;
    }


    public function getRole()
    {
    	return $this->model
    	       ->leftjoin('role','users.role_id', '=', 'role.id')
    	       ->select('users.*','role.role_name')
    	       ->paginate(5);
    }
    
}