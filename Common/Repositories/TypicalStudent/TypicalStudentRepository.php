<?php
namespace Common\Repositories\TypicalStudent;

use Common\Model\TypicalStudent;
use Common\Repositories\BaseRepository;

class TypicalStudentRepository extends BaseRepository
{

    public function getModel()
    {
        return TypicalStudent::class;
    }
}