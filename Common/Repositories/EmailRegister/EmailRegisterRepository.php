<?php
namespace Common\Repositories\EmailRegister;
use Common\Model\EmailRegister;
use Common\Repositories\BaseRepository;

class EmailRegisterRepository extends BaseRepository
{

    public function getModel()
    {
        return EmailRegister::class;
    }
    public function checkEmail($email){
        return $this->model
            ->where('email',$email)
            ->first();
    }
}