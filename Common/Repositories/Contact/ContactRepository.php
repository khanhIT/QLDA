<?php
namespace Common\Repositories\Contact;

use Common\Model\Contact;
use Common\Repositories\BaseRepository;

class ContactRepository extends BaseRepository
{

    public function getModel()
    {
        return Contact::class;
    }
}