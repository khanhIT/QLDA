<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Frontend\Http\Controllers'], function()
{
    Route::get('/Trang-chu', 'FrontendController@index')->name('index');
    Route::post('/Trang-chu', 'FrontendController@create');
    Route::get('/register', 'RegisterController@index')->name('register');
    Route::get('do-an-moi-dang-ky', 'ProjectController@news')->name('project-new');
    Route::get('do-an', 'ProjectController@index')->name('project');
    Route::get('/do-an/chi-tiet/{id}', 'ProjectController@show')->name('project-detail');
    Route::get('/danh-sach-de-tai/', 'ProjectController@listProject')->name('project-list');
    Route::get('/danh-sach-de-tai/chi-tiet/{id}', 'ProjectController@listProjectDetail')->name('project-list-detail');
    //Phan route tin tuc
    Route::get('tin-tuc', 'NewsController@index')->name('news');
    Route::get('/tin-tuc/chi-tiet/{id}', 'NewsController@show')->name('news-detail');
    Route::get('/lien-he', 'ContactController@index')->name('contact');
    Route::post('lien-he', 'ContactController@create');
    Route::get('/books', 'BookController@index')->name('books');
    Route::get('/books/detail/{id}', 'BookController@show')->name('viewBook');
    Route::get('/doi-ngu-can-bo', 'IntroduceController@lecturers')->name('lecturers');
    Route::post('/doi-ngu-can-bo', 'IntroduceController@create');

    //đăng ký đồ án
    Route::get('/dang-ky-do-an', 'RegisterController@index')->name('register');
    Route::post('/dang-ky-do-an', 'RegisterController@create');
    Route::get('/name/{id}', 'RegisterController@name')->name('name');
});
