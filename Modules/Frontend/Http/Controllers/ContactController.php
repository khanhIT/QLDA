<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Frontend\Http\Requests\ContactRequest;
use Common\Repositories\Contact\ContactRepository;
use Common\Repositories\Custormer\CustormerRepository;
use Mail;
class ContactController extends Controller
{
    public function __construct(
        ContactRepository $contact,
        CustormerRepository $customer
    ){
        $this->contact = $contact;
        $this->customer=$customer;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('frontend::contact');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(ContactRequest $request)
    { $contact =
        [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'content' => $request->noidung,
            'status'=>0

        ];
        $check=$this->customer->checkEmailCustomer($request->email);
        if ($this->contact->create($contact)){
            if(!empty($check)){
                $data=[
                    'data'=>$check,
                    'data1'=>$request->noidung
                ];
                $email=$check->l_email;
                Mail::send(['html' => 'frontend.mail.MailCustomerContact'], $data, function($message) use($email) {
                    $message->to($email)
                        ->subject('Thông tin sinh viên liên hệ');
                    $message->from($email,'Bộ Môn Tin Trắc Địa');
                });
            }
            return redirect()->route('contact')->with(['info' => sprintf(MSG030,'')])->withInput();
        }else{
            return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */

    /**
     * Show the specified resource.
     * @return Response
     */

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('frontend::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
