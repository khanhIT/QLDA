<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Books\BooksResponsitory;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct(BooksResponsitory $books){
        $this->books = $books;
    }
    public function index(Request $request)
    {
        $listBooks=$this->books->paginate(12);
        if(!empty($request->nameBooks)){
            $listBooks=$this->books->searchQuery($request);
        }
        return view('frontend::books',compact('listBooks'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('frontend::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $viewBooks=$this->books->findBy('id',$id);
        return view('frontend::book_detail',compact('viewBooks'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('frontend::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
