<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Frontend\Http\Requests\RegisterRequest;
use Common\Repositories\Custormer\CustormerRepository;
use Common\Repositories\Project\ProjectRepository;
use Common\Repositories\Lecturers\LecturersRepository;
use Common\Repositories\ClassName\ClassNameRepository;
use Common\Repositories\SchoolYear\SchoolYearRepository;
use Common\Repositories\ListProject\ListProjectRepository;
use Common\Model\Custormer;
use Common\Model\Project;
use Illuminate\Support\Facades\Mail;
use DateTime;

class RegisterController extends Controller
{
    public function __construct(

           CustormerRepository $cus,
           ProjectRepository $project,
           LecturersRepository  $lecturers,
           ClassNameRepository $className,
           SchoolYearRepository $schoolYear,
           ListProjectRepository $lisProject
    ){
           $this->cus = $cus;
           $this->project = $project;
            $this->lecturers = $lecturers;
            $this->className = $className;
            $this->schoolYear = $schoolYear;
            $this->listProject = $lisProject;

    }

    public function index()
    {
        $dataSchoolYear=$this->schoolYear->all('id asc');
        $dataListProject=$this->listProject->getManyWhereProject();
        $dataLecturers=$this->lecturers->getManyWhereOrder(['staff'=>1],['*'],'id asc');
        $dataClassName=$this->className->all('id asc');
        return view('frontend::Register',compact('dataClassName','dataSchoolYear','dataLecturers','dataListProject'));
    }

    
    public function create(RegisterRequest $request)
    {

        $cus = new Custormer;

            $cus->fullname = $request->fullname;

            $cus->email = $request->email;

            $cus->msv = $request->msv;

            $cus->phone = $request->phone;

            $cus->birthday = date_format(new DateTime($request->birthday),'d-m-Y');

            $cus->address = $request->address;

            $cus->project_name = $request->project_name;

            $cus->c_id = $request->c_id;

            $cus->s_id = $request->s_id;

            $cus->l_id = $request->l_id;
            $checkMSV=$this->cus->checkCreate($request->msv);
            $checkGV=$this->cus->checkLecturers($request->l_id);
            $checkEmail=$this->cus->checkEmail($request->email);
            $checkProject=$this->cus->checkProject($request->project_name);
            if(!empty($checkMSV)){
                return redirect()->back()->with(['msv_error' => sprintf(MSG011,'Mã số sinh viên')])->withInput();
            }
            if(!empty($checkEmail)){
                return redirect()->back()->with(['email_error' => sprintf(MSG011,'Email')])->withInput();
            }
            if(!empty($checkProject)){
                return redirect()->back()->with(['project_error' => sprintf(MSG011,'Đồ án ')])->withInput();
            }
            if(count($checkGV)>=5){
                return redirect()->back()->with(['gv_error' => sprintf('Giáo viên đã đủ sinh viên hướng dẫn')])->withInput();
            }
            $del_flag=1;
            $this->listProject->updateName(['del_flag'=>$del_flag],$request->project_name);
            $cus->save();


        if ($cus->save()) {
            
            $project = new Project;

            $project->cus_id = $cus->id;

            $project->c_id = $cus->c_id;

            $project->s_id = $cus->s_id;

            $project->l_id = $cus->l_id;

            $project->name_project = $cus->project_name;

            $project->save();

        }else{
            echo "Có lỗi";
        }

            $datacheck= $this->cus->joinLecturers($request);
        $data= json_decode(json_encode($datacheck),TRUE);
        $data=[
            'data'=>$data
        ];
        $emailLecturers= $datacheck->l_email;
        $email=$request->email;

        Mail::send(['html' => 'frontend.mail.sendStudent'], $data, function($message) use($email) {
            $message->to($email)
                ->subject('Thông Tin Đăng Ký Đồ Án');
            $message->from($email,'Bộ Môn Tin Trắc Địa');
        });
        Mail::send(['html' => 'frontend.mail.sendLecturers'], $data, function($message) use($emailLecturers) {
            $message->to($emailLecturers)
                ->subject('Thông tin sinh viên đăng ký đồ án');
            $message->from($emailLecturers,'Bộ Môn Tin Trắc Địa');
        });
        Mail::send(['html' => 'frontend.mail.admin'], $data, function($message) use($email) {
            $message->to([env('MAIL_SUPER')])
                ->subject('Thông tin sinh viên đăng ký đồ án');
            $message->from($email,'Bộ Môn Tin Trắc Địa');
        });
        return redirect()->route('project-new')->with(['info' => sprintf(MSG007, 'Đồ án')])
           ->withInput();
        
    }

   
    public function store(Request $request)
    {
    }

    
    public function name($id)
    {
        $Project=$this->listProject->listProjectNameSaleFirst($id);
        $projectFirst=$this->listProject->listProjectNameFirstCheck($id);
        $projectDelFlag1=$this->listProject->listProjectNameDelFlag1($id);
        $checkGV=$this->listProject->checkListProject($id);
            if(count($checkGV) < 5){
                echo "<option  value=''>".'--Chọn  đồ án của giáo viên ' .$projectFirst->l_fullname.' --'."</option>";
            }else{
                    echo "<option  value=''>".'--Giáo viên ' .$projectDelFlag1->l_fullname.' đã đủ sinh viên hướng dẫn--'."</option>";

            }
            foreach($Project as $listProject){
                echo "<option  value='".$listProject->name."'>".$listProject->name."</option>";
            }
    }

    public function update(Request $request)
    {
    }

    
    public function destroy()
    {
    }
}
