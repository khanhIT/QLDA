<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Project\ProjectRepository;
use Common\Repositories\Lecturers\LecturersRepository;
use Common\Repositories\ClassName\ClassNameRepository;
use Common\Repositories\SchoolYear\SchoolYearRepository;
use Common\Repositories\CadresRead\CadresReadReponsitory;
use Common\Repositories\ListProject\ListProjectRepository;
class ProjectController extends Controller
{
    public function __construct(
        ProjectRepository $project,
        LecturersRepository  $lecturers,
        ClassNameRepository $className,
        SchoolYearRepository $schoolYear,
        CadresReadReponsitory $cadresRead,
        ListProjectRepository $listProjectName
    ){
        $this->project = $project;
        $this->lecturers = $lecturers;
        $this->className = $className;
        $this->schoolYear = $schoolYear;
        $this->cadresRead = $cadresRead;
        $this->listProjectName = $listProjectName;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $datacadresRead=$this->cadresRead->all('id asc');
        $dataSchoolYear=$this->schoolYear->all('id asc');
        $dataLecturers=$this->lecturers->getManyWhereOrder(['staff'=>1],['*'],'id asc');
        $dataClassName=$this->className->all('id asc');
        $listProject=$this->project->listProject();
        if(!empty($request->name_project)){
            $listProject=$this->project->searchQueryProject($request);
        }
        if(!empty($request->nameRead)){
            $listProject=$this->project->searchQueryProject($request);
        }
        if(!empty($request->lecturers)){
            $listProject=$this->project->searchQueryProject($request);
        }
        if(!empty($request->Lop)){
            $listProject=$this->project->searchQueryProject($request);
        }
        if(!empty($request->khoa)){
            $listProject=$this->project->searchQueryProject($request);
        }
        if(!empty($request->group)){
            $listProject=$this->project->searchQueryProject($request);
        }
        return view('frontend::project',compact('listProject','dataLecturers','dataClassName','dataSchoolYear','datacadresRead','listExaminer'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function news(Request $request)
    {
        $dataSchoolYear=$this->schoolYear->all('id asc');
        $dataLecturers=$this->lecturers->getManyWhereOrder(['staff'=>1],['*'],'id asc');
        $dataClassName=$this->className->all('id asc');
        $listProjectNew=$this->project->listProjectNew();
        if(!empty($request->name_project)){
            $listProjectNew=$this->project->searchQuery($request);
        }
        if(!empty($request->searchLecturers)){
            $listProjectNew=$this->project->searchQuery($request);
        }
        if(!empty($request->lecturers)){
            $listProjectNew=$this->project->searchQuery($request);
        }
        if(!empty($request->Lop)){
            $listProjectNew=$this->project->searchQuery($request);
        }
        if(!empty($request->khoa)){
            $listProjectNew=$this->project->searchQuery($request);
        }
        return view('frontend::project_new',compact('listProjectNew','dataLecturers','dataClassName','dataSchoolYear'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $detail_project=$this->project->detailProject($id);
        return view('frontend::project_detail',compact('detail_project'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function listProject()
    {
        $dataListProjectName=$this->listProjectName->listProjectName();
        $dataListProjectNameSale=$this->listProjectName->listProjectNameSale();
        return view('frontend::ListProjectName.index',compact('dataListProjectName','dataListProjectNameSale'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function listProjectDetail($id)
    {
        $dataListProjectName=$this->listProjectName->listProjectNameFirst($id);
        return view('frontend::ListProjectName.view',compact('dataListProjectName'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
