<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Lecturers\LecturersRepository;
use Common\Repositories\TypicalStudent\TypicalStudentRepository;
use Common\Repositories\MemorialPhoto\MemorialPhotoRepository;
use Common\Repositories\Schoolyear\SchoolYearRepository;
use Common\Repositories\Slide\SlideRepository;
use Common\Repositories\EmailRegister\EmailRegisterRepository;
use Modules\Frontend\Http\Requests\EmailRegisterRequest;

class FrontendController extends Controller
{
    public function __construct(
        EmailRegisterRepository $emailRegister,
        LecturersRepository  $lecturers,
        TypicalStudentRepository $student,
        MemorialPhotoRepository $photo,
        SchoolYearRepository $schoolYear,
        SlideRepository $slide
    ){
        $this->lecturers = $lecturers;
        $this->student = $student;
        $this->photo = $photo;
        $this->schoolYear = $schoolYear;
        $this->slide = $slide;
        $this->emailRegister = $emailRegister;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $dataLecturesMaster = $this->lecturers->getManyLecturerMaster();
        $dataLectures = $this->lecturers->getManyLecturers();
        $dataStudent = $this->student->all('id asc');
        $dataPhoto = $this->photo->all('id desc');
        $dataYear = $this->schoolYear->all('id asc');
        $dataslide = $this->slide->all('id desc');
        return view('frontend::index', compact('dataLecturesMaster', 'dataLectures', 'dataStudent', 'dataPhoto', 'dataYear','dataslide'));
    }

    public function create(EmailRegisterRequest $request)
    {
        $email =
            [
                'email' => $request->email,
            ];
        if ($this->emailRegister->create($email)) {
            $checkemail = $this->emailRegister->checkEmail($request->email);
            if (empty($checkemail)) {
                return redirect()->route('index')->with(['info' => sprintf(MSG030)])->withInput();
            } else {
                return redirect()->back()->with(['error' => sprintf(MSG011, 'email')])->withInput();
            }
        }
    }


    public function store(Request $request)
    {
    }


    public function show()
    {
        return view('frontend::show');
    }


    public function edit()
    {
        return view('frontend::edit');
    }


    public function update(Request $request)
    {
    }


    public function destroy()
    {
    }
}
