<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Lecturers\LecturersRepository;

class IntroduceController extends Controller
{
    public function __construct(
        LecturersRepository  $lecturers

){
    $this->lecturers = $lecturers;

}
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function lecturers()
    {
        $dataLectures = $this->lecturers->getManyLecturers();
        $dataLecturesMaster = $this->lecturers->getManyLecturerMaster();
        return view('frontend::lecturers',compact('dataLectures','dataLecturesMaster'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('frontend::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('frontend::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
