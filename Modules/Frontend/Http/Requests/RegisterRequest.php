<?php

namespace Modules\Frontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Common\Requests\BaseRequest;

class RegisterRequest extends BaseRequest
{
    public function __construct(){
        parent::__construct();

        $this->file_name = REGISTER_TABLE_JSON;
        $this->file_content = $this->readFileConfig($this->path . $this->file_name);
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->_rules = $this->responseRules();

        return $this->_rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        $this->_message = $this->responseMessages();
        return $this->_message;
    }
}
