@extends('frontend::layouts.master')
@section('title')
    Sách tham khảo
@endsection
@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/book.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
    <section id="books">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{ url('/') }}/upload/books/images/{!! $viewBooks->cover !!}" class="img-responsive">
                        </div>
                        <div class="col-md-8">
                            <table class="table table-hover">
                                <tr>
                                    <th>Tiêu đề:</th>
                                    <td>{{$viewBooks->title}}</td>
                                </tr>
                                <tr>
                                    <th>Tác giả</th>
                                    <td>{{$viewBooks->author}}</td>
                                </tr>
                                <tr>
                                    <th>Nhà xuất bản</th>
                                    <td>{{$viewBooks->publisher}}</td>
                                </tr>
                                <tr>
                                    <th>Năm</th>
                                    <td>{{$viewBooks->year}}</td>
                                </tr>
                                <tr>
                                    <th>Edition</th>
                                    <td>{{$viewBooks->edition}}</td>
                                </tr>
                                <tr>
                                    <th>Mô tả</th>
                                    <td>{!!$viewBooks->abstract!!}</td>
                                </tr>
                                <tr>
                                    <th>Tư liệu</th>
                                    <td><a href="#" data-toggle="modal" data-target="#exampleModalCenter">{!!$viewBooks->inside!!}</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>

        </div>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="margin-top: 120px ;border: 1px solid #2ada10">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle" style="font-weight: bold;color: #000">{{$viewBooks->title}}</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <h4 style="color:red ;font-weight: bold">Vì lý do bản quyền nên bạn không thể tải tài liệu xuống.</h4>
                      <h4  style="color:red ;font-weight: bold">Nếu bạn muốn tìm hiểu thì vui lòng lên văn phòng bộ môn để tìm hiểu.</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scriptAdd')
    <script type="text/javascript">

    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection