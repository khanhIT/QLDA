@extends('frontend::layouts.master')
@section('title')
    Đồ án đã bảo vệ
@endsection
@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/project_detail.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    <style type="text/css">
    
    iframe{
        margin-top: 20px;
        width: 100%; 
        height: 400px
    }
    
</style>
@endsection
@section('content')
    <section id="detail_project">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="text-center"><b style="color: red;">Tên Đồ án :</b><?php
                        if(strlen($detail_project->name_project)>5){
                            echo $detail_project->name_project;
                        }else{
                            echo $detail_project->pro_name;
                        }
                        ?></h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                            <tr>
                                <th>Tên Sinh Viên:</th>
                                <td>{{$detail_project->fullname}}</td>
                            </tr>
                            <tr>
                                <th>Mã số sinh viên:</th>
                                <td>{{$detail_project->msv}}</td>
                            </tr>
                            <tr>
                                <th>Giáo viên hướng dẫn :</th>
                                <td>{{$detail_project->l_fullname}}</td>
                            </tr>
                            <tr>
                                <th>Tên đề tài:</th>
                                <td><?php
                                    if(strlen($detail_project->name_project)>5){
                                        echo $detail_project->name_project;
                                    }else{
                                        echo $detail_project->pro_name;
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <th>Người chấm thi:</th>
                                <td>{{$detail_project->cadres_read}}</td>
                            </tr>
                            <tr>
                                <th>Lớp chuyên ngành:</th>
                                <td>{{$detail_project->c_name}}</td>
                            </tr>
                            <tr>
                                <th>Khóa:</th>
                                <td>K{{$detail_project->s_key}}</td>
                            </tr>
                            <tr>
                                <th>Điểm:</th>
                                <td>
                                    @if($detail_project->point >=5)
                                        <button class="btn btn-success btn-xs">Đạt</button>
                                    @else
                                        <button class="btn btn-danger btn-xs">Chưa đạt</button>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Tài liệu tham khảo:</th>
                                <td>
                                    {{$detail_project->file_upload_word}}
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <iframe class="doc" src="https://docs.google.com/viewer?embedded=true&url=http://qlda.localhost:8080/upload/files/file_word/Hoàng Minh Khánh_kiểm tra.docx"></iframe>
                                </td>
                            </tr>
                    </table>
                    <a href="{{route('project')}}" class="btn btn-danger">Back <span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </section>
@stop
@section('scriptAdd')
    <script type="text/javascript">

    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection