@extends('frontend::layouts.master')
@section('title')
    Danh sách các đề tài
@endsection
@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/project_list.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
    <section id="detai">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="col-md-4">
                        <p>Danh sách đề tài mới <img src="{{ url('/') }}/images/gif/gif-new-5.gif" alt="" style="width: 50px"></p>
                    </div>
                    <div class="listProject">
                        <table class="table table-hover" >
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên Đề Tài</th>
                                <th>Giáo viên hướng dẫn</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $n=1;?>
                            @foreach($dataListProjectName as $projectname)
                                <tr>
                                    <td>{{$n}}</td>
                                    <td><a href="{{route('project-list-detail',['id'=>$projectname->id])}}">{{$projectname->name}}</a></td>
                                    <td>{{$projectname->l_fullname}}</td>
                                    <th><a href="{{route('project-list-detail',['id'=>$projectname->id])}}" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span> Xem</a></th>
                                </tr>
                                <?php $n++;?>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $dataListProjectName->links() }}
                    </div>
                        <div class="col-md-4">
                            <p>Danh sách đề tài mới cũ</p>
                        </div>
                        <div class="listProject">
                            <table class="table table-hover" >
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên Đề Tài</th>
                                    <th>Giáo viên hướng dẫn</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $n=1;?>
                                @foreach($dataListProjectNameSale as $projectname)
                                    <tr>
                                        <td>{{$n}}</td>
                                        <td><a href="{{route('project-list-detail',['id'=>$projectname->id])}}">{{$projectname->name}}</a></td>
                                        <td>{{$projectname->l_fullname}}</td>
                                        <th><a href="{{route('project-list-detail',['id'=>$projectname->id])}}" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span> Xem</a></th>
                                    </tr>
                                    <?php $n++;?>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $dataListProjectNameSale->links() }}
                        </div>
                    </div>
            </div>
        </div>

    </section>

@stop
@section('scriptAdd')
    <script type="text/javascript">

    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection