@extends('frontend::layouts.master')
@section('title')
    Chi tiết đề tài
@endsection
@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/project_detail.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
    <section id="detail_project">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="text-center"><b style="color: red;">Tên đề tài :</b>{{$dataListProjectName->name}}
                       </h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>Giáo viên hướng dẫn:</th>
                            <td>{{$dataListProjectName->l_fullname}}</td>
                        </tr>
                        <tr>
                            <th>Tên đề tài:</th>
                            <td>
                                {{$dataListProjectName->name}}
                            </td>
                        </tr>
                        <tr>
                            <th>Mô tả về đề tài</th>
                            <td>{!! $dataListProjectName->description !!}</td>
                        </tr>
                    </table>
                    <a href="{{route('project-list')}}" class="btn btn-danger">Back <span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </section>
@stop
@section('scriptAdd')
    <script type="text/javascript">

    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection