@extends('frontend::layouts.master')
@section('title')
   Tin tức
@endsection
@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/news.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<section id="news">
	<div class="container-fluid">
		<div class="row">
			@foreach($dataNews as $news)
			<div class="col-md-12">
				<div class="col-md-4">
					<a href="{{route('news-detail',['id' =>$news->id])}}"></a>
					<img src="{{ url('/') }}/upload/news/{{$news->image}}">
				</div>
				<div class="col-md-8">
					<a href="{{route('news-detail',['id' =>$news->id])}}">
						<h1>{{$news->title}}</h1>
					</a>
					<h5>
						<span class="fa fa-user"></span> Bộ môn Tin học trắc địa<span class="fa fa-calendar" style="margin-left: 20px;"> {{date_format(new DateTime($news->created_at),'d-m-Y')}}</span>
					</h5>

					<p>@if(!empty($news))
                            <?php $string=$news->content?>
							@if(strlen($string)>894)
								{{ substr($string,0,895)}}...
							@else
								{!! $lecturers->info !!}
							@endif
						@endif</p>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
@stop
@section('scriptAdd')
<script type="text/javascript">

</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection