@extends('frontend::layouts.master')
@section('title')
    Đồ án đã bảo vệ
@endsection
@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/project.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<section id="search">
	<div class="col-md-12">
		<div class="panel panel-info">
                   <div class="panel-heading">
                          <h3 class="text-center">Danh sách đồ án đã bảo vệ</h3>  
                     </div>
			<div class="panel-body">
				<form action="" method="GET" role="form">
					{{csrf_field()}}
					<div class="col-md-2">
						
							<input type="search" name="name_project" class="form-control" placeholder="Tìm kiếm theo tên đồ án..">
				
						</div>
						<div class="col-md-2">
							<select name="lecturers" id="input" class="form-control" >
								<option value="">--Chọn CBHD--</option>
								@foreach($dataLecturers as $lecturers)
									<option  value="{{$lecturers->id}}">{{$lecturers->l_fullname}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<input type="search" name="nameRead" class="form-control" value="{{old('nameRead')}}" placeholder="Tìm kiếm giáo viên đọc chấm">
						</div>
						<div class="col-md-2">
							<select name="group" id="input" class="form-control" >
								<option value="">--Chọn Hội Đồng--</option>
								<option {{ request()->group == '1' ? 'selected="selected"':'' }} value="1">1</option>
								<option {{ request()->group == '2' ? 'selected="selected"':'' }} value="2">2</option>
								<option {{ request()->group == '3' ? 'selected="selected"':'' }} value="3">3</option>
								<option {{ request()->group == '4' ? 'selected="selected"':'' }} value="4">4</option>
							</select>
						</div>
						<div class="col-md-2">
							<select name="Lop" id="input" class="form-control" >
								<option value="">--Chọn Lớp--</option>
								@foreach($dataClassName as $className)
									<option value="{{$className->id}}">{{$className->c_name}}</option>
								@endforeach
							</select>
						</div>
					<div class="col-md-2">
						<select name="khoa" id="input" class="form-control" >
							<option value="">--Chọn Khóa--</option>
							@foreach($dataSchoolYear as $schoolYear)
								<option value="{{$schoolYear->id}}">DCCTTDK{{$schoolYear->s_key}}</option>
							@endforeach
						</select>
					</div>
					<div class="text-center " >
						<button type="submit" class="btn btn-primary col-md-2 " style="margin-left: 530px; margin-top: 20px;" ><span class="glyphicon glyphicon-search"></span> Tìm Kiếm</button>
					</div>
					
				</form>
				
			</div>
		</div>
	</div>
</section>
<section id="list-project">
	<div class="col-md-12">

	<table class="table table-hover" >
       <thead>
       	    <tr>
       	    	<th>STT</th>
       	    	<th>Tên Đề Tài</th>
       	    	<th>Sinh Viên Thực Hiện</th>
       	    	<th>GV Hướng Dẫn</th>
       	    	<th>GV chấm thi</th>
       	    	<th>Hội Đồng Thi</th>
				<th>Lớp</th>
				<th>Khóa</th>
       	    	<th>Điểm</th>
       	    </tr>
       </thead>
       <tbody>
       <?php $n=1;?>
	   @foreach($listProject as $project)
		   <tr>
			   <td>{{$n}}</td>
			   <td><a href="{{route('project-detail',['id' =>$project->id])}}">
					   <?php
                       if(strlen($project->name_project)>5){
                           echo $project->name_project;
                       }else{
                           echo $project->pro_name;
                       }
                       ?>
				   </a></td>
			   <td>{{$project->fullname}}</td>
			   <td>{{$project->l_fullname}}</td>
			   <td>{{$project->cadres_read}}</td>
			   <td>{{$project->group}}</td>
			   <td>{{$project->c_name}}</td>
			   <td>DCTCCD{{$project->s_key}}</td>
			   <td>

				   		@if($project->point >=5)
					   		<button class="btn btn-xs btn-success" style="width: 50px;">Đạt</button>
					   @else
						   <button class="btn btn-xs btn-danger">Không đạt</button>
						@endif
			   </td>
		   </tr>
           <?php $n++;?>
		   @endforeach
       </tbody>
    </table>
		{{ $listProject->links() }}
	</div>
</section>

@stop
@section('scriptAdd')
<script type="text/javascript">

</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection