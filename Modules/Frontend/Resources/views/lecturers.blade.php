@extends('frontend::layouts.master')
@section('title')
   Đội ngũ cán bộ
@endsection
@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/lectures.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<section id="lectures">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="title col-md-10">
				<h1 class="text-center">ĐỘI NGŨ CÁN BỘ</h1>
			</div>
			<div class="col-md-1"></div>
			<div class="list-lectures col-md-12">
				<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-2" style="margin-left: -50px"></div>
					@foreach($dataLecturesMaster as $lecture )
					<div class="col-md-3">
						<div class="img-circle">
							<a href="{{$lecture->link_url}}" target="_blank"><img src="{!! url('/') !!}/upload/lecturers/{!! $lecture->image !!}"></a>
						</div>
						<p><a href="{{$lecture->link_url}}" target="_blank">{{$lecture->l_fullname}}</a></p>
						<p style="font-weight: bold">{{$lecture->name}}@if(!empty($lecture->p_name)),@endif {{$lecture->p_name}}</p>
						<ul class="social-links">
							<li><a href="http://www.researchgate.net" title=""><i class="fa fa-linkedin"></i></a></li>
							<li class="fb"><a href="https://fb.com" title=""><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://google.com" title=""><i class="fa fa-twitter"></i></a></li>
						</ul>
					</div>
					@endforeach
					<div class="col-md-1"></div>
					@foreach($dataLectures as $lecture)
						<div class="col-md-3">
								<div class="img-circle">
									<a href="{{$lecture->link_url}}" target="_blank"><img src="{!! url('/') !!}/upload/lecturers/{!! $lecture->image !!}"></a>
								</div>
								<p><a href="{{$lecture->link_url}}" target="_blank">{{$lecture->l_fullname}}</a></p>
								<p style="font-weight: bold">{{$lecture->name}}@if(!empty($lecture->p_name)),@endif {{$lecture->p_name}}</p>
								<ul class="social-links">
									<li><a href="http://www.researchgate.net" title=""><i class="fa fa-linkedin"></i></a></li>
									<li class="fb"><a href="https://fb.com" title=""><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://google.com" title=""><i class="fa fa-twitter"></i></a></li>
								</ul>
						</div>
					@endforeach
				</div>

			</div>
			<div class="col-md-1"></div>
			</div>
			
		</div>
	</div>
</section>
@stop
@section('scriptAdd')
<script type="text/javascript">

</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection