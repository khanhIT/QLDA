@extends('frontend::layouts.master')
@section('title')
    Tin tức
@endsection
@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/news.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
    <section id="news_detail">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-1"></div>
               <div class="col-md-10">
                    <div class="col-md-8">
                        <h1>{{$detailNews->title}}</h1>
                        <h5><span class="fa fa-user"></span> Bộ môn Tin học trắc địa   <span class="fa fa-calendar" style="margin-left: 20px;"> {{date_format(new DateTime($detailNews->created_at),'d-m-Y')}}</span></h5>
                        <img src="{{ url('/') }}/upload/news/{{$detailNews->image_content}}" alt="">
                        <p class="text_news">
                            {!! $detailNews->content !!}
                        </p>
                    </div>

                   <div class="col-md-4 ">
                       <div class="link_news">
                        <p>Bài viết mới</p>
                       @foreach($listTitle as $list)
                            <div class="news_title">
                                <h4><a href="{{route('news-detail',['id'=>$list->id])}}">{{$list->title}}</a></h4>
                                <h5 style="color: #96a1a3"><span class="fa fa-calendar" ></span> {{date_format(new DateTime($list->created_at),'d-m-Y')}}</h5>
                            </div>
                       @endforeach
                       </div>
                   </div>
               </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </section>
@stop
@section('scriptAdd')
    <script type="text/javascript">

    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection