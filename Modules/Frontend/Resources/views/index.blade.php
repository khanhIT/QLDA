@extends('frontend::layouts.master')
@section('title')
    Trang Chủ
@endsection
@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"/>
    <style type="text/css">
        #lanhdao .intro-img h6.content{
            background-color: rgba(33, 91, 166, 0.7);
            color: #ffffff;
            font-size: 17px;
            left: 0;
            margin-bottom: 0;
            line-height: 30px;
            position: absolute;
            padding: 26px;
        }
        #lanhdao .intro-img:hover .content{
            animation: content 20s;
        }

        @keyframes content {
          0% {
            bottom: 0;
          }
          50% {
            bottom: 0;
          }
          100% {
            bottom: 0;
          }
}
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <section id="slider" style="margin-top: 100px;">
                @if (session('info'))
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{session(info)}}
                    </div>
                @endif
                <div id="carousel-id" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-id" data-slide-to="0" class=""></li>
                        <li data-target="#carousel-id" data-slide-to="1" class=""></li>
                        <li data-target="#carousel-id" data-slide-to="2" class="active"></li>
                    </ol>
                    <div class="carousel-inner">
                        @foreach($dataslide as $slide)
                            <div class="item">
                                <img data-src="holder.js/900x500/auto/#777:#7a7a7a/text:First slide" alt="First slide" src="{{ url('/') }}/upload/slide/{{$slide->image}}" style="height: 100%;width: 100%">
                                <div class="container">
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="item active">
                            <img data-src="holder.js/900x500/auto/#777:#7a7a7a/text:First slide" alt="First slide" src="{{ url('/') }}/upload/slide/5accc37c1e053.jpg" style="height: 100%;width: 100%">
                            <div class="container">
                                <div class="carousel-caption col-md-6 slide" style="float:right;">
                                    <h1 class="cttc">CTTD</h1>
                                    <p class="ptt">Các bạn sinh viên đang theo học hãy dựa vào kế hoạch đào tạo chuẩn này để đăng ký môn học. Tuy không bắt buộc nhưng hãy bám theo tài liệu này sẽ là một lựa chọn tốt.</p>
                                    <p class="ptt"><a class="btn btn-lg btn-primary" target="_blank" href="http://geoinformatics.humg.edu.vn/images/docs/DCCTTD_Ke_hoach_dao_tao.pdf" role="button">Tải về kế hoạch đào tạo chuẩn →</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                    <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
                <section id="introduce">
                    <div class="container-fluid subject">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1 class="text-center">Bộ môn Tin học trắc địa</h1>
                                <p class="text-center">Tên tiếng anh: Department of Geoinformatics</p>
                                <p class="text-center" >Là đơn vị chuyên môn, trực thuộc Khoa Công nghệ thông tin.</p>
                                <p class="text-center">Thành lập ngày 7 tháng 10 năm 2002 theo Quyết định số 381/QĐ-MĐC-TCCB của </p>
                                <p class="text-center">Hiệu trưởng Trường Đại học Mỏ - Địa chất</p>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-10" style="margin-top: 30px">
                                <div class="col-md-6">
                                    <div class="thanhtich ">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <h3>Đội ngũ cán bộ</h3>
                                    <p>Có tổng số 15 cán bộ viên chức, trong đó có 1 NGƯT. GVCC. PGS. TS, 2 Tiến sĩ, 8 Thạc sĩ, 4 Kỹ sư. Hiện tại có 4 cán bộ đang làm NCS ở nước ngoài (1 NCS tại Mỹ, 1 NCS tại Đức, 1 NCS tại Trung Quốc, 1 NCS tại Úc) và 2 cán bộ đang làm NCS trong nước, 2 cán bộ đang học ThS ở nước ngoài. Hầu hết các cán bộ giảng dạy đã hoàn thành 2 bằng đại học và có chuyên môn sâu trong cả lĩnh vực Tin học và lĩnh vực Trắc địa - Bản đồ viễn thám & Hệ thông tin địa lý.</p>


                                </div>
                                <div class="col-md-6">
                                    <div class="icon-daotao">
                                        <i class="fa fa-tasks"></i>
                                    </div>
                                    <h3>Chức năng nhiệm vụ</h3>
                                    <p>Đào tạo kỹ sư Công nghệ thông tin - Chuyên ngành Tin học trắc địa (kỹ sư Địa tin học) có phẩm chất đạo đức tốt, có kiến thức tổng hợp của chuyên ngành Tin học và Trắc địa - Bản đồ viễn thám & Hệ thông tin địa lý (GIS), có khả năng lập trình, phát triển và khai thác các phần mềm trong lĩnh vực Công nghệ thông tin - Trắc địa - Bản đồ viễn thám và GIS. Cung cấp nguồn nhân lực công nghệ thông tin phục vụ phát triển kinh tế.</p>
                                </div>
                                <div class="col-md-6">
                                    <div class="thanhtich ">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                    <h3>Thành tích nổi bật</h3>
                                    <p>Chúng tôi đã và đang tham gia đào tạo hàng trăm sinh viên tốt nghiệp ra trường, đa số có việc làm phù hợp với chuyên ngành được đào tạo; thực hiện nhiều đề tài nghiên cứu khoa học các cấp; thực hiện nhiều hợp đồng chuyển giao công nghệ; tham gia dự án phát triển phần mềm nguồn mở OpenTenure với Tổ chức Nông Lương Liên hợp quốc (UN-FAO); công bố nhiều bài báo trên các tạp chí trong nước và quốc tế; có nhiều sản phẩm phần mềm cho thiết bị di động trên Apple Store và các sản phẩm phần mềm cho Desktop trên nền tảng Windows.</p>


                                </div>
                                <div class="col-md-6">
                                    <div class="icon-daotao">
                                        <i class="fa fa-cubes"></i>
                                    </div>
                                    <h3>Chương trình đào tạo</h3>
                                    <p><b>Đào tạo đại học:</b>Chương trình Tin học trắc địa được thiết kế theo mô hình song ngành, và tham khảo trên khung chương trình đào tạo các ngành Địa tin học của các trường đại học trên thế giới như: University of Twente, University of Florida, California State University và các khung chương trình Công nghệ thông tin, Trắc địa, Bản đồ Trường ĐH Mỏ - Địa chất. Với mô hình đào tạo này, trên thế giới sử dụng thuật ngữ Địa tin học (Geoinformatics). Trên cơ sở đó, hiện nay Bộ môn Tin học trắc địa cùng Bộ môn Tin học Địa chất đã xây dựng chương trình Địa tin học trên nền tảng của chương trình Tin học trắc địa và Tin học địa chất. Chương trình này sẽ áp dụng cho khóa 60 và các khóa tiếp theo.
                                        Đào tạo sau đại học:	Hiện nay Bộ môn đã xây dựng đề án mở chương trình đào tạo sau đại học ngành Địa tin học và đang trình Bộ Giáo dục và Đào tạo phê duyệt. Dự kiến tuyển sinh trong năm 2017.</p>
                                </div>
                                <div class="col-md-6">
                                    <div class="thanhtich">
                                        <i class="fa fa-graduation-cap"></i>
                                    </div>
                                    <h3>Sinh viên tốt nghiệp</h3>
                                    <p>Sau khi tốt nghiệp, kỹ sư Địa tin học có thể làm việc cho các cơ quan như: Bộ Tài nguyên & Môi trường và các đơn vị trực thuộc; Bộ Nông nghiệp & Phát triển nông thôn và các đơn vị trực thuộc; Các doanh nghiệp trong lĩnh vực Đo đạc & Bản đồ; Các công ty về lĩnh vực công nghệ thông tin; Nhà phát triển phần mềm độc lập; Có cơ hội tham gia các dự án phát triển phần mềm với các tổ chức quốc tế,...</p>
                                </div>
                                <div class="col-md-6">
                                    <div class="icon-daotao">
                                        <i class="fa fa-paper-plane-o"></i>
                                    </div>
                                    <h3>Hướng nghiên cứu chính	</h3>
                                    <p>Hướng nghiên cứu chính của bộ môn là "Ứng dụng, phát triển sản phẩm Công nghệ thông tin trong lĩnh vực Trắc địa - Bản đồ - Viễn thám và GIS; Nghiên cứu, phát triển các sản phẩm phần mềm nguồn mở cho Địa tin học; Lập trình xử lý và đo vẽ ảnh viễn thám; Lập trình phát triển ứng dụng chuyên ngành trên các thiết bị di động thông minh,..."</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                </section>
                <section id="lanhdao">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="text-center">Các lãnh đạo đơn vị</h1>
                            </div>
                            <div class="col-md-12 info-leader">
                                @foreach($dataLecturesMaster as $master)
                                    <div class="col-md-4 leader">
                                        <div class="row">
                                            <div class="intro-img">
                                                <a href="{{$master->link_url}}" target="_blank"><img src="{!! url('/') !!}/upload/lecturers/{!! $master->image !!}"></a>
                                                <h6 class="content">
                                                    {!! $master->info !!}</h6>
                                            </div>
                                            <div class="name-gv1">
                                                <a href="{{$master->link_url}}" target="_blank" class="text-center"><h4>{{$master->name}}. {{$master->l_fullname}}</h4></a>
                                                <p class="text-center" style="text-transform:uppercase">{{$master->p_name}}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
                <section id="teachers">
                    <div class="col-md-12">
                        <h1 class="text-center">Đội ngũ cán bộ viên chức</h1>
                        <p class="text-center">Đội ngũ cán bộ mẫn cán này đã giúp chúng tôi có được một tập thể đoàn kết, vững mạnh. </p>
                        <p class="text-center">Họ đầy nhiệt huyết và không ngừng học tập để nâng cao trình độ chuyên môn</p>
                    </div>
                    <div class="slider">
                        @foreach($dataLectures as $lecturers)
                            <div class="slide">
                                <a href="{{$lecturers->link_url}}" target="_blank"><img src="{!! url('/') !!}/upload/lecturers/{!! $lecturers->image !!}"></a>
                                <a href="{{$lecturers->link_url}}" target="_blank"><h1>{{$lecturers->l_fullname}}</h1></a>
                                <p>{{$lecturers->p_name}}@if(!empty($lecturers->p_name)),@endif {{$lecturers->name}}</p>
                                <p>@if(!empty($lecturers))
                                        <?php $string=$lecturers->info?>
                                        @if(strlen($string)>79)
                                            {{ substr($string,0,80)}}...
                                        @else
                                            {!! $lecturers->info !!}
                                        @endif
                                    @endif
                                </p>
                            </div>
                        @endforeach

                    </div>

                </section>
                <section id="students">
                    <div class="col-md-12">
                        <h1 class="text-center">Một số gương mặt sinh viên và cựu sinh viên tiêu biểu</h1>
                        <p class="text-center">Đây chỉ là một ít cá nhân trong rất nhiều cá nhân điển hình trong học tập và nỗ lực để thành đạt trong cuộc sống. Chúng tôi không thể có ngày hôm nay nếu như không có sự đóng góp to lớn của các bạn bằng việc không ngừng nỗ lực trong học tập và rèn luyện cũng như sự nhiệt huyết với công việc.</p>

                    </div>
                    <div class="slider2">
                        @foreach($dataStudent as $student)
                            <div class="slide">
                                <a href="{{$student->link_url}}" target="_blank"><img src="{!! url('/') !!}/upload/typicalStudent/{!! $student->image !!}"></a>
                                <h1 class="text-center">{{$student->name}}</h1>
                                <p>@if(!empty($student))
                                        <?php $string=$student->info?>
                                        @if(strlen($string)>149)
                                            {!! substr($string,0,150)!!}...
                                        @else
                                            {!! $student->info !!}
                                        @endif
                                    @endif
                                </p>
                            </div>
                        @endforeach
                    </div>
                </section>
                <section id="img-student">
                    <div class="col-md-12">
                        <h1 class="text-center">Một số hình ảnh kỷ niệm của các thế hệ sinh viên</h1>
                    </div>
                    <div class="slider3">
                        @foreach($dataPhoto as $photo)
                            <div class="slide">
                                <img src="{!! url('/') !!}/upload/memorialPhoto/{!! $photo->image !!}" alt="" class="img-responsive" />
                            </div>
                        @endforeach
                    </div>

                </section>
                <section id="qty-student">
                    <div class="col-md-12">
                        <h1 class="text-center">Các thế hệ sinh viên <b>đã tốt nghiệp</b> và <b>được đào tạo</b> theo chuyên ngành Tin học trắc địa</h1>
                        <div class="quantity">
                            <div class="col-md-3">
                                63
                            </div>
                            <p  class="text-center">Khóa 47 ( 2002-2007 )</p>
                        </div>

                        <div class="quantity2" >
                            <div class="col-md-3">
                                40
                            </div>
                            <p  class="text-center">Khóa 48 ( 2003-2008 )</p>
                        </div>
                        <div class="quantity2" >
                            <div class="col-md-3">
                                42
                            </div>
                            <p  class="text-center">Khóa 49 ( 2004-2009 )</p>
                        </div>
                        <div class="quantity4"  >
                            <div class="col-md-3">
                                58
                            </div>
                            <p  class="text-center">Khóa 50 ( 2005-2010 )</p>
                        </div>
                        <div class="quantity4"  >
                            <div class="col-md-3">
                                50
                            </div>
                            <p class="text-center">Khóa 51 ( 2006-2011 )</p>
                        </div>
                        <div class="quantity">
                            <div class="col-md-3">
                                49
                            </div>
                            <p  class="text-center">Khóa 52 ( 2007-2012 )</p>
                        </div>

                        <div class="quantity2" >
                            <div class="col-md-3">
                                70
                            </div>
                            <p  class="text-center">Khóa 53 ( 2008-2013 )</p>
                        </div>
                        <div class="quantity2" >
                            <div class="col-md-3">
                                48
                            </div>
                            <p  class="text-center">Khóa 54 ( 2009-2014 )</p>
                        </div>
                        <div class="quantity4"  >
                            <div class="col-md-3">
                                36
                            </div>
                            <p  class="text-center">Khóa 55 ( 2010-2014 )</p>
                        </div>
                        <div class="quantity4"  >
                            <div class="col-md-3">
                                24
                            </div>
                            <p class="text-center">Khóa 56 ( 2011-2015 )</p>
                        </div>
                         <div class="quantity4"  >
                            <div class="col-md-3">
                                39
                            </div>
                            <p class="text-center">Khóa 57 ( 2012-2017 )</p>
                        </div>
                        @foreach($dataYear as $year)
                            <div class="quantity4"  >
                                <div class="col-md-3">
                                    {{$year->total_student}}
                                </div>
                                <p class="text-center">Khóa {{$year->s_key}} ( {{date_format(new DateTime($year->year_start),'Y')}} - {{date_format(new DateTime($year->year_end),'Y')}} )</p>
                            </div>
                        @endforeach

                    </div>
                </section>
                <div class="col-md-12 link">
                    <h4 class="text-center">Liên kết Website</h4>
                    <div class="col-md-3">
                        <a href="http://www.humg.edu.vn/"><img src="{{asset('images/logo/xlogo-3.png')}}"></a>
                    </div>
                    <div class="col-md-3">
                        <a href="http://it.humg.edu.vn/"><img src="{{asset('images/logo/xit_client.png')}}"></a>
                    </div>
                    <div class="col-md-3">
                        <a href="http://daotao.humg.edu.vn/"><img src="{{asset('images/logo/xdktc.gif')}}"></a>
                    </div>
                    <div class="col-md-3">
                        <a href="http://tapchi.humg.edu.vn/"><img src="{{asset('images/logo/xtapchi_humg_client.png')}}"></a>
                    </div>
                </div>
        </div>
    </div>
    <script type="text/javascript">
        window.onload=function(){
            $('.slider').slick({
                autoplay:true,
                autoplaySpeed:2000,
                arrows:true,
                centerMode:true,
                slidesToShow:4,
                slidesToScroll:1,
                dots1: true
            });

            $('.slider2').slick({
                autoplay:true,
                autoplaySpeed:2000,
                arrows:true,
                centerMode:true,
                slidesToShow:4,
                slidesToScroll:1,
                dots2: true
            });

            $('.slider3').slick({
                autoplay:true,
                autoplaySpeed:2000,
                arrows:true,
                centerMode:true,
                slidesToShow:4,
                slidesToScroll:1,
                dots: true
            });
        };
    </script>
@stop
@section('scriptAdd')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
@endsection

