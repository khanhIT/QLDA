@extends('frontend::layouts.master')
@section('title')
    Đồ án mới đăng ký
@endsection
@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/project.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<section id="search">
	<div class="col-md-12">
		<div class="panel panel-info">
                   <div class="panel-heading">
                          <h3 class="text-center">Danh sách đồ án mới đăng ký</h3>  
                     </div>
			<div class="panel-body">
				<form action="" method="GET" role="form">
					{{csrf_field()}}
					<div class="col-md-3">
							<input type="search" name="name_project"  class="form-control" placeholder="Tìm kiếm theo tên đồ án...">
						</div>
						<div class="col-md-2">
							<select name="lecturers" id="input" class="form-control">
								<option value="">--Chọn CBHD--</option>
							@foreach($dataLecturers as $lecturers)
								<option  {{ old('lecturers') == $lecturers['id'] ? "selected" : "" }} value="{{$lecturers->id}}">{{$lecturers->l_fullname}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<select name="Lop" id="input" class="form-control">
								<option value="">--Chọn Lớp--</option>
								@foreach($dataClassName as $className)
									<option {{ old('Lop') == $className['id'] ? "selected" : "" }} value="{{$className->id}}">{{$className->c_name}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<select name="khoa" id="input" class="form-control">
								<option value="">--Chọn Khóa--</option>
								@foreach($dataSchoolYear as $schoolYear)
									<option {{ old('khoa') == $schoolYear['id'] ? "selected" : "" }} value="{{$schoolYear->id}}">DCCTTDK{{$schoolYear->s_key}}</option>
								@endforeach
							</select>
						</div>
					<div class="col-md-3">
						<button type="submit" class="btn btn-primary "><span class="glyphicon glyphicon-search"></span> Tìm Kiếm</button>
					</div>
					
				</form>
				
			</div>
		</div>
	</div>
</section>
<section id="list-project">
	<div class="col-md-12">

	<table class="table table-hover" >
       <thead>
       	    <tr>
       	    	<th>STT</th>
       	    	<th>Tên Đề Tài</th>
       	    	<th>Sinh Viên Thực Hiện</th>
       	    	<th>GV Hướng Dẫn</th>
       	    	<th>Lớp</th>
				<th>Khóa</th>
       	    </tr>
       </thead>
       <tbody>
		   <?php $n=1;?>
	   @foreach($listProjectNew as $project)
       	<tr>
       		<td>{{$n}}</td>
       		<td><a href="">
					<?php
                    if(strlen($project->name_project)>5){
                        echo $project->name_project;
                    }else{
                        echo $project->pro_name;
                    }
					?>
				</a></td>
       		<td>{{$project->fullname}}</td>
       		<td>{{$project->l_fullname}}</td>
       		<td>{{$project->c_name}}</td>
			<td>DCCTTDK{{$project->s_key}}</td>
       	</tr>
		   <?php $n++;?>
		   @endforeach
       </tbody>
    </table>
		{{ $listProjectNew->links() }}
	</div>
</section>

@stop
@section('scriptAdd')
<script type="text/javascript">

</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection