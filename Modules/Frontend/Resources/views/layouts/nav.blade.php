<section id="menu">
            <div class="col-md-12" id="header-nav">
            <div class="col-md-10 contact">
                <p>
                    <a href="" ><i class="fa fa-home"></i> Bộ Môn Tin học trắc địa</a>
                    <a href="" ><i class="fa fa-phone"></i> Hotline:0983 448 779</a>
                </p>
            </div>
            <div class="col-md-2 links">
                <p>
                    <a href="" class="facebook img-circle"><i class="fa fa-facebook"></i></a>
                    <a href="" class="twitter img-circle"><i class="fa fa-twitter"></i></a>
                    <a href="" class="google img-circle"><i class="fa fa-google-plus"></i></a>
                    <a href="" class="linkedin img-circle"><i class="fa fa-linkedin"></i></a>
                </p>
            </div>
        </div>
        	<div class="container-fluid">
        	<div class="row">
        			<div class="col-md-12">
        				<div class="row">
        					<nav class="navbar navbar-default" role="navigation">
        					<div class="container-fluid">
        						<!-- Brand and toggle get grouped for better mobile display -->
        						<div class="navbar-header logo">
        							<a href="{{route('index')}}">
                                        <img src="{{asset('images/logo/logo.png')}}" style="margin-left: 40px">
                                    </a>
        						</div>
        				
        						<!-- Collect the nav links, forms, and other content for toggling -->
        						<div class="collapse navbar-collapse navbar-ex1-collapse">
        							<ul class="nav navbar-nav">
        								<li><a href="{{route('index')}}"> <span class="glyphicon glyphicon-home"></span> TRANG CHỦ</a></li>
        								<li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-tasks"></span> GIỚI THIỆU<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{route('lecturers')}}">CÁN BỘ</a></li>
                                            </ul>
                                        </li>
        								<li><a href="{{route('news')}}"><span class="glyphicon glyphicon-list-alt"></span> TIN TỨC</a></li>
        								<li><a href="{{route('contact')}}"><span class="glyphicon glyphicon-envelope"></span> LIÊN HỆ</a></li>
                                        <li><a href="{{route('project-list')}}"><span class="glyphicon glyphicon-list-alt"></span> DANH SÁCH ĐỀ TÀI</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-education"></span> ĐATN <b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{route('project-new')}}">MỚI ĐĂNG KÝ</a></li>
                                                <li><a href="{{route('project')}}">ĐÃ BẢO VỆ</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="{{route('books')}}"><span class="fa fa-book"></span> SÁCH</a></li>
        							</ul>
        							
        							<ul class="nav navbar-nav navbar-right">
        								<li><a href="{{route('register')}}"><span class="glyphicon glyphicon-registration-mark"></span> ĐĂNG KÝ</a></li>	
        							</ul>
        						</div><!-- /.navbar-collapse -->
        					</div>
        				</nav>
        				</div>
        				
        			</div>
        		</div>
        	</div>
        </section>