<footer>        
    <div class="col-md-12 footer-link">
        <div class="col-md-6 link-left">
            <h1>GEOINFORMATICS</h1>
            <p>Copyright © 2018 Department of Geoinformatics. All Rights Reserved.</p>
            <p class="link-in">
                <a  addthis:userid="chuyentt" href="http://www.facebook.com/chuyentt" target="_blank" title="Follow on Facebook"><i class="fa fa-facebook"></i></a>
                <a  addthis:userid="chuyentt" href="//twitter.com/chuyentt" target="_blank" title="Follow on Twitter"><i class="fa fa-twitter"></i></a>
                <a  addthis:userid="+chuyentt" title="Follow on Google" href="https://plus.google.com/+chuyentt" target="_blank"><i class="fa fa-google-plus"></i></a>
                <a  addthis:userid="chuyentt" href="http://www.pinterest.com/chuyentt" target="_blank" title="Follow on Pinterest"><i class="fa fa-pinterest"></i></a>
                <a  addthis:usertype="company" addthis:userid="chuyentt" href="http://www.linkedin.com/company/chuyentt" target="_blank" title="Follow on LinkedIn"><i class="fa fa-linkedin"></i></a>
            </p>
        </div>
        <div class="col-md-6 link-right">
            <div class="register">
                <div class="box">
                     <p>Đăng ký vào danh sách email của chúng tôi để nhận được thông tin cập nhật.</p>
                <form class="form-inline" method="POST">
                    {{csrf_field()}}
                    <div class="form-group @if (session('error'))  has-error    @endif {{ $errors->has('email') ? 'has-error' : '' }}">
                         <input type="email" name="email" class="form-control"  placeholder="Email">
                    <button type="submit">Đăng Ký</button>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif {{ $errors->first('email') }}</span>
                    </div>
                   
                </form>
                </div>
               
            </div>
        </div>

        <button onclick="topFunction()" id="myBtn" title="Go to top"> <span class="glyphicon glyphicon-chevron-up" style="font-size: 23px"></span></button>
    </div>
    <script type="text/javascript">
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            $('html, body').animate({
                scrollTop: ($('#myBtn').offset().top -6000)
            },1000);
        }
    </script>
</footer>