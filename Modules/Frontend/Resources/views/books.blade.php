@extends('frontend::layouts.master')
@section('title')
    Sách tham khảo
@endsection
@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/book.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<section id="books">
	<div class="container-fluid">
		<div class="row">
			<div class="search-book">
				<form action="" method="GET" class="form-inline" role="form">
					{{csrf_field()}}
					<div class="form-group">
						<label class="sr-only" for="">label</label>
						<input type="search" class="form-control" id="" name="nameBooks" placeholder="Nhập tên sách..">
					</div>
					<button type="submit" class="btn btn-primary">Tìm Kiếm </button>
				</form>
			</div>
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="row">
					@foreach($listBooks as $books)
					<div class="col-md-3">
						<div class="img-book">
							<a href="{{route('viewBook',['id'=>$books->id])}}"><img src="{{ url('/') }}/upload/books/images/{!! $books->cover !!}" class="img-responsive"></a>
						</div>
						
						<p><a href="{{route('viewBook',['id'=>$books->id])}}">@if(!empty($books))
                                    <?php $string=$books->title?>
									@if(strlen($string)>24)
										{{ substr($string,0,25)}}...
									@else
										{!! $books->title !!}
									@endif
								@endif</a></p>
						<p>Edition: {{$books->edition}} ; Year: {{$books->year}}</p>
						<p>Author:@if(!empty($books))
                                <?php $string=$books->author?>
								@if(strlen($string)>24)
									{{ substr($string,0,25)}}...
								@else
									{!! $books->author !!}
								@endif
							@endif</p>
						<p>ISBN: @if(!empty($books))
                                <?php $string=$books->isbn?>
								@if(strlen($string)>23)
									{{ substr($string,0,24)}}...
								@else
									{!! $books->isbn !!}
								@endif
							@endif</p>
					</div>
					@endforeach
				</div>
			</div>
			{{ $listBooks->links() }}
			<div class="col-md-1"></div>
		</div>

	</div>
</section>
@stop
@section('scriptAdd')
<script type="text/javascript">

</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection