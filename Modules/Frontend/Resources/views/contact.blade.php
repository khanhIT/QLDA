@extends('frontend::layouts.master')
@section('title')
    Liên hệ
@endsection
@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/contact.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
@endsection
@section('content')
<section id="contact">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				@if (session('info'))
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						{{session('info')}}
					</div>
				@endif
				<h1 class="text-center" style="margin-bottom: 50px;">BỘ MÔN TIN TRẮC ĐỊA</h1>
				<div class="col-md-5">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">Thông Tin</h3>
						</div>
						<div class="panel-body">
							<p><span class="fa fa-map-marker"></span> Phòng 7.09, Tầng 7 - Nhà C 12 tầng, Trường Đại học Mỏ-Địa chất</p>
							<p><span class="fa fa-phone"></span> Văn phòng: 043 755 1112</p>
							<p><span class="fa fa-phone-square"></span> Cố vấn học tập: 0983 448 779</p>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">Liên Hệ</h3>
						</div>
						<div class="panel-body">
							<form action="" method="POST">
								{{csrf_field()}}
								<div class="col-md-6 form-group" {{ $errors->has('name') ? 'has-error' : '' }}>
									<input type="text" name="name" class="form-control" placeholder="Họ Và Tên..." value="{{old('name')}}">
									<span class="text-danger">{{ $errors->first('name') }}</span>
								</div>
								<div class="col-md-6 form-group" {{ $errors->has('address') ? 'has-error' : '' }}>
									<input type="text" name="address" class="form-control" placeholder="Nhập địa chỉ..." value="{{old('address')}}">
									<span class="text-danger">{{ $errors->first('address') }}</span>
								</div>
								<div class="col-md-12 form-group" {{ $errors->has('email') ? 'has-error' : '' }}>
									<input type="email" name="email" class="form-control" placeholder="Nhập đúng email đã đăng ký đồ án để liên hệ giáo viên hướng dẫn" value="{{old('email')}}">
									<span class="text-danger">{{ $errors->first('email') }}</span>
								</div>

								<div class="col-md-12 form-group" {{ $errors->has('noidung') ? 'has-error' : '' }}>
									<textarea name="noidung" id="input" class="form-control" rows="4" placeholder="Nội Dung...">{{old('noidung')}}</textarea>
									<span class="text-danger">{{ $errors->first('noidung') }}</span>
								</div>
								<button type="submit" class="btn btn-primary col-md-3">Gửi</button>
							</form>
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
@stop
@section('scriptAdd')
<script type="text/javascript">

</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
@endsection