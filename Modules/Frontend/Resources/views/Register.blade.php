@extends('frontend::layouts.master')
@section('title')
    Đăng ký đồ án
@endsection
@section('stylesheet')
	<style type="text/css">
		#menu{
			display: none;
		}

		.Register,
		.Register::after {
		  -webkit-transition: all 0.3s;
			-moz-transition: all 0.3s;
		  -o-transition: all 0.3s;
			transition: all 0.3s;
		}

		.Register {
		  background: none;
		  border: 3px solid #fff;
		  border-radius: 5px;
		  color: #fff;
		  display: block;
		  font-size: 1.6em;
		  font-weight: bold;
		  margin: 1em auto;
		  padding: 2em 6em;
		  position: relative;
		  text-transform: uppercase;
		}

		.Register::before,
		.Register::after {
		  background: #fff;
		  content: '';
		  position: absolute;
		  z-index: -1;
		}

		.Register:hover {
		  color: #2ecc71;
		}

		.Register {
		  overflow: hidden;
		}

		.Register::after {
		  background-color: #6d8484;
		  height: 100%;
		  left: -35%;
		  top: 0;
		  transform: skew(50deg);
		  transition-duration: 0.6s;
		  transform-origin: top left;
		  width: 0;
		}

		.Register:hover:after {
		  height: 100%;
		  width: 135%;
		}
	</style>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}">
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="title">
				<h1 class="text-center">ĐĂNG KÝ ĐỒ ÁN TỐT NGHIỆP</h1>
			</div>
			<div class="clearfix"></div>
		    @if (session('info'))
		        <div class="alert alert-info">
		            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {{session('info')}}
		        </div>
		    @endif
			<div class="col-md-1"></div>
			<div class="col-md-10 form_register">
				<form action="" method="POST" role="form">
                    {{ csrf_field() }}
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('fullname') ? 'has-error' : '' }}">
							<label for="">Họ và Tên</label>
							<input type="text" class="form-control" name="fullname" placeholder="Nhập Họ và tên" value="{{ old('fullname') }}"  @if($errors->has('fullname')) autofocus @elseif($errors->has('email')) @elseif($errors->has('msv')) @elseif($errors->has('phone')) @elseif($errors->has('birthday')) @elseif($errors->has('address')) @elseif($errors->has('l_id')) @elseif($errors->has('project_name')) @elseif($errors->has('c_id')) @elseif($errors->has('s_id')) @else autofocus @endif >
							<span class="text-danger">{{ $errors->first('fullname') }}</span>
						</div>
						<div class="form-group @if (session('email_error'))  has-error    @endif {{ $errors->has('email') ? 'has-error' : '' }}">
							<label for="">Email</label>
							<input type="email" class="form-control" name="email" placeholder="Nhập Email" value="{{ old('email') }}" @if( $errors->has('email')) autofocus @endif>
							<span class="text-danger">@if (session('email_error')){{ session('email_error') }}@endif{{ $errors->first('email') }}</span>
						</div>
						<div class="form-group @if (session('msv_error'))  has-error    @endif {{ $errors->has('msv') ? 'has-error' : '' }}">
							<label for="">Mã Số Sinh Viên</label>
							<input type="number" class="form-control" name="msv" placeholder="Nhập Mã Sinh Viên" value="{{ old('msv') }}" @if( $errors->has('msv')) autofocus @endif>
							<span class="text-danger">@if (session('msv_error')){{ session('msv_error') }}@endif{{ $errors->first('msv') }}</span>
						</div>
						<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
							<label for="">Nhập Số Điện Thoại</label>
							<input type="number" class="form-control" name="phone" placeholder="Nhập Số điện thoại" value="{{ old('phone') }}" @if( $errors->has('phone')) autofocus @endif>

							<span class="text-danger">{{ $errors->first('phone') }}</span>
						</div>
						<div class="form-group {{ $errors->has('birthday') ? 'has-error' : '' }}">
							<label for="">Ngày Sinh</label>
							<input type="date" class="form-control" name="birthday" placeholder="Nhập ngày sinh" value="{{ old('birthday') }}" @if( $errors->has('birthday')) autofocus @endif>
							<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('birthday') }}</span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
							<label for="">Địa Chỉ</label>
							<input type="text" class="form-control" name="address" placeholder="Nhập địa chỉ" value="{{ old('address') }}" @if( $errors->has('address')) autofocus @endif>
							<span class="text-danger">{{ $errors->first('address') }}</span>
						</div>
						<div class="form-group {{ $errors->has('l_id') ? 'has-error' : '' }}">
							<label for="">Giáo Viên Hướng Dẫn</label>
							<select name="l_id" id="l_id" class="form-control" @if( $errors->has('l_id')) autofocus @endif>
								<option value="">Chọn giáo viên hướng dẫn</option>
								@foreach($dataLecturers as $lecturer)
									<option {{ old('l_id') == $lecturer['id'] ? "selected" : "" }} value="{{$lecturer->id}}">{{$lecturer->l_fullname}}</option>
								@endforeach
							</select>
							<span class="text-danger">@if (session('gv_error')){{ session('gv_error') }}@endif {{ $errors->first('l_id') }}</span>
						</div>
						<div class="form-group @if (session('error'))  has-error    @endif {{ $errors->has('project_name') ? 'has-error' : '' }}">
							<label for="">Tên đồ án có sẵn</label>
							<select name="project_name" id="tendoan" class="form-control checkclick" @if( $errors->has('project_name')) autofocus @endif>
								<option value="">--Chọn đồ án--</option>
								@foreach($dataListProject as $listProject)
									<option {{ old('project_name') == $listProject['id'] ? "selected" : "" }}  value="{{$listProject->name}}">{{$listProject->name}}</option>
								@endforeach
							</select>
							<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('project_name') }}</span>
						</div>
						<div class="form-group @if (session('project_error'))  has-error    @endif {{ $errors->has('project_name') ? 'has-error' : '' }}">
							<label for="">Nhập tên đồ án tự chọn</label>
							<input type="text" class="form-control" id="disabled_input"  name="project_name" placeholder="Nhập Tên đồ án bảo vệ" value="{{ old('project_name') }}" @if(session('msv_error') ||session('project_error') || session('email_error') ) disabled @endif>
							<span class="text-danger">@if (session('project_error')){{ session('project_error') }}@endif{{ $errors->first('project_name') }}</span>
						</div>

						<div class="col-md-6" style="margin-left: -15px">
							<div class="form-group {{ $errors->has('c_id') ? 'has-error' : '' }}">
								<label for="">Lớp Chuyên ngành</label>
								<select name="c_id" id="input" class="form-control" @if( $errors->has('c_id')) autofocus @endif>
									<option value="">--Chọn lớp chuyên ngành--</option>
									@foreach($dataClassName as $class)
										<option {{ old('c_id') == $class['id'] ? "selected" : "" }} value="{{$class->id}}">{{$class->c_name}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('c_id') }}</span>
							</div>
							
						</div>
						<div class="col-md-6" style="margin-right: -30px;">
							<div class="form-group {{ $errors->has('s_id') ? 'has-error' : '' }}">
								<label for="">Khóa học</label>
								<select name="s_id" id="input" class="form-control" @if( $errors->has('s_id')) autofocus @endif>
									<option value="">--Chọn khóa học--</option>
									@foreach($dataSchoolYear as $year)
										<option {{ old('s_id') == $year['id'] ? "selected" : "" }} value="{{$year->id}}">K{{$year->s_key}}</option>
									@endforeach
								</select>
								<span class="text-danger">{{ $errors->first('s_id') }}</span>
							</div>
						</div>

					</div>
						<button type="submit" class="btn btn-primary col-sm-6 Register">
						  Đăng Ký
						</button>
				</form>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
</div>
@stop


@section('scriptAdd')
	<script type="text/javascript">
        $(document).ready(function() {
            $("#tendoan").change(function() {
               var val = $(this).val();
               if(val != '') {
                    //alert(1234567);
                  $("#disabled_input").prop("disabled", true)}
                  else{
                   $("#disabled_input").prop("disabled", false)
			   }
            });
            $('#disabled_input').click(function () {
                    $('#tendoan').prop('disabled',true);
            });
			//list ra tên project theo giáo viên
            $("#l_id").change(function() {
                var idLisProject =$(this).val();

                $.get("name/" + idLisProject, function (data) {
                    $('#tendoan').html(data);
                });
            });
            });
</script>
@endsection
