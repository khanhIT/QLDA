<?php

namespace Modules\Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LecturersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'l_fullname' => 'required',
            'l_username' => 'required',
            'l_address' => 'required',
            'l_email' => 'required|email',
            'l_phone' => 'required',
            'l_birthday' => 'required',
            'staff' => 'required',
            'p_id' => 'required',
            'd_id' => 'required',
            'info' => 'required',
            'marride' => 'required',
            'gender' => 'required',
            'link_url' => 'required'
        ];
    }

    public function messages(){
        return [
            'l_fullname.required' => 'Vui lòng nhập họ và tên',
            'l_username.required' => 'Vui lòng nhập tên',
            'l_address.required' => 'Vui lòng nhập địa chỉ',
            'l_email.required' => 'Vui lòng nhập email',
            'l_email.email' => 'Vui lòng nhập đúng định đạng email',
            'l_phone.required' => 'Vui lòng nhập số điện thoại',
            'l_birthday.required' => 'Vui lòng nhập ngày sinh',
            'p_id.required' => 'Vui lòng nhập chức vụ',
            'd_id.required' => 'Vui lòng nhập bằng cấp',
            'info.required' => 'Vui lòng nhập thông tin',
            'marride.required' => 'Vui lòng chọn tình trạng hôn nhân',
            'gender.required' => 'Vui lòng chọn giới tính',
            'staff.required' => 'Vui lòng chọn hướng dẫn',
            'link_url.required' => 'Vui lòng nhập link_url'
        ];
    }

    
}
