<?php

namespace Modules\Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustormerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'msv' => 'required',
            'fullname' => 'required',
            'phone' => 'required',
            'birthday' => 'required',
            'project_name' => 'required',
            'c_id' => 'required',
            'address' => 'required',
            's_id' => 'required',
            'l_id' => 'required'
        ];
    }

    public function messages(){
        return [
            'msv.required' => 'Vui lòng nhập mã sinh viên',
            'fullname.required' => 'Vui lòng nhập họ và tên',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'birthday.required' => 'Vui lòng chọn ngày sinh',
            'project_name.required' => 'Vui lòng nhập tên đề tài',
            'address.required' => 'Vui lòng nhập địa chỉ',
            'c_id.required' => 'Vui lòng chọn lớp',
            's_id.required' => 'Vui lòng chọn khóa',
            'l_id.required' => 'Vui lòng chọn tên giảng viên hướng dẫn',
        ];
    }
}
