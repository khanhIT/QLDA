<?php

namespace Modules\Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassNameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'c_name'=>'required',
            'description'=>'required'
        ];
    }

    public function messages(){
        return [
            'c_name.required' => 'Vui lòng nhập tên lớp',
            'description.required' => 'Vui lòng nhập mô tả'
        ];
    }
}
