<?php

namespace Modules\Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'role_id' => 'required',
          'u_fullname' => 'required',
          'u_name' => 'required',
          'u_email' => 'required',
          'password' => 'required',
          'avatar' => 'image',
          'mobile' => 'required',
          'skype' => 'required',
          'birthday' => 'required',
          'gender' => 'required',
          'married' => 'required',
          'address' => 'required',
        ];
    }

    public function messages(){
        return [
          'role_id.required' => 'Vui lòng chọn quyền',
          'u_fullname.required' => 'Vui lòng nhập họ và tên',
          'u_name.required' => 'Vui lòng nhập tên',
          'u_email.required' => 'Vui lòng nhập email',
          'password.required' => 'Vui lòng nhập mật khẩu',
          'avatar.image' => 'Vui lòng chọn đúng file ảnh',
          'mobile.required' => 'Vui lòng nhập số điện thoại',
          'skype.required' => 'Vui lòng nhập tên skype',
          'birthday.required' => 'Vui lòng chọn ngày sinh',
          'gender.required' => 'Vui lòng chọn giới tính',
          'married.required' => 'Vui lòng chọn tình trạng hôn nhân',
          'address.required' => 'Vui lòng nhập địa chỉ',
        ];
    }
}
