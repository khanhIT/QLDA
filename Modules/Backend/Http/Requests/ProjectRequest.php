<?php

namespace Modules\Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_id' => 'required',
            'u_fullname' => 'required',
            'u_name' => 'required',
            'u_email' => 'required',
            'password' => 'required',
            'avatar' => 'required|image',
            'mobile' => 'required',
            'skype' => 'required',
            'birthday' => 'required',
            'gender' => 'required',
            'married' => 'required',
            'address' => 'required',
            'file_upload_word' => 'required',
            'upload_source_code' => 'required'
        ];
    }

    public function messages(){
        return [
            'role_id.required' => 'Vui lòng chọn quyền',
            'u_fullname.required' => 'Vui lòng nhập họ và tên',
            'u_name.required' => 'Vui lòng nhập tên',
            'u_email.required' => 'Vui lòng nhập email',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'avatar.required' => 'Vui lòng chọn ảnh',
            'avatar.image' => 'Vui lòng chọn đúng file ảnh',
            'mobile.required' => 'Vui lòng nhập số điện thoại',
            'skype.required' => 'Vui lòng nhập tên skype',
            'birthday.required' => 'Vui lòng chọn ngày sinh',
            'gender.required' => 'Vui lòng chọn giới tính',
            'married.required' => 'Vui lòng chọn tình trạng hôn nhân',
            'address.required' => 'Vui lòng nhập địa chỉ',
            'file_upload_word.required' => 'Vui lòng chọn file word',
            'upload_source_code.required' => 'Vui lòng chọn file source_code',
        ];
    }
}
