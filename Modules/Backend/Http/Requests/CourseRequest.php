<?php

namespace Modules\Backend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_key' => 'required',
            'year_start' => 'required',
            'year_end' => 'required',
            'total_student' => 'required'
        ];
    }

    public function messages(){
        return [
            's_key.required' => 'Vui lòng nhập tên khóa học',
            'year_start.required' => 'Vui lòng nhập năm bắt đầu',
            'year_end.required' => 'Vui lòng nhập tên năm kết thúc',
            'total_student.required' => 'Vui lòng nhập tổng sinh viên',
        ];
    }
}
