<?php

//admin
Route::group(['middleware' => 'web', 'prefix' => 'admin', 'namespace' => 'Modules\Backend\Http\Controllers'], function()
{
    Route::get('/', function (){
    return redirect('admin/login');
});
	//trang chủ
    Route::get('/dashboard', 'BackendController@index')->name('dashboard');

    

    //login
    Route::get('/login','Auth\LoginController@login')->name('login');
    Route::post('/login','Auth\LoginController@postLogin');
    Route::get('/logout', 'Auth\LoginController@logout' ,function () {
        return redirect('admin/login');
    })->name('admin-logout');

    // Router Users 
    Route::get('/users','UserController@index')->name('users-index');
    Route::get('/users/edit/{id}','UserController@edit')->name('users-edit');
    Route::post('/users/edit/{id}','UserController@update');
    Route::get('/users/show/{id}','UserController@show')->name('users-show');
    Route::get('/users/create','UserController@create')->name('users-create');
    Route::post('/users/create','UserController@store');
    Route::get('/users/delete/{id}','UserController@destroy')->name('users-destroy');

    //Router Project
    Route::get('do-an','ProjectController@index')->name('project-index');
    Route::get('/do-an/edit/{id}','ProjectController@edit')->name('project-edit');
    Route::post('/do-an/edit/{id}','ProjectController@update');
    Route::get('/do-an/show/{id}','ProjectController@show')->name('project-show');
    Route::get('/do-an/exportHĐ1','ProjectController@exportHĐ1')->name('exportHĐ1');
    Route::get('/do-an/exportHĐ2','ProjectController@exportHĐ2')->name('exportHĐ2');
    Route::get('/do-an/exportHĐ3','ProjectController@exportHĐ3')->name('exportHĐ3');
    Route::get('/do-an/exportHĐ4','ProjectController@exportHĐ4')->name('exportHĐ4');
    Route::get('/do-an/exportHĐ5','ProjectController@exportHĐ5')->name('exportHĐ5');
    Route::get('do-an/file_word/download/{id}', 'ProjectController@downloadFile')->name('downloadFile');

    //Router Lecturers
    Route::get('giang-vien','LecturerController@index')->name('lecturers-index');
    Route::get('/giang-vien/edit/{id}','LecturerController@edit')->name('lecturers-edit');
    Route::post('/giang-vien/edit/{id}','LecturerController@update');
    Route::get('/giang-vien/show/{id}','LecturerController@show')->name('lecturers-show');
    Route::get('/giang-vien/create','LecturerController@create')->name('lecturers-create');
    Route::post('/giang-vien/create','LecturerController@store');
    Route::get('/giang-vien/delete/{l_id}','LecturerController@destroy')->name('lecturers-destroy');

    //Router Course
    Route::get('/course','CourseController@index')->name('course-index');
    Route::get('/course/edit/{id}','CourseController@edit')->name('course-edit');
    Route::post('/course/edit/{id}','CourseController@update');
    Route::get('/course/create','CourseController@create')->name('course-create');
    Route::post('/course/create','CourseController@store');
    Route::get('/course/delete/{id}','CourseController@destroy')->name('course-destroy');

    //Router ClassRoom
    Route::get('/classroom','ClassRoomController@index')->name('classroom-index');
    Route::get('/classroom/edit/{id}','ClassRoomController@edit')->name('classroom-edit');
    Route::post('/classroom/edit/{id}','ClassRoomController@update');
    Route::get('/classroom/show','ClassRoomController@show')->name('classroom-show');
    Route::get('/classroom/create','ClassRoomController@create')->name('classroom-create');
    Route::post('/classroom/create','ClassRoomController@store');
    Route::get('/classroom/delete/{id}','ClassRoomController@destroy')->name('classroom-destroy');

    //Router Position (chức vụ)
    Route::get('chuc-vu','PositionController@index')->name('position-index');
    Route::get('/chuc-vu/edit/{id}','PositionController@edit')->name('position-edit');
    Route::post('/chuc-vu/edit/{id}','PositionController@update');
    Route::get('/chuc-vu/show','PositionController@show')->name('position-show');
    Route::post('/chuc-vu/show','PositionController@store');
    Route::get('/chuc-vu/delete/{id}','PositionController@destroy')->name('position-destroy');

    //Router Degree (bằng cấp)
    Route::get('bang-cap','DegreeController@index')->name('degree-index');
    Route::get('/bang-cap/edit/{id}','DegreeController@edit')->name('degree-edit');
    Route::post('/bang-cap/edit/{id}','DegreeController@update');
    Route::get('/bang-cap/show','DegreeController@show')->name('degree-show');
    Route::post('/bang-cap/show','DegreeController@store');
    Route::get('/bang-cap/delete/{id}','DegreeController@destroy')->name('degree-destroy');

    //Router ListProject
    Route::get('danh-sach-de-tai','ListProjectAdminController@index')->name('ListProjects_index');
    Route::get('/danh-sach-de-tai/edit/{id}','ListProjectAdminController@edit')->name('ListProjects_edit');
    Route::post('/danh-sach-de-tai/edit/{id}','ListProjectAdminController@update');
    Route::get('/danh-sach-de-tai/create','ListProjectAdminController@show')->name('ListProjects_show');
    Route::post('/danh-sach-de-tai/create','ListProjectAdminController@store')->name('ListProjects_store');
    Route::get('/danh-sach-de-tai/delete/{id}','ListProjectAdminController@destroy')->name('ListProjects_destroy');

    //Router Student
    Route::get('/student','StudentController@index')->name('students-index');
    Route::get('/student/edit/{id}','StudentController@edit')->name('students-edit');
    Route::post('/student/edit/{id}','StudentController@update');
    Route::get('/student/show/{id}','StudentController@show')->name('students-show');
    Route::get('student/export','StudentController@studentExport')->name('student-export');
    Route::post('student/import','StudentController@postImport')->name('importStudent');

    //emailRegister
    Route::get('emailRegister/danh-sach','EmailRegisterController@index')->name('listEmail');
    Route::get('emailRegister/mail/send','EmailRegisterController@sendMail')->name('sendMail');
    Route::post('emailRegister/mail/send','EmailRegisterController@postSendMail');

    //slide
    Route::get('slide/danh-sach','SlideController@index')->name('listSlide');
    Route::get('slide/them-moi','SlideController@add')->name('addSlide');
    Route::post('slide/them-moi','SlideController@postAdd')->name('addSlide');
    Route::get('slide/sua/{id}','SlideController@edit')->name('editSlide');
    Route::post('slide/sua/{id}','SlideController@postEdit')->name('editSlide');
    Route::get('/slide/delete/{id}','SlideController@destroy')->name('slide-destroy');

    //typical_student ( sinh viên tiêu biểu )
    Route::get('sinh-viên-tiêu-biểu/danh-sach','TypicalStudentController@index')->name('listTypicalStudent');
    Route::get('sinh-viên-tiêu-biểu/them-moi','TypicalStudentController@add')->name('addTypicalStudent');
    Route::post('sinh-viên-tiêu-biểu/them-moi','TypicalStudentController@postAdd')->name('addTypicalStudent');
    Route::get('sinh-viên-tiêu-biểu/sua/{id}','TypicalStudentController@edit')->name('editTypicalStudent');
    Route::post('sinh-viên-tiêu-biểu/sua/{id}','TypicalStudentController@postEdit')->name('editTypicalStudent');
    Route::get('/sinh-viên-tiêu-biểu/delete/{id}','TypicalStudentController@destroy')->name('TypicalStudent-destroy');

    //memorial_photo ( hình ảnh kỷ niệm )
    Route::get('hinh-anh-ky-niem/danh-sach','MemorialPhotoController@index')->name('listMemorialPhoto');
    Route::get('hinh-anh-ky-niem/them-moi','MemorialPhotoController@add')->name('addMemorialPhoto');
    Route::post('hinh-anh-ky-niem/them-moi','MemorialPhotoController@postAdd')->name('addMemorialPhoto');
    Route::get('hinh-anh-ky-niem/sua/{id}','MemorialPhotoController@edit')->name('editMemorialPhoto');
    Route::post('hinh-anh-ky-niem/sua/{id}','MemorialPhotoController@postEdit')->name('editMemorialPhoto');
    Route::get('/hinh-anh-ky-niem/delete/{id}','MemorialPhotoController@destroy')->name('MemorialPhoto-destroy');

    //contact
    Route::get('/lien-he/danh-sach','ContactController@index')->name('listContact');
    Route::get('/lien-he/tra-loi/{id}','ContactController@edit')->name('replyContact');
    Route::post('/lien-he/tra-loi/{id}','ContactController@update');
    //books
    Route::get('/sach/danh-sach','BookController@index')->name('listBooks');
    Route::get('/sach/them-moi','BookController@create')->name('addBooks');
    Route::post('/sach/them-moi','BookController@store');
    Route::get('/sach/chinh-sua/{id}','BookController@edit')->name('editBooks');
    Route::post('/sach/chinh-sua/{id}','BookController@update');
    Route::get('/sach/xem/{id}','BookController@view')->name('viewBooks');
    Route::get('/sach/xoa/{id}','BookController@destroy')->name('deleteBooks');
    //news (tin-tức)
    Route::get('tin-tuc/danh-sach','NewsController@index')->name('news-index');
    Route::get('tin-tuc/them-moi','NewsController@create')->name('news-create');
    Route::post('tin-tuc/them-moi','NewsController@store');
    Route::get('tin-tuc/show/{id}','NewsController@show')->name('news-show');
    Route::get('tin-tuc/edit/{id}','NewsController@edit')->name('news-edit');
    Route::post('tin-tuc/edit/{id}','NewsController@update');
    Route::get('tin-tuc/delete/{id}','NewsController@destroy')->name('news-destroy');
});



//giáo viên
Route::group(['middleware' => 'web', 'prefix' => 'assist', 'namespace' => 'Modules\Backend\Http\Controllers'], function()
{
   Route::get('/', function (){
        return redirect('assist/login');
    });

   //trang chủ
   //Route::get('giaovien', 'DashBoardController@index')->name('dashboard');

   //Router Student
    Route::get('sinh-vien','SubStudentController@index')->name('student_index');
    Route::get('/sinh-vien/edit/{id}','SubStudentController@edit')->name('student_edit');
    Route::post('/sinh-vien/edit/{id}','SubStudentController@update');
    Route::get('/sinh-vien/show/{id}','SubStudentController@show')->name('student_show');
   
   //Router ListProject
    Route::get('danh-sach-de-tai','ListProjectController@index')->name('ListProject-index');
    Route::get('/danh-sach-de-tai/edit/{id}','ListProjectController@edit')->name('ListProject-edit');
    Route::post('/danh-sach-de-tai/edit/{id}','ListProjectController@update');
    Route::get('/danh-sach-de-tai/show','ListProjectController@show')->name('ListProject-show');
    Route::post('/danh-sach-de-tai/create','ListProjectController@store')->name('ListProject-store');
    Route::get('/danh-sach-de-tai/delete/{id}','ListProjectController@destroy')->name('ListProject-destroy');
    Route::post('/danh-sach-de-tai/import','ListProjectController@postImport')->name('importListProject');

   //logout
    Route::get('/login','Auth\LoginController@login')->name('login');
    Route::post('/login','Auth\LoginController@postLogin');
    Route::get('/logout', 'Auth\LoginController@logout', function (){
        return redirect('admin/login');
    })->name('logout-assist');
});