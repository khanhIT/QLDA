<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Users\UsersRepository;
use Illuminate\Support\Facades\Auth;

class DashBoardController extends Controller
{
    public function __construct(
        UsersRepository $user
    )
    {
        $this->user = $user;
    }

    public function index()
    {
        return view('backend::index');
    }
}
