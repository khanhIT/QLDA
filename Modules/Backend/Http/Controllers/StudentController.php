<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Custormer\CustormerRepository;
use Common\Repositories\Lecturers\LecturersRepository;
use Common\Repositories\SchoolYear\SchoolYearRepository;
use Common\Repositories\ClassName\ClassNameRepository;
use Modules\Backend\Http\Requests\CustormerRequest;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{

    public function __construct(

        CustormerRepository $custormer,
        LecturersRepository $lec,
        SchoolYearRepository $school,
        ClassNameRepository $className

    ){

        $this->custormer = $custormer;
        $this->lec = $lec;
        $this->school = $school;
        $this->className = $className;

    }
    
    public function index()
    {
        $custormer = $this->custormer->listIndex();
        return view('backend::students.index')->with(['custormer' => $custormer]);
    }

   
    public function show($id)
    {

        $custormer = $this->custormer->listShow($id);
    
        return view('backend::students.view')->with(['custormer' => $custormer]);
    }

   
    public function edit($id)
    {
        $custormer = $this->custormer->listView($id);
        $lec = $this->lec->all('id');
        $school = $this->school->all('id'); 
        $className = $this->className->all('id'); 
        return view('backend::students.edit')->with(['custormer' => $custormer, 'lec' => $lec, 'school' => $school, 'className' => $className]);
    }

    
    public function update(CustormerRequest $request, $id)
    {
        $custormer = [

            'msv' => $request->msv,
            'fullname' => $request->fullname,  
            'phone' => $request->phone,
            'birthday' => $request->birthday,
            'address' => $request->address,
            'project_name' => $request->project_name,
            'c_id' => $request->c_id,
            's_id' => $request->s_id,
            'l_id' => $request->l_id

        ];

        $this->custormer->update($custormer, $id, 'id');
        return redirect()->route('students-index')->with(['info' => sprintf(MSG008,'sinh viên')])->withInput();
    }

   
   public function studentExport()
   {

       $custormer = $this->custormer->listExport();
       
       $custormerArray = [];

       $custormerArray[] = ['id', 'msv', 'email', 'fullname', 'phone', 'birthday', 'address', 'project_name', 's_key', 'c_name', 'l_fullname', 'name'];

       foreach ($custormer as $cus) {
        $custormerArray[] = $cus->toArray();
       }
       
       Excel::create('Danh sách sinh viên', function($excel) use ($custormer){

           $excel->sheet('Danh sách sinh viên',function($sheet) use ($custormer){

              $sheet->fromArray($custormer, null, 'A5', false, true);
              $sheet->mergeCells('A1:B1');
              $sheet->cell('A1', function($cell) {
              $cell->setValue('');

              // Set black background
              //$cell->setBackground('red');

              // Set font
              $cell->setFont([
                'family'     => 'Arial',
                'size'       => '11',
                'bold'       =>  true
              ]);

                // Set all borders (top, right, bottom, left)
              $cell->setBorder('solid', 'none', 'none', 'solid');

            });

            $sheet->mergeCells('D2:H2'); 
            $sheet->cell('D2', function ($cell) {
                $cell->setValue('DANH SÁCH SINH VIÊN BẢO VỆ TỐT NGHIỆP CHUYÊN NGÀNH TIN HỌC TRẮC ĐỊA NĂM HỌC 2017-2018');
                $cell->setFontWeight('bold');
            });  

           });

       })->export('xlsx');

       return redirect()->route('students-index')->with('info','Bạn đã xuất file thành công');

   }


   public function postImport(Request $request)
   {
    $file = $request->file('import');
    $filename = $file->getClientOriginalName();
    //get data on import file
    $excelData = \Excel::load($file, function ($reader) {
          $results = $reader->get();
        })->get();

    try{

      \DB::beginTransaction();
      $importResult = $this->custormer->importFromExcelCustormer($excelData, $filename);

      $logData = [
        'countRowSuccess' => $importResult['countRowInsertSuccess'],
        'totalRow' => $importResult['totalRow'],
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
      ];

      \DB::commit();
      return redirect(route('students-index'));
    } catch (\Exception $e) {
      \DB::rollback();
      return $e->getMessage();
    }

   }

}
