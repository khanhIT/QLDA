<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Books\BooksResponsitory;
use Common\upload;
class BookController extends Controller
{
    public function __construct(BooksResponsitory $books){
        $this->books = $books;
    }
    public function index()
    {
        $listBooks=$this->books->all('id desc');
        return view('backend::books.index',compact('listBooks'));
    }
    public function create()
    {
        return view('backend::books.add');
    }
    public function store(Request $request)
    {
        $books = [
            'title' => $request->title,
            'edition' => $request->edition,
            'year' => $request->year,
            'author' => $request->author,
            'isbn' => $request->isbn,
            'publisher' => $request->publisher,
            'abstract' => $request->abstract
        ];
        if ($request->hasFile('cover')) {
            $books['cover'] = Upload::uploadFile('cover', $request, BOOK_UPLOAD_PATH);
        }
        if ($request->hasFile('inside')) {
            $books['inside'] = Upload::uploadFile('inside', $request, BOOK_INSIDE_UPLOAD_PATH);
        }
        if ($this->books->create($books)){
            return redirect()->route('listBooks')->with(['info' => sprintf(MSG007, 'Books')])->withInput();
        }else{
            return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
        }
    }
    public function edit($id)
    {
        $editBooks=$this->books->findBy('id',$id);
        return view('backend::books.edit',compact('editBooks'));
    }
    public function view($id)
    {
        $viewBooks=$this->books->findBy('id',$id);
        return view('backend::books.view',compact('viewBooks'));
    }

    public function update(Request $request,$id)
    {
        $books = [
            'title' => $request->title,
            'edition' => $request->edition,
            'year' => $request->year,
            'author' => $request->author,
            'isbn' => $request->isbn,
            'publisher' => $request->publisher,
            'abstract' => $request->abstract
        ];

        if ($request->hasFile('cover')) {
            $books['cover'] = Upload::uploadFile('cover', $request, BOOK_UPLOAD_PATH);
        }
        if ($request->hasFile('inside')) {
            $books['inside'] = Upload::uploadFile('inside', $request, BOOK_INSIDE_UPLOAD_PATH);
        }

        $this->books->update($books, $id, 'id');
        return redirect()->route('listBooks')->with(['info' => sprintf(MSG008,'sách')])->withInput();
    }
    public function destroy($id)
    {
        $data = $this->books->delete($id);
        return "true";
    }
}
