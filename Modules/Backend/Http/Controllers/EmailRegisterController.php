<?php

namespace Modules\Backend\Http\Controllers;

use App\Mail\ThongBaoTuHeThong;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\EmailRegister\EmailRegisterRepository;
use Modules\Backend\Http\Requests\EmailRegisterRequest;
use File;
use Mail;
use DB;
class EmailRegisterController extends Controller
{
	public function __construct(
        EmailRegisterRepository $email
	)
	{
        $this->emailRegister = $email;
	}

    public function index(){
    	$emailRegister = $this->emailRegister->all('id desc');
        return view('backend::emailRegister.index',compact('emailRegister'));
    }

    public function sendMail(){
        return view('backend::emailRegister.sendMail');
    }

    public function postSendMail(EmailRegisterRequest $request)
    {
        $title=$request->title;
        $content=$request->noidung;
       
        dispatch(new \App\Jobs\SendMailCustomer($title,$content));
        return redirect()->route('listEmail')->with(['info' => sprintf(MSG007, 'Gửi Email')])->withInput();
    }

    public function destroy($id)
    {

    }


    public function search(Request $request)
    {
    }
}
