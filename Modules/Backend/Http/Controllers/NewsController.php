<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\News\NewsRepository;
use Modules\Backend\Http\Requests\NewsRequest;
use Common\upload;

class NewsController extends Controller
{

    public function __construct
    (

        NewsRepository $news

    ){
     
        $this->news = $news;

    }
   
    public function index()
    {
 
        $news = $this->news->all('id desc');

        return view('backend::news.index')->with(['news' => $news]);
    }

    public function create()
    {
        return view('backend::news.add');
    }

  
    public function store(NewsRequest $request)
    {
        $news = [

            'title' => $request->title,

            'content' => $request->content,

            'image' => Upload::uploadFile('image', $request, NEWS_UPLOAD_PATH),

            'image_content' => Upload::uploadFile('image_content', $request, NEWS_UPLOAD_PATH)

        ];

        if ($this->news->create($news)){
            return redirect()->route('news-index')->with(['info' => sprintf(MSG007, 'Tin tức')])->withInput();
        }else{
           return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }

  
    public function show($id)
    {
        $news = $this->news->findBy('id', $id);
        return view('backend::news.view')->with(['news' => $news]);
    }

   
    public function edit($id)
    {
        $news = $this->news->findBy('id', $id);
        return view('backend::.news.edit')->with(['news' => $news]);
    }

   
    public function update(NewsRequest $request, $id)
    {
        $news = [

            'title' => $request->title,

            'content' => $request->content

        ];

        if ($request->hasFile('image')) {
            $news['image'] = Upload::uploadFile('image', $request, NEWS_UPLOAD_PATH);
        }

        if ($request->hasFile('image_content')) {
            $news['image_content'] = Upload::uploadFile('image_content', $request, NEWS_UPLOAD_PATH);
        }


        $this->news->update($news, $id, 'id');
        return redirect()->route('news-index')->with(['info' => sprintf(MSG008,'tin tức')])->withInput();
    }

   
    public function destroy($id)
    {
         $data = $this->news->delete($id);
            return "true";
    }
}
