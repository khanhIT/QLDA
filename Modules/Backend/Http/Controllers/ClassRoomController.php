<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\ClassName\ClassNameRepository;
use Modules\Backend\Http\Requests\ClassNameRequest;

class ClassRoomController extends Controller
{
    public function __construct(
        ClassNameRepository $className
    ){

        $this->className = $className;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $list_class=$this->className->all('id desc');
        return view('backend::classRoom.index',compact('list_class'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('backend::classRoom.add');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ClassNameRequest $request)
    {
        $add_className =
            [
                'c_name' => $request->c_name,
                'description' => $request->description

            ];
        if ($this->className->create($add_className)){
            return redirect()->route('classroom-index')->with(['info' => sprintf(MSG007, 'Lớp ')])->withInput();
        }else{
            return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('backend::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $edit_class=$this->className->findBy('id',$id);
        return view('backend::classRoom.edit',compact('edit_class'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(ClassNameRequest $request,$id)
    {
        $update_class=[
            'c_name'=>$request->c_name,
            'description'=>$request->description
        ];
        if ($this->className->update($update_class,$id,'id')){
            return redirect()->route('classroom-index')->with(['info' => sprintf(MSG008, 'Lớp chuyên ngành')])->withInput();
        }else{
            return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $data = $this->className->delete($id);
        return "true";
    }
}
