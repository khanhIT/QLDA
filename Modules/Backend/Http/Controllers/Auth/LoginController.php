<?php

namespace Modules\Backend\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Backend\Http\Requests\LoginRequest;
use Common\Repositories\Users\UsersRepository;
use Common\Repositories\Custormer\CustormerRepository;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct(
        UsersRepository $user,
        CustormerRepository $cus
    )
    {
        $this->middleware(function ($request, $next) {
            $auth = Auth::user();
            if ($auth != null) {
                if($auth->role_id == 1){
                    return $next($request);
                }elseif($auth->role_id == 2) {
                    return redirect()->route('student-index');
                }
            }else{
                return redirect('admin/login');
            }
        })->only('giaovien');

        $this->user = $user;
        $this->cus = $cus;
    }

    public function login()
    {
       
        return view('backend::layouts.login');
    }

    public function postLogin(LoginRequest $request)
    {

       $dataLogin = [
            ['u_name', '=', $request->u_name],
            ['role_id', '!=', 0]
        ];
        $user = $this->user->getManyWhereOrderLimit($dataLogin);
        $rules = $request->rules();
        if($request->validated($rules)) {
            $password = $request->password;
            if ($user && Auth::attempt(['u_name' => $request->u_name, 'password' => $password],true)) {
                if (Auth::user()->role_id==1) {
                    return redirect()->route('dashboard');
                }else{
                    return redirect()->route('student_index');  
                }

            } else {
                return redirect()->back()->with(['error' => MSG004 , 'u_name' => $request->u_name])->withInput();
            }
        }
    }

    public function logout()
    {
        if(Auth::user()->role_id==1){
            Auth::logout();
            return redirect('admin/login');
        }
        if(Auth::user()->role_id==2){
            Auth::logout();
            return redirect('assist/login');
        }
    }

    public function giaovien()
    {
        $giaovien = $this->cus->all('status desc');
        return view('backend::students.index', compact('giaovien'));
    }
}