<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Custormer\CustormerRepository;
use Common\Repositories\Project\ProjectRepository;
use Common\Repositories\Users\UsersRepository;

class BackendController extends Controller
{

    public function __construct(

        ProjectRepository $project,
        CustormerRepository $cus,
        UsersRepository $users

    ){

        
        $this->cus = $cus;
        $this->project = $project;
        $this->users = $users;

    }

    public function index()
    {
    	$project = $this->project->all()->count();
    	$cus = $this->cus->all()->count();
        $users = $this->users->getRole();
        $countPass = $this->project->getCountPass();
        $countNoPass = $this->project->getCountNoPass();

    	return view('backend::index')->with(['project' => $project, 'cus' => $cus, 'users' => $users, 'countPass' => $countPass, 'countNoPass' => $countNoPass]);
    }

    
}
