<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Lecturers\LecturersRepository;
use Common\Repositories\Position\PositionRepository;
use Common\Repositories\Degree\DegreeRepository;
use Modules\Backend\Http\Requests\LecturersRequest;
use Common\upload;

class LecturerController extends Controller
{

   
     public function __construct(

         LecturersRepository $lecturers,
         PositionRepository $position,
         DegreeRepository $degree

    ){

        $this->Lecturers = $lecturers;
        $this->position = $position;
        $this->degree = $degree;

    }
    public function index()
    {
        $list_lecturers=$this->Lecturers->all('id desc');
        return view('backend::lecturers.index',compact('list_lecturers'));
    }

    

    public function create()
    {
        
        $position = $this->position->all();
        $degree = $this->degree->all();
        return view('backend::lecturers.add')->with(['position' => $position, 'degree' => $degree]);
    }

    

    public function store(LecturersRequest $request)
    {
         $Lecturers = 
            [

             'l_fullname' => $request->l_fullname, 
             'l_username' => $request->l_username, 
             'l_email' => $request->l_email, 
             'l_phone' => $request->l_phone, 
             'l_birthday' => $request->l_birthday,
             'staff' => $request->staff, 
             'p_id' => $request->p_id,
             'd_id' => $request->d_id,
             'info' => $request->info, 
             'link_url' => $request->link_url,
             'gender' => $request->gender, 
             'marride' => $request->marride,
             'l_address' => $request->l_address,
             'image' => Upload::uploadFile('image', $request, LECTURERS_UPLOAD_PATH),
           ];
           
        if ($this->Lecturers->create($Lecturers)){
            return redirect()->route('lecturers-index')->with(['info' => sprintf(MSG007, 'Giảng viên')])->withInput();
        }else{
           return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }

    

    public function show($id)
    {
        
        $show_lecturers=$this->Lecturers->getShow($id);
       
        return view('backend::lecturers.view',compact('show_lecturers'));
    }

   

    public function edit($id)
    {
        $position = $this->position->all('id');
        $degree = $this->degree->all('id');
        $edit_lecturers=$this->Lecturers->findBy('id',$id);

        return view('backend::lecturers.edit',compact('edit_lecturers','position','degree'));
    }

    

    public function update(LecturersRequest $request,$id)
    {
        $data_update=[
            'l_fullname' => $request->l_fullname,
            'l_username' => $request->l_username,
            'l_email' => $request->l_email,
            'l_phone' => $request->l_phone,
            'l_birthday' => $request->l_birthday,
            'staff' => $request->staff, 
            'p_id' => $request->p_id,
            'd_id' => $request->d_id,
            'info' => $request->info,
            'link_url' => $request->link_url,
            'gender' => $request->gender,
            'marride' => $request->marride,
            'l_address' => $request->l_address
        ];

        if ($request->hasFile('image')) {
            $data_update['image'] = Upload::uploadFile('image', $request, LECTURERS_UPLOAD_PATH);
        }
        $this->Lecturers->update($data_update,$id,'id');
        return redirect()->route('lecturers-index')->with(['info'=>sprintf(MSG008,'giảng viên')]);
    }

   

    public function destroy($id)
    {
        $data = $this->Lecturers->delete($id);
        return "true";
    }
}
