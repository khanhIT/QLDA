<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Degree\DegreeRepository;
use Modules\Backend\Http\Requests\DegreeRequest;

class DegreeController extends Controller
{
    
    public function __construct(

        DegreeRepository $degree

    ){

        $this->degree = $degree;

    }

    public function index()
    {
        $degree = $this->degree->all('id desc');        
        return view('backend::degree.index')->with(['degree' => $degree]);
    }

    
    public function store(DegreeRequest $request)
    {
        $degree = [
 
             'name' => $request->name

        ];

        if ($this->degree->create($degree)){
            return redirect()->route('degree-index')->with(['info' => sprintf(MSG007, 'Bằng c')])->withInput();
        }else{
           return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }

    
    public function show()
    {
        return view('backend::degree.add');
    }

   
    public function edit($id)
    {
        $degree = $this->degree->findBy('id', $id);
        return view('backend::degree.edit')->with(['degree' => $degree]);
    }

    
    public function update(DegreeRequest $request, $id)
    {
        $degree = [

            'name' => $request->name

        ];

        $this->degree->update($degree, $id, 'id');
        return redirect()->route('degree-index')->with(['info' => sprintf(MSG008,'bằng cấp')])->withInput();
    }

    
   public function destroy($id)
    {
         $data = $this->degree->delete($id);
            return "true";
    }
}
