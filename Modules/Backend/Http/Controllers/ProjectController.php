<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Project\ProjectRepository;
use Common\Repositories\Custormer\CustormerRepository;
use Common\Repositories\Lecturers\LecturersRepository;
use Common\Repositories\SchoolYear\SchoolYearRepository;
use Common\Repositories\ClassName\ClassNameRepository;
use Modules\Backend\Http\Requests\ProjectRequest;
use Common\uploadName;
use File;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Common\Model\Project;

class ProjectController extends Controller
{
    
    public function __construct(

        ProjectRepository $project,   
        CustormerRepository $cus,
        LecturersRepository $lec,
        SchoolYearRepository $school,
        ClassNameRepository $className  

    ){

        $this->project = $project;
        $this->cus = $cus;
        $this->lec = $lec;
        $this->school = $school;
        $this->className = $className;

    }

    public function index()
    {
        $project = $this->project->listIndex();
        $projectHD1=$this->project->findBy('group',1);
        $projectHD2=$this->project->findBy('group',2);
        $projectHD3=$this->project->findBy('group',3);
        $projectHD4=$this->project->findBy('group',4);
        $projectHD5=$this->project->findBy('group',5);
        return view('backend::projects.index')->with(['project' => $project,'projectHĐ1'=>$projectHD1,'projectHĐ2'=>$projectHD2,'projectHĐ3'=>$projectHD3,'projectHĐ4'=>$projectHD4,'projectHĐ5'=>$projectHD5]);
    }
    
    public function show($id)
    {
        
        $project = $this->project->listShow($id);
        
        return view('backend::projects.view',compact('project'));   
    }

    
    public function edit($id)
    {
        $project = $this->project->listEdit($id);
       
        return view('backend::projects.edit')->with(['project' => $project]);
    }

    
    public function update(Request $request, $id)
    {
        $pro = $this->project->findBy('id', $id);

       if($request->file_upload_word == '' && $request->upload_source_code == '')
       {
       
           return redirect()->route('project-edit',['id'=>$pro->id])->with(['error' => sprintf(MSG036,'')])->withInput();

       }else{

            $project = [
                
                'point'  => $request->point,
                'group'  => $request->group,     
                'status' => $request->status,
                'cadres_read' => $request->cadres_read
                
            ];
            

            if ($request->hasFile('file_upload_word')) {
                $project['file_upload_word'] = UploadName::uploadFile('file_upload_word', $request, WORD_UPLOAD_PATH);
            }

            if ($request->hasFile('upload_source_code')) {
                $project['upload_source_code'] = UploadName::uploadFile('upload_source_code', $request, SOURCE_CODE_UPLOAD_PATH);
            }
       }

        $this->project->update($project, $id, 'id');
        return redirect()->route('project-index')->with(['info' => sprintf(MSG008,'đồ án')])->withInput();
    }


    public function exportHĐ1()
    {
        $project = $this->project->exportHĐ1();

        $projectArray = [];


        $projectArray = ['id', 'msv', 'fullname', 'name_project', 'l_id', 'cadres_read'];

        foreach ($project as $pro) {
        $projectArray[] = $pro->toArray();
        }

        Excel::create('Danh sách đồ án hội đồng 1', function($excel) use ($project){

           $excel->sheet('Danh sách đồ án hội đồng 1',function($sheet) use ($project){

              $sheet->fromArray($project, null, 'A5', false, true);
              $sheet->mergeCells('A1:B1');
              $sheet->cell('A1', function($cell) {
              $cell->setValue('');

              // Set black background
              //$cell->setBackground('red');

              // Set font
              $cell->setFont([
                'family'     => 'Arial',
                'size'       => '11',
                'bold'       =>  true
              ]);

                // Set all borders (top, right, bottom, left)
              $cell->setBorder('solid', 'none', 'none', 'solid');

            });

            $sheet->mergeCells('C2:H2'); 
            $sheet->cell('C2', function ($cell) {
                $date= date('Y')-1 .'-'.date('Y');
                $cell->setValue("DANH SÁCH HỘI ĐỒNG 1 SINH VIÊN BẢO VỆ TỐT NGHIỆP CHUYÊN NGÀNH TIN HỌC TRẮC ĐỊA NĂM HỌC $date");
                $cell->setFontWeight('bold');
            });  

           });

       })->export('xlsx');
       return redirect()->route('project-index')->with('info','Bạn đã xuất file thành công');
    }


    public function exportHĐ2()
    {
        $project = $this->project->exportHĐ2();

        $projectArray = [];

        $projectArray = ['id', 'msv', 'fullname', 'name_project', 'l_id', 'cadres_read'];

        foreach ($project as $pro) {
        $projectArray[] = $pro->toArray();
        }

        Excel::create('Danh sách đồ án hội đồng 2', function($excel) use ($project){

           $excel->sheet('Danh sách đồ án hội đồng 2',function($sheet) use ($project){

              $sheet->fromArray($project, null, 'A5', false, true);
              $sheet->mergeCells('A1:B1');
              $sheet->cell('A1', function($cell) {
              $cell->setValue('');

              // Set black background
              //$cell->setBackground('red');

              // Set font
              $cell->setFont([
                'family'     => 'Arial',
                'size'       => '11',
                'bold'       =>  true
              ]);

                // Set all borders (top, right, bottom, left)
              $cell->setBorder('solid', 'none', 'none', 'solid');

            });

            $sheet->mergeCells('C2:H2'); 
            $sheet->cell('C2', function ($cell) {
                $date= date('Y')-1 .'-'.date('Y');
                $cell->setValue("DANH SÁCH HỘI ĐỒNG 2 SINH VIÊN BẢO VỆ TỐT NGHIỆP CHUYÊN NGÀNH TIN HỌC TRẮC ĐỊA NĂM HỌC $date");
                $cell->setFontWeight('bold');
            });  

           });

       })->export('xlsx');

       return redirect()->route('project-index')->with('info','Bạn đã xuất file thành công');
    }
    public function exportHĐ3()
    {
        $project = $this->project->exportHĐ3();
        $projectArray = [];

        $projectArray = ['id', 'msv', 'fullname', 'name_project', 'l_id', 'cadres_read'];

        foreach ($project as $pro) {
            $projectArray[] = $pro->toArray();
        }

        Excel::create('Danh sách đồ án hội đồng 3', function($excel) use ($project){

            $excel->sheet('Danh sách đồ án hội đồng 3',function($sheet) use ($project){

                $sheet->fromArray($project, null, 'A5', false, true);
                $sheet->mergeCells('A1:B1');
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('');

                    // Set black background
                    //$cell->setBackground('red');

                    // Set font
                    $cell->setFont([
                        'family'     => 'Arial',
                        'size'       => '11',
                        'bold'       =>  true
                    ]);

                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('solid', 'none', 'none', 'solid');

                });

                $sheet->mergeCells('C2:H2');
                $sheet->cell('C2', function ($cell) {
                    $date= date('Y')-1 .'-'.date('Y');
                    $cell->setValue("DANH SÁCH HỘI ĐỒNG 3 SINH VIÊN BẢO VỆ TỐT NGHIỆP CHUYÊN NGÀNH TIN HỌC TRẮC ĐỊA NĂM HỌC $date");
                    $cell->setFontWeight('bold');
                });

            });

        })->export('xlsx');

        return redirect()->route('project-index')->with('info','Bạn đã xuất file thành công');
    }
    public function exportHĐ4()
    {
        $project = $this->project->exportHĐ4();
        $projectArray = [];

        $projectArray = ['id', 'msv', 'fullname', 'name_project', 'l_id', 'cadres_read'];

        foreach ($project as $pro) {
            $projectArray[] = $pro->toArray();
        }

        Excel::create('Danh sách đồ án hội đồng 4', function($excel) use ($project){

            $excel->sheet('Danh sách đồ án hội đồng 4',function($sheet) use ($project){

                $sheet->fromArray($project, null, 'A5', false, true);
                $sheet->mergeCells('A1:B1');
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('');

                    // Set black background
                    //$cell->setBackground('red');

                    // Set font
                    $cell->setFont([
                        'family'     => 'Arial',
                        'size'       => '11',
                        'bold'       =>  true
                    ]);

                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('solid', 'none', 'none', 'solid');

                });

                $sheet->mergeCells('C2:H2');
                $sheet->cell('C2', function ($cell) {
                    $date= date('Y')-1 .'-'.date('Y');
                    $cell->setValue("DANH SÁCH HỘI ĐỒNG 4 SINH VIÊN BẢO VỆ TỐT NGHIỆP CHUYÊN NGÀNH TIN HỌC TRẮC ĐỊA NĂM HỌC $date");
                    $cell->setFontWeight('bold');
                });

            });

        })->export('xlsx');

        return redirect()->route('project-index')->with('info','Bạn đã xuất file thành công');
    }
    public function exportHĐ5()
    {
        $project = $this->project->exportHĐ5();
        $projectArray = [];

        $projectArray = ['id', 'msv', 'fullname', 'name_project', 'l_id', 'cadres_read'];

        foreach ($project as $pro) {
            $projectArray[] = $pro->toArray();
        }

        Excel::create('Danh sách đồ án hội đồng 5', function($excel) use ($project){

            $excel->sheet('Danh sách đồ án hội đồng 5',function($sheet) use ($project){

                $sheet->fromArray($project, null, 'A5', false, true);
                $sheet->mergeCells('A1:B1');
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('');

                    // Set black background
                    //$cell->setBackground('red');

                    // Set font
                    $cell->setFont([
                        'family'     => 'Arial',
                        'size'       => '11',
                        'bold'       =>  true
                    ]);

                    // Set all borders (top, right, bottom, left)
                    $cell->setBorder('solid', 'none', 'none', 'solid');

                });

                $sheet->mergeCells('C2:H2');
                $sheet->cell('C2', function ($cell) {
                    $date= date('Y')-1 .'-'.date('Y');
                    $cell->setValue("DANH SÁCH HỘI ĐỒNG 5 SINH VIÊN BẢO VỆ TỐT NGHIỆP CHUYÊN NGÀNH TIN HỌC TRẮC ĐỊA NĂM HỌC $date");
                    $cell->setFontWeight('bold');
                });

            });

        })->export('xlsx');

        return redirect()->route('project-index')->with('info','Bạn đã xuất file thành công');
    }

    public function downloadFile($id)
    {
      //read info account
       $user = Auth::user();

       $project = Project::find($id);
       $file = public_path() . '/upload/files/file_word/' . $project->file_upload_word;
        if(file_exists($file)) {
                return response()->download($file);
        } else {
            return "File không tồn tại";
        }
    }

}
