<?php

namespace Modules\Backend\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\ListProject\ListProjectRepository;
use Common\Repositories\Lecturers\LecturersRepository;
use Modules\Backend\Http\Requests\ListProjectRequest;

class ListProjectAdminController extends Controller
{
    
    public function __construct(
         
         ListProjectRepository  $list,
         LecturersRepository $lec

    ){
        
        $this->list = $list;
        $this->lec = $lec;

    }

    public function index()
    {
        $list = $this->list->getIndex();
        return view('backend::listProjectAdmin.index',compact('list'));
    }

  
    public function show()
    {
        $lec = $this->lec->all('id');
        return view('backend::listProjectAdmin.add')->with(['lec' => $lec]);
    }

    
    public function store(ListProjectRequest $request)
    {
        $list = [

            "name" => $request->name,

            "l_id" => $request->l_id,

            "description" => $request->description,

            "expried_at" => $request->expried_at

        ];
         
        if ($this->list->create($list)){
            return redirect()->route('ListProjects_index')->with(['info' => sprintf(MSG007, 'Đề tài')])->withInput();
        }else{
           return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }

   
    public function edit($id)
    {
        $lec = $this->lec->all('id');
        $list = $this->list->findBy('id', $id);
        return view('backend::listProjectAdmin.edit')->with(['lec' => $lec, 'list' => $list]);
    }

   
    public function update(ListProjectRequest $request, $id)
    {
        $list = [

            "name" => $request->name,

            "l_id" => $request->l_id,

            "description" => $request->description,

            "expried_at" => $request->expried_at

        ];

        $this->list->update($list, $id, 'id');
        return redirect()->route('ListProjects_index')->with(['info' => sprintf(MSG008,'danh sách đề tài')])->withInput();
    }

    
    public function destroy($id)
    {
         $data = $this->list->delete($id);
            return "true";
    }
}
