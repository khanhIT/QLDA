<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Position\PositionRepository;
use Modules\Backend\Http\Requests\PositionRequest;

class PositionController extends Controller
{

    public function __construct(

        PositionRepository $position

    ){

        $this->position = $position;

    }
    
    public function index()
    {
        $position = $this->position->all('id desc');
        return view('backend::position.index')->with(['position' => $position]);
    }

    
    public function store(PositionRequest $request)
    {
        $position = [
 
             'name' => $request->name

        ];

        if ($this->position->create($position)){
            return redirect()->route('position-index')->with(['info' => sprintf(MSG007, 'Chức vụ')])->withInput();
        }else{
           return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }

    
    public function show()
    {
        return view('backend::position.add');
    }

   
    public function edit($id)
    {
        $position = $this->position->findBy('id', $id);
        return view('backend::position.edit')->with(['position' => $position]);
    }

    
    public function update(PositionRequest $request, $id)
    {
        $position = [

            'name' => $request->name

        ];

        $this->position->update($position, $id, 'id');
        return redirect()->route('position-index')->with(['info' => sprintf(MSG008,'chức vụ')])->withInput();
    }

    
   public function destroy($id)
    {
         $data = $this->position->delete($id);
            return "true";
    }
}
