<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\ListProject\ListProjectRepository;
use Common\Repositories\Lecturers\LecturersRepository;
use Modules\Backend\Http\Requests\ListProjectRequest;

class ListProjectController extends Controller
{
    
    public function __construct(
         
         ListProjectRepository  $list,
         LecturersRepository $lec

    ){
        
        $this->list = $list;
        $this->lec = $lec;

    }

    public function index()
    {
        $list = $this->list->getIndex();
        return view('backend::listProject.index')->with(['list' => $list]);
    }

  
    public function show()
    {
        $lec = $this->lec->all('id');
        return view('backend::listProject.add')->with(['lec' => $lec]);
    }

    
    public function store(ListProjectRequest $request)
    {
        $list = [

            "name" => $request->name,

            "l_id" => $request->l_id,

            "description" => $request->description,

            "expried_at" => $request->expried_at

        ];

        if ($this->list->create($list)){
            return redirect()->route('ListProject-index')->with(['info' => sprintf(MSG007, 'Đồ án')])->withInput();
        }else{
           return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }

   
    public function edit($id)
    {
        $lec = $this->lec->all('id');
        $list = $this->list->findBy('id', $id);
        return view('backend::listProject.edit')->with(['lec' => $lec, 'list' => $list]);
    }

   
    public function update(ListProjectRequest $request, $id)
    {
        $list = [

            "name" => $request->name,

            "l_id" => $request->l_id,

            "description" => $request->description,

            "expried_at" => $request->expried_at

        ];

        $this->list->update($list, $id, 'id');
        return redirect()->route('ListProject-index')->with(['info' => sprintf(MSG008,'danh sách đồ án')])->withInput();
    }

    
    public function destroy($id)
    {
         $data = $this->list->delete($id);
            return "true";
    }


    public function postImport(Request $request)
    {
        $file = $request->file('import');
        $filename = $file->getClientOriginalName();
        //get data on import file
        $excelData = \Excel::load($file, function ($reader) {
              $results = $reader->get();
            })->get();

        try{

          \DB::beginTransaction();
          $importResult = $this->list->importFromExcelListProject($excelData, $filename);

          $logData = [
            'countRowSuccess' => $importResult['countRowInsertSuccess'],
            'totalRow' => $importResult['totalRow'],
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
          ];

          \DB::commit();
          return redirect(route('ListProject-index'));
        } catch (\Exception $e) {
          \DB::rollback();
          return $e->getMessage();
        }
    }
}
