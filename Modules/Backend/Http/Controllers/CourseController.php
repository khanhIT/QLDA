<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\SchoolYear\SchoolYearRepository;
use Modules\Backend\Http\Requests\CourseRequest;
class CourseController extends Controller
{
    public function __construct(
        SchoolYearRepository $course
    ){

        $this->course = $course;
    }
    
    public function index()
    {
        $list_course=$this->course->listIndex();
        return view('backend::course.index',compact('list_course'));
    }

    
    public function create()
    {
        return view('backend::course.add');
    }

    
    public function store(CourseRequest $request)
    {
        $add_course =
            [
                's_key' => $request->s_key,
                'year_start' => $request->year_start,
                'year_end' => $request->year_end,
                'total_student' => $request->total_student
            ];
        if ($this->course->create($add_course)){
            return redirect()->route('course-index')->with(['info' => sprintf(MSG007, 'Khóa học')])->withInput();
        }else{
            return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
        }
    }

    
    public function edit($id)
    {
        $edit_course=$this->course->findBy('id',$id);
        return view('backend::course.edit',compact('edit_course'));
    }

    
    public function update(CourseRequest $request ,$id)
    {
        $update_cousre=[
          's_key'=>$request->s_key,
            'year_start'=>$request->year_start,
            'year_end'=>$request->year_end,
              'total_student'=>$request->total_student
        ];
        if ($this->course->update($update_cousre,$id,'id')){
            return redirect()->route('course-index')->with(['info' => sprintf(MSG008, 'Khóa học')])->withInput();
        }else{
            return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
        }
    }

    
    public function destroy($id)
    {
        $data = $this->course->delete($id);
        return "true";
    }
}
