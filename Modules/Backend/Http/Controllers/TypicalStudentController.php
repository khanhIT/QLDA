<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\TypicalStudent\TypicalStudentRepository;
use Modules\Backend\Http\Requests\TypicalStudentRequest;
use Common\upload;
class TypicalStudentController extends Controller
{
    public function __construct(
        TypicalStudentRepository $typical
    )
    { 
        $this->typical = $typical;
    }

    public function index()
    {   
        $typical = $this->typical->all('id desc');
    	return view('backend::typicalStudent.index')->with(['typical' => $typical]);
    }

    public function add()
    {
    	return view('backend::typicalStudent.add');
    }

    public function postAdd(TypicalStudentRequest $request)
    {
       $typical = [
          'name' => $request->name,
          'info' => $request->info,
          'link_url' => $request->link_url,
          'image' => Upload::uploadFile('image', $request, TYPICALSTUDENT_UPLOAD_PATH)
       ];
       
       if ($this->typical->create($typical)) {
           return redirect()->route('listTypicalStudent')->with(['info' => sprintf(MSG007, 'Sinh viên tiêu biểu')])->withInput();
       }else{
           return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }
    public function edit($id)
    {
        $typical = $this->typical->findBy('id', $id);
    	return view('backend::typicalStudent.edit')->with(['typical' => $typical]);
    }

    public function postEdit(TypicalStudentRequest $request, $id)
    {
        $typical = [
            'name' => $request->name,
            'info' => $request->info,
            'link_url' => $request->link_url
        ];

        if ($request->hasFile('image')) {
            $typical['image'] = Upload::uploadFile('image', $request, TYPICALSTUDENT_UPLOAD_PATH);
        }

        $this->typical->update($typical, $id, 'id');
        return redirect()->route('listTypicalStudent')->with(['info' => sprintf(MSG008,'sinh viên tiêu  biểu')])->withInput();
    }

    public function destroy($id)
    {
         $data = $this->typical->delete($id);
            return "true";
    }
}
