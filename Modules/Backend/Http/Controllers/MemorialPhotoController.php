<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\MemorialPhoto\MemorialPhotoRepository;
use Modules\Backend\Http\Requests\MemorialPhotoRequest;
use Common\upload;

class MemorialPhotoController extends Controller
{
    public function __construct(
        MemorialPhotoRepository $photo
    )
    {
        $this->photo = $photo;
    } 

    public function index()
    {
        $photo = $this->photo->all('id desc'); 
    	return view('backend::memorialPhoto.index')->with(['photo' => $photo]);
    }

    public function add()
    {
    	return view('backend::memorialPhoto.add');
    }

    public function postAdd(MemorialPhotoRequest $request)
    {
       $photo = [
           'name' => $request->name,
           'image' => Upload::uploadFile('image', $request, MEMORIALPHOTO_UPLOAD_PATH)
       ];

       if ($this->photo->create($photo)) {
           return redirect()->route('listMemorialPhoto')->with(['info' => sprintf(MSG007, 'Hình ảnh tiêu biểu')])->withInput();
       }else{
          return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }
    public function edit($id)
    {
        $photo = $this->photo->findBy('id', $id);
    	return view('backend::memorialPhoto.edit')->with(['photo' => $photo]);
    }

    public function postEdit(MemorialPhotoRequest $request, $id)
    {
       $photo = [
           'name' => $request->name
       ];

       if ($request->hasFile('image')) {
           $photo['image'] = Upload::uploadFile('image', $request, MEMORIALPHOTO_UPLOAD_PATH);
       }

       $this->photo->update($photo, $id, 'id');
       return redirect()->route('listMemorialPhoto')->with(['info' => sprintf(MSG008,'hình ảnh tiêu biểu')])->withInput();
    }

    public function destroy($id)
    {
         $data = $this->photo->delete($id);
            return "true";
    }
}
