<?php

namespace Modules\Backend\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Users\UsersRepository;
use Common\Repositories\Role\RoleRepository;
use Modules\Backend\Http\Requests\UsersRequest;
use Common\upload;

class UserController extends Controller
{

    public function __construct(
        UsersRepository $users,
        RoleRepository $role
    ){

        $this->users = $users;
        $this->role =$role;
    }
    
    public function index()
    {
        $user_data=$this->users->all('id desc');
        return view('backend::users.index',compact('user_data'));
    }

    
    public function create()
    {
        $role = $this->role->all('id asc');
        return view('backend::users.add',compact('role'));
    }

    
    public function store(UsersRequest $request)
    {   
         $data_users=[
            'u_fullname'=> $request->u_fullname,
            'u_name'=> $request->u_name,
            'u_email'=> $request->u_email,
            'password'=> bcrypt($request->password),
            'mobile'=> $request->mobile,
            'gender'=>$request->gender,
            'skype'=>$request->skype,
            'avatar'=> Upload::uploadFile('avatar', $request, USERS_UPLOAD_PATH),
            'birthday'=>$request->birthday,
            'married'=>$request->married,
            'address'=>$request->address,
            'role_id'=>$request->role_id
          
        ];
        $exit = $this->users->find($request->u_email);
        if($exit== null) {
            if($this->users->create($data_users) ) {
                return redirect('admin/users')->with(['info' =>sprintf(MSG007,'Users')])->withInput();
            }else {
                return false;
            }
        }else{
            return redirect()->back()->with(['error' => sprintf(MSG006, 'Email đã tồn tại')])->withInput();
        }

    }

    
    public function show($id)
    {
        $user_show = $this->users->findBy('id', $id);
        return view('backend::users.view',compact('user_show'));
    }

    
    public function edit($id)
    {
        $user_edit = $this->users->findBy('id', $id);
        $role = $this->role->all('id asc');
        return view('backend::users.edit',compact('user_edit','role'));
    }

    
    public function update(UsersRequest $request, $id)
    {
         $data_update=[

           'u_fullname'=> $request->u_fullname,
            'u_name'=> $request->u_name,
            'u_email'=> $request->u_email,
            'password'=> bcrypt($request->password),
            'mobile'=> $request->mobile,
            'gender'=>$request->gender,
            'skype'=>$request->skype,
            'birthday'=>$request->birthday,
            'married'=>$request->married,
            'address'=>$request->address,
            'role_id'=>$request->role_id
        ];

        if ($request->hasFile('avatar')) {
             $data_update['avatar'] = Upload::uploadFile('avatar', $request, USERS_UPLOAD_PATH);
         } 
        $this->users->update($data_update,$id,'id');
            return redirect()->route('users-index')->with(['info'=>sprintf(MSG008,'Users')]);
    }

    
    public function destroy($id)
    {
         $data = $this->users->delete($id);
            return "true";
    }
}
