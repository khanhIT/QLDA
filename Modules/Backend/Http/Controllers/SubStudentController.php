<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Custormer\CustormerRepository;
use Common\Repositories\Lecturers\LecturersRepository;
use Common\Repositories\SchoolYear\SchoolYearRepository;
use Common\Repositories\ClassName\ClassNameRepository;
use Modules\Backend\Http\Requests\CustormerRequest;

class SubStudentController extends Controller
{

    public function __construct(

        CustormerRepository $custormer,
        LecturersRepository $lec,
        SchoolYearRepository $school,
        ClassNameRepository $className

    ){

        $this->custormer = $custormer;
        $this->lec = $lec;
        $this->school = $school;
        $this->className = $className;

    }
   
    public function index()
    {
        $custormer = $this->custormer->listIndex();
        return view('backend::subStudents.index')->with(['custormer' => $custormer]);
    }

   
    public function show($id)
    {

       $custormer = $this->custormer->listShow($id);
        return view('backend::subStudents.view')->with(['custormer' => $custormer]);
    }

   
    public function edit($id)
    {
        $custormer = $this->custormer->listView($id);
        $lec = $this->lec->all('id');
        $school = $this->school->all('id'); 
        $className = $this->className->all('id'); 
        return view('backend::subStudents.edit')->with(['custormer' => $custormer, 'lec' => $lec, 'school' => $school, 'className' => $className]);
    }

    
    public function update(CustormerRequest $request, $id)
    {
        $custormer = [

            'msv' => $request->msv,
            'fullname' => $request->fullname,  
            'phone' => $request->phone,
            'birthday' => $request->birthday,
            'address' => $request->address,
            'project_name' => $request->project_name,
            'c_id' => $request->c_id,
            's_id' => $request->s_id,
            'l_id' => $request->l_id

        ];

        $this->custormer->update($custormer, $id, 'id');
        return redirect()->route('student_index')->with(['info' => sprintf(MSG008,'sinh viên')])->withInput();
    }
}
