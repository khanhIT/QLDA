<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Slide\SlideRepository;
use Modules\Backend\Http\Requests\SlideRequest;
use Common\upload;
class SlideController extends Controller
{
	public function __construct(
		SlideRepository $slide
	)
	{
        $this->slide = $slide;
	}

    public function index(){
    	$slide = $this->slide->all('id desc');
        return view('backend::slide.index')->with(['slide' => $slide]);

    }

    public function add(){
        return view('backend::slide.add');
    }

    public function postAdd(SlideRequest $request){
        $slide = 
            [
             'name' => $request->name, 
	         'image' => Upload::uploadFile('image', $request, SLIDE_UPLOAD_PATH),
	       ];
        if ($this->slide->create($slide)){
            return redirect()->route('listSlide')->with(['info' => sprintf(MSG007, 'Slide')])->withInput();
        }else{
           return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
       }
    }
    public function edit($id){
    	$slide = $this->slide->findBy('id',$id);
        return view('backend::slide.edit')->with(['slide' => $slide]);
    }

    public function postEdit(SlideRequest $request, $id){
    	$slide = [
    		'name' => $request->name
    	];

    	if ($request->hasFile('image')) {
    		$slide['image'] = Upload::uploadFile('image', $request, SLIDE_UPLOAD_PATH);
    	}

    	$this->slide->update($slide, $id, 'id');
    	 return redirect()->route('listSlide')->with(['info' => sprintf(MSG008,'slide')])->withInput();
    }

    public function destroy($id)
    {
         $data = $this->slide->delete($id);
            return "true";
    }
}
