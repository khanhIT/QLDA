<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Common\Repositories\Contact\ContactRepository;
use Modules\Backend\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Mail;
class ContactController extends Controller
{
	public function __construct(
		ContactRepository $contact
	)
	{
        $this->contact = $contact;
	}
    public function index()
    {
        $contact = $this->contact->getManyWhereOrder(['status'=>0],'*','id desc');
        return view('backend::contact.index',compact('contact'));
    }
    public function edit($id)
    {
        $listContact=$this->contact->findBy('id',$id);
        return view('backend::contact.reply',compact('listContact'));
    }

    public function update(ContactRequest $request ,$id)
    {
        $dataContact=[
            'reply'=>$request->reply,
            'status'=>1
        ];

          if ($this->contact->update($dataContact,$id,'id')){
              $dataContactSend=$this->contact->findBy('id',$id);
                $data=json_decode(json_encode($dataContactSend),TRUE);
              $data=[
                  'data'=>$data
              ];
                $email=$dataContactSend['email'];
              Mail::send(['html' => 'backend.mail.replyContact'],$data, function($message) use($email) {
                  $message->to($email)
                      ->subject('Trả lời từ hệ thống');
                  $message->from($email,'Bộ Môn Tin Trắc Địa');
              });
              return redirect()->route('listContact')->with(['info' => sprintf(MSG037,'')])->withInput();
          }else{
              return redirect()->back()->with(['error' => sprintf(MSG034, '')])->withInput();
          }
    }
}
