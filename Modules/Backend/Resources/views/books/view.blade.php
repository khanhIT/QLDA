@extends('backend::layouts.master')

@section('title')
    Quản lý sách
@stop
@section('styleSheet')
    <link rel="stylesheet" href="{{asset('css/backend/lecturers/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
    <style type="text/css">
        #users .avatar{
            min-height: 350px
        }
        #users .avatar .users-detail{
            font-size: 16px;
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{{route('listBooks')}}">Sách</a></li>
            <li><a href="#"  style="color: red">Xem chi tiết</a></li>
        </ul>
                   
        <section class="content container" id="users">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-xs-6 col-md-4">
                        <div class="card">
                            <div class="avatar">
                                @if(!empty($viewBooks->cover))
                                    <img src="{!! url('/') !!}/upload/books/images/{!! $viewBooks->cover !!}" width="100%" height="100%">
                                @else()
                                    <img src="{!! url('/') !!}/upload/users/5aae2373cfd21.jpg" width="100%" height="100%">
                                @endif

                                <p class="users-detail">{{$viewBooks->title}}</p>

                                <div class="col-xs-6 item-avatar">
                                    <p>Tác giả</p>
                                    <strong>{{$viewBooks->author}}</strong>
                                </div>
                               
                                <div class="col-xs-6 item-avatar">
                                    <p>Năm</p>
                                    <strong>{{$viewBooks->year}}</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-8">
                        <div class="card">
                            <div class="card-body item-detail-01">
                                <div class="main_title">
                                    <h3 class="title-03 fleft">
                                        Thông tin sách
                                    </h3>
                                </div>
                                <div class="main-item container-fluid">
                                    <table class="table-detail-01">
                                        <tbody>
                                        <tr>
                                            <th>Tên sách</th>
                                            <td>{{$viewBooks->title}}</td>
                                        </tr>
                                        <tr>
                                            <th>Phiên bản:</th>
                                            <td>{{$viewBooks->edition}}</td>
                                        </tr>
                                        <tr>
                                            <th>isbn:</th>
                                            <td> {{ $viewBooks->isbn}}</td>
                                        </tr>
                                        <tr>
                                            <th>Nhà xuất bản:</th>
                                            <td>
                                             {{$viewBooks->publisher}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Mô tả:</th>
                                            <td>
                                              {!! $viewBooks->abstract !!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Tài liệu:</th>
                                            <td>{{$viewBooks->inside}}</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>    
                        <a href="{{route('listBooks')}}" class="btn btn-sm btn-success">
                            <span class="glyphicon glyphicon-arrow-left"></span> Trở lại</a>
                    </div>

                </div>
            </div>
        </section>
    </div>
@stop
