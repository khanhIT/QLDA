@extends('backend::layouts.master')
@section('title')
    Danh sách sách
@stop
@section('styleSheet')
    <style type="text/css">
        .add{
            margin-top: 60px;
        }
        div.dataTables_wrapper div.dataTables_filter{
            text-align: left;
            margin-left: -575px;
        }
        div.dataTables_wrapper div.dataTables_paginate{
            margin: 0;
            white-space: nowrap;
            text-align: right;
            margin-right: 64px;
        }
    </style>
@stop
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
            <div class="title_right">
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right">
                    <a href="{!! route('addBooks') !!}">
                        <button class="btn btn-sm btn-success add"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
                    </a>
                </div>
            </div>
        </div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a>
            </li>
            <li class="breadcrumb-item active" style="color: red">Danh sách sách</li>
        </ol>
        <div class="clearfix"></div>
        @if (session('info'))
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('info')}}
            </div>
    @endif
    <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="x_panel">
                        <div class="x_content">
                            <table class=" table table-hover" id="data-table2">
                                <thead>
                                <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                    <th width="30px;">
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">STT</font>
                                        </font>
                                    </th>
                                    <th class="no-sorting" width="50px">
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">Hình ảnh</font>
                                        </font>
                                    </th>
                                    <th class="no-sorting" style="width: 250px">
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">Tên</font>
                                        </font>
                                    </th>
                                    <th style="width: 250px">
                                        <font style="vertical-align: inherit;" >
                                            <font style="vertical-align: inherit;">Tên tác giả</font>
                                        </font>
                                    </th>
                                    <th style="width: 100px">
                                        <font style="vertical-align: inherit;" >
                                            <font style="vertical-align: inherit;">Nhà xuất bản</font>
                                        </font>
                                    </th>
                                    <th class="no-sorting">
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">Hành động</font>
                                        </font>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php $stt = 1; ?>
                                @foreach($listBooks as $book)
                                    <tr>
                                        <td scope="row">
                                            <font style="vertical-align: inherit;">
                                                <font>
                                                    {!! $stt !!}
                                                </font>
                                            </font>
                                        </td>
                                        <td >
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">
                                                    <img src="{{ url('/') }}/upload/books/images/{!! $book->cover !!}" alt="hình ảnh" style="width: 150px; height: 100px">
                                                </font>
                                            </font>
                                        </td>
                                        <td scope="row">
                                            <font style="vertical-align: inherit;">
                                                <font>
                                                    {!! $book->title !!}
                                                </font>
                                            </font>
                                        </td>
                                        <td scope="row">
                                            <font style="vertical-align: inherit;">
                                                <font>
                                                    {!! $book->author !!}
                                                </font>
                                            </font>
                                        </td>
                                        <td scope="row">
                                            <font style="vertical-align: inherit;">
                                                <font>
                                                    {!! $book->publisher !!}
                                                </font>
                                            </font>
                                        </td>
                                        <td>
                                            <a href="{!! route('viewBooks',['id'=>$book->id]) !!}">
                                                <button type="button" id="delete" class="btn btn-xs btn-info">
                                                    <i class="fa fa-eye" aria-hidden="true"></i> xem
                                                </button>
                                            </a>
                                            <a href="{!! route('editBooks',['id'=>$book->id]) !!}">
                                                <button type="button" id="delete" class="btn btn-xs btn-warning">
                                                    <i class="fa fa-edit" aria-hidden="true"></i> sửa
                                                </button>
                                            </a>
                                            <a href="#" class="btn btn-danger btn-xs  delete_data_books delete_{{ $book->id}}" data-action='{{url("admin/sach/xoa/$book->id")}}' data-id="{{ $book->id}}"  ><span class="glyphicon glyphicon-trash"></span> xóa</a>
                                        </td>
                                    </tr>
                                    <?php $stt++; ?>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

            </div>


        </section>
        <!-- /.content -->
    </div>
@stop

@section('scriptAdd')
    <script type="text/javascript">
        $(document).ready(function(){

            $(document).on('click', '.delete_data_books', function(e){
                delete_data_userAPI = $(this).data('action');

                swal({
                        title: "Bạn có chắc chắn muốn xóa item này？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,

                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_userAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('admin/sach/danh-sach')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });



    </script>
@endsection
