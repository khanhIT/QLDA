@extends('backend::layouts.master')
@section('title')
    Thêm mới sách
@stop
@section('styleSheet')
    <style type="text/css">
        form{
            margin-top: 80px;
        }
        .list{
            margin-top: 60px;
        }
    </style>
@stop
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
            <div class="title_right">
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <a href="{!! route('listBooks') !!}">
                        <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                    </a>
                </div>
            </div>
        </div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('listBooks') }}">Danh sách sách</a></li>
            <li class="breadcrumb-item active" style="color: red">Thêm mới</li>
        </ol>
        <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="row">
                    <form action="" method="post" enctype="multipart/form-data" id="demo-form2"  class="form-horizontal form-label-left" novalidate="">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Tiêu đề </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="title" class="form-control col-md-7 col-xs-12" placeholder="Nhập tên sách">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('title') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('author') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Tác giả </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="author" class="form-control col-md-7 col-xs-12" placeholder="Nhập tên tác giả">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('author') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('edition') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Phiên bản </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="edition" class="form-control col-md-7 col-xs-12" placeholder="Nhập tháng">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('edition') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Năm </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="year" class="form-control col-md-7 col-xs-12" placeholder="Nhập năm ">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('year') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">isbn </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="isbn" class="form-control col-md-7 col-xs-12" placeholder="Nhập tên tác giả">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('isbn') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('publisher') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Nhà xuất bản </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="publisher" class="form-control col-md-7 col-xs-12" placeholder="Nhập tên nhà xuất bản">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('publisher') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('abstract') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Mô tả </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="abstract" id="" cols="30" rows="10" class="form-control col-md-7 col-xs-12" placeholder="Nhập mô tả"></textarea>
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('abstract') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Tư liệu </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" name="inside" class="form-control col-md-7 col-xs-12" placeholder="Nhập tên nhà xuất bản">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('inside') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('cover') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Hình ảnh </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" onchange='openFile(event)' name="cover" class="form-control col-md-7 col-xs-12">
                                <img id="output" src="<?php echo url('/')?>/images/web-04.jpg" style="margin-top: 15px" />
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('cover') }}</span>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-primary" type="reset"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><span class="glyphicon glyphicon-refresh"></span> Nhập lại</font></font></button>
                                <button type="submit" class="btn btn-success"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</font></font></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">

            </div>


        </section>
        <!-- /.content -->
    </div>
@stop

@section('scriptAdd')
    <script>
        var openFile = function(event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('output');
                output.src = dataURL;
            };
            reader.onload = function (e) {
                $('#output')
                    .attr('src', e.target.result)
                    .width(565)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        };
    </script>
@stop
