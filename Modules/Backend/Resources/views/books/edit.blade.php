@extends('backend::layouts.master')
@section('title')
    Sửa thông tin sách
@stop
@section('styleSheet')
    <style type="text/css">
        form{
            margin-top: 90px
        }
        button{
            margin-top: 80px;
        }
        .list{
            margin-left: 85px
        }
    </style>
@stop
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
            <div class="title_right">
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <a href="{!! route('addBooks') !!}">
                        <button class="btn btn-sm btn-success"><i class="fa fa-plus" aria-hidden="true"></i>Thêm mới</button>
                    </a>
                </div>
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <a href="{!! route('listBooks') !!}">
                        <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                    </a>
                </div>
            </div>
        </div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item"><a href="{!! route('listSlide') !!}">Danh sách sách</a></li>
            <li class="breadcrumb-item active" style="color: red">Sửa thông tin sách</li>
        </ol>
        <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="row">
                    <form action="" method="post" enctype="multipart/form-data" id="demo-form2"  class="form-horizontal form-label-left" novalidate="">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Tiêu đề </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="title" value="{{$editBooks->title}}" class="form-control col-md-7 col-xs-12">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('title') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('edition') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Phiên bản </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="edition" value="{{$editBooks->edition}}" class="form-control col-md-7 col-xs-12">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('edition') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('year') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Năm </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="year" value="{{$editBooks->year}}" class="form-control col-md-7 col-xs-12">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('year') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('isbn') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">isbn </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="isbn" value="{{$editBooks->isbn}}" class="form-control col-md-7 col-xs-12">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('isbn') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('author') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Tác giả </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="author" value="{{$editBooks->author}}" class="form-control col-md-7 col-xs-12">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('name') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('publisher') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Nhà xuất bản </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="publisher" value="{{$editBooks->publisher}}" class="form-control col-md-7 col-xs-12">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('publisher') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Mô tả </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea name="abstract" id="" cols="30" rows="10" class="form-control col-md-7 col-xs-12">{{$editBooks->abstract}}</textarea>
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('abstract') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('inside') ? 'has-error' : '' }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Tư liệu </font>
                                </font>
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" name="inside" value="{{$editBooks->inside}}" class="form-control col-md-7 col-xs-12">
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('inside') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                            <label class="col-xs-3 control-label">Hình ảnh
                                <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" onchange='openFile(event)' name="cover" class="form-control col-md-7 col-xs-12">
                                @if(!empty($editBooks->cover))
                                    <img id="image" class="" src="<?php echo url('/')?>/upload/books/images/{!! $editBooks->cover !!}" width="570" height="200" style="margin-top: 15px"/>
                                @else
                                    <img src="<?php echo url('/')?>/images/web-04.jpg" style="margin-top: 15px" />
                            @endif
                            {{--<img id="output" src="#"/> --}}
                            </div>
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('cover') }}</span>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" class="btn btn-success">
                                    <span class="glyphicon glyphicon-ok"></span>
                                    Cập nhập
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">

            </div>


        </section>
        <!-- /.content -->
    </div>
@stop

@section('scriptAdd')
    <script>
        var openFile = function(event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('output');
                output.src = dataURL;
            };
            reader.onload = function (e) {
                $('#image')
                    .attr('src', e.target.result)
                    .width(570)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        };
    </script>
@stop
