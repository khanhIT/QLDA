@extends('backend::layouts.master')
@section('title')
    Gửi thông báo
@stop
@section('styleSheet')
  <style type="text/css">
      form{
        margin-top: 90px
      }
      button{
        margin-top: 80px;
      }
      .list{
        margin-left: 85px
      }
  </style>
@stop
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="page-title">
        <div class="title_right">
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
            </div>
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('listEmail') !!}">
                    <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                </a>
            </div>
        </div>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
      <li class="breadcrumb-item"><a href="{!! route('listEmail') !!}">Danh sách mail</a></li>
      <li class="breadcrumb-item active" style="color: red">Gửi mail</li>
    </ol>
    <!-- Main content -->
    <section class="content">
        <div class="container">
        	<div class="row">
	            <form action="" method="post" enctype="multipart/form-data" id="demo-form2"  class="form-horizontal form-label-left" novalidate="">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Tiêu đề</font>
                            </font>
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="title" class="form-control col-md-7 col-xs-12" value="" placeholder="Nhập tiêu đề" autofocus>
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('title') }}</span>
                    </div>
                    
                    <div class="form-group {{ $errors->has('noidung') ? 'has-error' : '' }}">
                        <label class="col-xs-3 control-label">Nội dung
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="noidung" class="form-control" rows="3"  placeholder="Nhập nội dung" @if($errors->has('noidung')) autofocus @else @endif></textarea>
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('noidung') }}</span>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3" style="margin-top: -60px">
                            <button type="submit" class="btn btn-success">
                               <span class="glyphicon glyphicon-send"></span>
                              Gửi
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="row">
            
        </div>
        

    </section>
    <!-- /.content -->
</div>
@stop
