@extends('backend::layouts.master')
@section('title')
    Danh sách email đăng ký
@stop
@section('styleSheet')
  <style type="text/css">
      .add{
        margin-top: 60px;
      }
 
      input[type='text']{
         margin-left: 15px;
         width: 95% !important
      }
      .pagination{
        margin-right: 125px !important;
      }
      div.dataTables_wrapper div.dataTables_filter{
          text-align: left;
          margin-left: -575px;
      }
      div.dataTables_wrapper div.dataTables_paginate{
          margin: 0;
          white-space: nowrap;
          text-align: right;
          margin-right: 64px;
      }
  </style>
@stop
@section('content')
 <div class="content-wrapper">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a>
    </li>
      <li class="breadcrumb-item active" style="color: red">Danh sách email</li>
    </ol>

    <div class="clearfix"></div>
    @if (session('info'))
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{session('info')}}
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container">
        	<div class="row">
	            <div class="x_panel">
                    <div class="page-title">
                        <div class="title_right " style="margin-left: 900px;">
                            <a href="{{route('sendMail')}}">
                                <button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-send" aria-hidden="true"></i> Gửi Email cho tất cả</button>
                            </a>
                        </div>
                    </div>
                    <div class="x_content">
                        <table id="data-table2" class="table table-bordered table-hover">
                            <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th width="30px;">STT</th>
                                <th class="no-sorting">Email</th>
                                <th>Ngày Thêm mới</th>
                            </tr>
                            </thead>
                            <?php $stt = 1; ?>

                            <tbody>
                            @foreach($emailRegister as $email)
                                <tr>
                                    <td >{!! $stt !!}</td>
                                    <td >{!! $email->email !!}</td>

                                    <td >
                                        {!! date_format(new DateTime($email->created_at),'d-m-Y') !!}
                                    </td>
                                </tr>
                                <?php $stt++; ?>
                            @endforeach()
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            
        </div>
        

    </section>
    <!-- /.content -->
</div>
@stop
@section('scriptAdd')
    <script type="text/javascript">

        //xóa 
        $(document).ready(function(){

            $(document).on('click', '.delete_data_banner', function(e){
                delete_data_userAPI = $(this).data('action');

                swal({
                        title: "Bạn Có Chắc chắn Xóa Item Này？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,
                    
                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_userAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('emailRegister')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });

    </script>
@endsection
