@extends('backend::layouts.master')

@section('title')
  Danh sách đề tài
@stop
@section('styleSheet')
 <link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
	        <div class="title_right">
	            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right">
	                <a href="{!! route('ListProject-show') !!}">
	                    <button class="btn btn-sm btn-success add"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
	                </a>
	            </div>
	        </div>
	    </div>

        <ul class="breadcrumb">
            <li><a href=""><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="#" style="color: red">Danh sách đồ án</a></li>
        </ul>

        <div style="float: right; margin-top: 5px">
            <button type="button" class="btn btn-sm" style="background: #77b315; color: #fff" data-toggle="modal" data-target="#exampleModal">
                <span class="glyphicon glyphicon-arrow-up"></span> Import file excel
            </button>
        </div> 

        <div class="clearfix"></div>
	    @if (session('info'))
	        <div class="alert alert-info">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            {{session('info')}}
	        </div>
	    @endif      

        <section class="content container" id="users">
            <div class="row">
                <div class="col-md-12">                    
                    <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th>STT</th>
                                <th class="no-sorting">Tên đồ án</th>
                                <th class="no-sorting">Tên giảng viên</th>
                                <th class="no-sorting">Mô tả</th>
                                <th class="no-sorting">Hành động</th>
                            </tr>
                        </thead>
                        <?php $stt = 1; ?>
                        <tbody>
                        	@foreach($list as $list)
                            <tr>
                                <td>{!! $stt !!}</td>
                                <td>{!! $list->name !!}</td>
                                <td>{!! $list->l_fullname !!}</td>
                                <td>{!! $list->description !!}</td>
                                <td>

                                     @if(strtotime(date('d-m-Y')) <= strtotime(date_format(new DateTime($list->expried_at),'d-m-Y')))
                                        <a href="{!! route('ListProject-edit',['id' => $list->id]) !!}">
                                            <button type="button" id="delete" class="btn btn-xs btn-info">
                                                <i class="fa fa-edit" aria-hidden="true"></i> sửa
                                            </button>
                                        </a>
                                       <a href="#" class="btn btn-danger btn-xs  delete_data_list delete_{{ $list->id}}" data-action='{{url("assist/danh-sach-de-tai/delete/$list->id")}}' data-id="{{ $list->id}}"  ><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                                    @endif   
                                </td>
                            </tr>
                            <?php $stt++; ?>
                            @endforeach()
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

     <!-- modal import file excel -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Import danh sách đề tài từ file excel</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="cart">
                <form method="post" action="{!! route('importListProject') !!}" id="form-register" name="registerForm" novalidate class="form-validate form-edit" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="cart-body">
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                             <input type="file" name="import">
                                            <div class="mda-form-control-line"></div>
                                            <label>chọn file import</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                      </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@stop
@section('scriptAdd')
    <script type="text/javascript">

        //xóa 
        $(document).ready(function(){

            $(document).on('click', '.delete_data_list', function(e){
                delete_data_userAPI = $(this).data('action');

                swal({
                        title: "Bạn có chắc chắn muốn xóa đề tài này？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,
                    
                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_userAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('assist/danh-sach-de-tai')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });

    </script>
@endsection
