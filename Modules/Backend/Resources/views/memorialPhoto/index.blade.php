@extends('backend::layouts.master')
@section('title')
    Danh sách hình ảnh tiêu biểu
@stop
@section('styleSheet')
  <style type="text/css">
      .add{
        margin-top: 60px;
      }
      div.dataTables_wrapper div.dataTables_filter{
          text-align: left;
          margin-left: -575px;
      }
      div.dataTables_wrapper div.dataTables_paginate{
          margin: 0;
          white-space: nowrap;
          text-align: right;
          margin-right: 64px;
      }
  </style>
@stop
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="page-title">
        <div class="title_right">
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right">
                <a href="{!! route('addMemorialPhoto') !!}">
                    <button class="btn btn-sm btn-success add"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
                </a>
            </div>
        </div>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a>
      </li>
      <li class="breadcrumb-item active" style="color: red">Danh sách hình ảnh tiêu biểu</li>
    </ol>
    <div class="clearfix"></div>
    @if (session('info'))
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{session('info')}}
        </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="container">
        	<div class="row">
	            <div class="x_panel">
                    <div class="x_content">
                        <table class="table table-hover"  id="data-table2">
                            <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th width="30px;">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">STT</font>
                                    </font>
                                </th>
                                <th class=" no-sorting">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Tên</font>
                                    </font>
                                </th>
                                <th class="  no-sorting">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Hình ảnh</font>
                                    </font>
                                </th>
                                <th class="  no-sorting">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Hành động</font>
                                    </font>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $stt = 1; ?>
                            @foreach($photo as $pho)
                                <tr>
                                    <td scope="row">
                                        <font style="vertical-align: inherit;">
                                            <font>
                                                {!! $stt !!}
                                            </font>
                                        </font>
                                    </td>
                                    <td scope="row">
                                        <font style="vertical-align: inherit;">
                                            <font>
                                                {!! $pho->name !!}
                                            </font>
                                        </font>
                                    </td>
                                    <td >
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">
                                                <img src="{{ url('/') }}/upload/memorialPhoto/{!! $pho->image !!}" alt="hình ảnh" style="width: 150px; height: 100px">
                                            </font>
                                        </font>
                                    </td>
                                    <td>
                                        <a href="{!! route('editMemorialPhoto',$pho->id) !!}">
                                            <button type="button" id="delete" class="btn btn-xs btn-info">
                                                <i class="fa fa-edit" aria-hidden="true"></i> sửa
                                            </button>
                                        </a>
                                        <a href="#" class="btn btn-danger btn-xs  delete_data_memorial delete_{{ $pho->id}}" data-action='{{url("admin/hinh-anh-ky-niem/delete/$pho->id")}}' data-id="{{ $pho->id}}"  ><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                                    </td>
                                </tr>
                                <?php $stt++; ?>
                            @endforeach
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            
        </div>
        

    </section>
    <!-- /.content -->
</div>
@stop

@section('scriptAdd')
    <script type="text/javascript">
        $(document).ready(function(){

            $(document).on('click', '.delete_data_memorial', function(e){
                delete_data_userAPI = $(this).data('action');

                swal({
                        title: "Bạn có chắc chắn muốn xóa？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,
                    
                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_userAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('admin/hinh-anh-ky-niem/danh-sach')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });



    </script>
@endsection

