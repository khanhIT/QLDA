@extends('backend::layouts.master')
@section('title')
    Sửa hình ảnh tiêu biểu
@stop
@section('styleSheet')
  <style type="text/css">
      form{
        margin-top: 90px
      }
      button{
        margin-top: 80px;
      }
      .list{
        margin-left: 85px
      }
  </style>
@stop
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="page-title">
        <div class="title_right">
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('addMemorialPhoto') !!}">
                    <button class="btn btn-sm btn-success"><i class="fa fa-plus" aria-hidden="true"></i>Thêm mới</button>
                </a>
            </div>
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('listMemorialPhoto') !!}">
                    <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                </a>
            </div>
        </div>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
      <li class="breadcrumb-item"><a href="{!! route('listMemorialPhoto') !!}">Danh sách hình ảnh tiêu biểu</a></li>
      <li class="breadcrumb-item active" style="color: red;">Sửa hình ảnh tiêu biểu</li>
    </ol>
    <!-- Main content -->
    <section class="content">
        <div class="container">
        	<div class="row">
	            <form action="" method="post" enctype="multipart/form-data" id="demo-form2"  class="form-horizontal form-label-left" novalidate="">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Tên</font>
                            </font>
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="name" value="{!! $photo->name !!}" class="form-control col-md-7 col-xs-12">
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('name') }}</span>
                    </div>
                    
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label class="col-xs-3 control-label">Hình ảnh
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" onchange='showFile(event)' name="image" class="form-control col-md-7 col-xs-12">
                            @if(!empty($photo->image))
                                <img id="image" class="" src="{!! url('/') !!}/upload/memorialPhoto/{!! $photo->image !!}" width="570" height="200" style="margin-top: 15px"/>
                            @else 
                                <img src="{!! url('/') !!}/upload/memorialPhoto/5aadd16308382.jpg" width="570" height="200" style="margin-top: 15px">
                            @endif
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('image') }}</span>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-ok"></span>
                                Cập nhập
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="row">
            
        </div>
        

    </section>
    <!-- /.content -->
</div>
@stop
