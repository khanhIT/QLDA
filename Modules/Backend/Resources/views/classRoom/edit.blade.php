@extends('backend::layouts.master')

@section('title')
Quản lý Lớp Chuyên Ngành
@stop
@section('styleSheet')
  <style type="text/css">

    .list{
      margin-left: 85px
    }

  </style>
@endsection
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <ul class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
    <li><a href="{!! route('classroom-index') !!}">Lớp Chuyên Ngành</a></li>
    <li><a href="#" style="color: red">Chỉnh sửa</a></li>
  </ul>

  <div class="page-title">
    <div class="title_right">
      <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
        <a href="{!! route('classroom-create') !!}">
          <button class="btn btn-sm btn-success"><i class="fa fa-plus" aria-hidden="true"></i>Thêm mới</button>
        </a>
      </div>
      <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
        <a href="{!! route('classroom-index') !!}">
          <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
        </a>
      </div>
    </div>
  </div>
  <section class="content container" id="classRoom">
    <div class="row">
      <div class="col-xs-12">                    

        <div class="col-md-3"></div>
        <div class="col-md-6 add-user">
          <form action="" method="POST" >
            {{ csrf_field() }}
            <div class="form-group  {{ $errors->has('c_name') ? 'has-error' : '' }}">
              <label for="exampleInputEmail1">Tên Lớp Chuyên Ngành</label>
              <input type="text" class="form-control" name="c_name" value="{{ old('c_name', $edit_class->c_name) }}">
              <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('c_name') }}</span>
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
              <label for="exampleInputEmail1">Mô tả</label>
              <textarea name="description" id="inputDescription" class="form-control" rows="3"  >{{ old('description', $edit_class->description) }}</textarea>
              <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('description') }}</span>
            </div>  
            <div class="class-action">
             <button type="submit" class="btn btn-success">
              <span class="glyphicon glyphicon-ok"></span> Cập Nhật</button>
            </div>

          </div>
        </form>
        <div class="col-md-3"></div>
      </div>
    </div>
  </section>
</div>
@stop