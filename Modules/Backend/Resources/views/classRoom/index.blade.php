@extends('backend::layouts.master')

@section('title')
Quản lý Lớp Học Chuyên Ngành
@stop
@section('styleSheet')
   <style type="text/css">
       .add{
          margin-top: 60px
       }
       div.dataTables_wrapper div.dataTables_filter{
           text-align: left;
           margin-left: -575px;
       }
       div.dataTables_wrapper div.dataTables_paginate{
           margin: 0;
           white-space: nowrap;
           text-align: right;
           margin-right: 64px;
       }
   </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
            <div class="title_right">
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right">
                    <a href="{{route('classroom-create')}}">
                        <button class="btn btn-sm btn-success add"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
                    </a>
                </div>
            </div>
        </div>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="#" style="color: red">Lớp Học Chuyên Ngành</a></li>
        </ul>
                    

        <section class="content container" id="classRoom">
            <div class="row">

                <div class="col-xs-12">
                    <div class="col-md-11">
                        @if (session('info'))
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{session('info')}}
                            </div>
                        @endif
                    </div>

                        	 <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th width="30px;">STT</th>
                                <th class="no-sorting">Tên Lớp Chuyên Ngành</th>
                                <th class="no-sorting">Mô Tả</th>
                                 <th class="no-sorting">Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $n=1?>
                        @foreach($list_class as $class)
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{$class->c_name}}</td>
                                <td>{{$class->description}}</td>
                                <td>
                                    <a href="{{route('classroom-edit',['id'=>$class->id])}}" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-pencil"></span> Sửa</a>
                                    <a href="#" class="btn btn-danger btn-xs  delete_data_class delete_{{ $class->id}}" data-action='{{url("admin/classroom/delete/$class->id")}}' data-id="{{ $class->id}}"  ><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                                </td>
                            </tr>
                            <?php $n++?>
                            @endforeach
                        </tbody>
                    </table>
                       
                   
                </div>
            </div>
        </section>
    </div>
@section('scriptAdd')
    <script type="text/javascript">
        $(document).ready(function(){

            // readProducts();  it will load products when document loads

            $(document).on('click', '.delete_data_class', function(e){
                delete_data_classAPI = $(this).data('action');

                swal({
                        title: "Bạn có chắc chắn muốn xóa？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,

                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_classAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('admin/classroom')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });



    </script>
@endsection
@stop
