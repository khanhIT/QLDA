@extends('backend::layouts.master')

@section('title')
   Quản lý tin tức
@stop
@section('styleSheet')
<link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
<style type="text/css">
	.table-detail-01 tr th:first-child{
		width: 20%
	}
	.table-detail-01 tr td:nth-child(2){
		padding-top: 20px;
        padding-bottom: 40px
	}
</style>
@endsection
@section('content')
   <div class="content-wrapper">
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{{route('news-index')}}">Danh sách tin tức</a></li>
             <li><a href="#"  style="color: red">Xem chi tiết</a></li>
        </ul>
                  
        <section class="content container" id="users">
            <div class="row">
                <div class="col-xs-12">
                      <div class="col-md-11">
                      	<div class="card">
                      		<div class="card-body item-detail-01">
	                      		<div class="main_title">
				                    <h3 class="title-03 fleft">
				                        {{$news->title}}
				                    </h3>
				                </div>
	                            <div class="card-body">
			                      <table class="table-detail-01">
			                      	<tbody>
			                      		<tr>
			                      			<th>Nội dung</th>
			                      			<td>
			                      				<textarea class="form-control" id="exampleFormControlTextarea1" rows="15">{{$news->content}}</textarea>
			                      			</td>
			                      		</tr>
			                      		<tr>
			                      			<th>Hình ảnh</th>
			                      			@if(!empty($news->image))
					                      	   <td>
					                      	   	   <img src="{!! url('/') !!}/upload/news/{!! $news->image !!}" width="100%" height="100%">
					                      	   </td>
					                        @else()
					                            <td>
					                            	Không có hình ảnh
					                            </td>
					                        @endif	
			                      		</tr>
			                      		<tr>
			                      			<th>Hình ảnh nội dung</th>
			                      			@if(!empty($news->image_content))
					                      	   <td>
					                      	   	   <img src="{!! url('/') !!}/upload/news/{!! $news->image_content !!}" width="100%" height="100%">
					                      	   </td>
					                        @else()
					                            <td>
					                            	Không có hình ảnh
					                            </td>
					                        @endif	
			                      		</tr>
			                      		
			                      	</tbody>
			                      </table>
			                    </div>
			                </div>
			            </div>
	                      <a href="{!! route('news-index') !!}" class="btn btn-sm btn-info">
	                       <span class="glyphicon glyphicon-arrow-left"></span>Trở lại</a>
                      </div>
                </div>
            </div>
        </section>
    </div>
@stop
