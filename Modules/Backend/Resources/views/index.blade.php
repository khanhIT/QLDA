@extends('backend::layouts.master')
@section('title')
    Trang quản trị Admin
@stop
@section('content')
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Quản lý đồ án tốt nghiệp
        </h1>
        
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6" title="Tổng số sinh viên">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $cus }}</h3>

                        <p>Tổng số sinh viên đăng ký</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="{{ route('students-index') }}" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6" title="Tổng số đồ án">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ $project }}</h3>
                        <!-- <sup style="font-size: 20px">%</sup> -->
                        <p>Tổng số đồ án</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{{ route('project-index') }}" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6" title="Sinh viên đạt">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ $countPass }}</h3>

                        <p>Sinh viên đạt</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6" title="Sinh viên không đạt">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{ $countNoPass }}</h3>

                        <p>Sinh viên không đạt</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">Xem thêm <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div>
                
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <table class="table table-hover">
                  <thead>
                    <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                      <th scope="col">#</th>
                      <th scope="col">Hình ảnh</th>
                      <th scope="col">Họ tên</th>
                      <th scope="col">Tên đăng nhập</th>
                      <th scope="col">Email</th>
                      <th scope="col">SĐT</th>
                      <th scope="col">Địa chỉ</th>
                      <th scope="col">Quyền</th>
                    </tr>
                  </thead>
                  <?php $n = 1; ?>
                  <tbody>
                    @foreach($users as $user)
                        <tr>
                          <th scope="row">{{ $n }}</th>
                          <td>
                              @if($user->avatar != '')
                                  <img src="{!! url('/') !!}/upload/users/{!! $user->avatar !!}" width="150px" height="100px">
                              @else
                                <img src="/backend/dist/img/no-logo.png" width="150px" height="100px"/>
                              @endif
                          </td>
                          <td>{{ $user->u_fullname }}</td>
                          <td>{{ $user->u_name }}</td>
                          <td>{{ $user->u_email }}</td>
                          <td>{{ $user->mobile }}</td>
                          <td>{{ $user->address }}</td>
                          <td>
                            @if($user->role_id == 1)
                              <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="admin">admin</button>
                            @else  
                              <button class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="right" title="giáo viên">giaovien</button>
                            @endif  
                          </td>
                        </tr>
                    <?php $n++; ?>
                    @endforeach()
                  </tbody>
                </table>
                <div style="float: right">{{ $users->links() }}</div>
            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
@stop
