@extends('backend::layouts.master')

@section('title')
    Trả lời liên hệ
@stop
@section('styleSheet')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <ul class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
        <li><a href="{!! route('listContact') !!}">Liên hệ</a></li>
        <li><a href="#" style="color: red">Trả lời</a></li>
    </ul>

    <section class="content container" id="course">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-right col-md-11">
                    <a href="{!! route('listContact') !!}">
                        <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                    </a>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-6 add-user">
                    <form action="" method="POST" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên người liên hệ</label>
                            <input type="text" class="form-control" name="name" value="{{$listContact->name}}" READONLY>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email Người liên hệ</label>
                            <input type="email" class="form-control" name="email"  value="{!!$listContact->email!!}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Địa chỉ người liên hệ</label>
                            <input type="text" class="form-control" name="address"  value="{!!$listContact->address!!}"  readonly>
                        </div>

                        <div class=" form-group">
                            <label for="exampleInputEmail1">Nội dung người gửi</label>
                            <textarea name="noidung" id="input" class="form-control" rows="4" disabled>{!!$listContact->content !!}</textarea>
                        </div>
                        <div class="form-group {{ $errors->has('reply') ? 'has-error' : '' }}">
                            <label for="exampleInputEmail1">Trả lời</label>
                            <textarea name="reply" id="input" class="form-control" rows="4" placeholder="Nhập câu trả lời" >{!!old('reply')!!}</textarea>
                            <span class="text-danger">{{ $errors->first('reply') }}</span>
                        </div>
                        <div class="user-action">
                            <button type="submit" class="btn btn-success">
                                <span class="glyphicon glyphicon-send"></span> Trả Lời</button>
                        </div>

                </div>
                </form>
                <div class="col-md-3"></div>
            </div>
        </div>
    </section>
</div>
@stop
