@extends('backend::layouts.master')
@section('title')
    Danh sách liên hệ
@stop
@section('styleSheet')
  <style type="text/css">
      .add{
        margin-top: 60px;
      }
      div.dataTables_wrapper div.dataTables_filter{
          text-align: left;
          margin-left: -575px;
      }
      div.dataTables_wrapper div.dataTables_paginate{
          margin: 0;
          white-space: nowrap;
          text-align: right;
          margin-right: 64px;
      }
  </style>
@stop
@section('content')
 <div class="content-wrapper">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a>
      </li>
      <li class="breadcrumb-item active" style="color: red">Danh sách liên hệ</li>
    </ol>
    <!-- Main content -->
    <section class="content">
        <div class="container">
        	<div class="row">
                @if (session('info'))
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{session('info')}}
                    </div>
                @endif
	            <div class="x_panel">
                    <div class="x_content">
                        <table class="table table-hover"  id="data-table2">
                            <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th width="30px;">
                                    STT
                                </th>
                                <th class="no-sorting">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Tên</font>
                                    </font>
                                </th>
                                <th class="no-sorting">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Email</font>
                                    </font>
                                </th>
                                <th class="no-sorting">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Địa chỉ</font>
                                    </font>
                                </th>
                                <th class="no-sorting">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Nội dung</font>
                                    </font>
                                </th>
                                <th class="no-sorting" style="width: 180px;">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">Hành động</font>
                                    </font>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $stt=1;?>
                            @foreach($contact as $contact)
                                <tr>
                                    <td scope="row">
                                        <font style="vertical-align: inherit;">
                                            <font>
                                                {{$stt}}
                                            </font>
                                        </font>
                                    </td>
                                    
                                    <td scope="row">
                                        <font style="vertical-align: inherit;">
                                            <font>
                                                {!! $contact->name !!}
                                            </font>
                                        </font>
                                    </td>
                                    <td scope="row">
                                        <font style="vertical-align: inherit;">
                                            <font>
                                                {!! $contact->email !!}
                                            </font>
                                        </font>
                                    </td>
                                    <td scope="row">
                                        <font style="vertical-align: inherit;">
                                            <font>
                                                {!! $contact->address !!}
                                            </font>
                                        </font>
                                    </td>
                                    <td scope="row">
                                        <font style="vertical-align: inherit;">
                                            <font>
                                                @if(!empty($contact))
                                                    <?php $string=$contact->content?>
                                                    @if(strlen($string)>59)
                                                        {{ substr($string,0,60)}}...
                                                    @else
                                                        {!! $contact->content !!}
                                                    @endif
                                                @endif
                                            </font>
                                        </font>
                                    </td>
                                    <td>
                                        <a href="{{route('replyContact',['id'=>$contact->id])}}">
                                            <button type="button" class="btn btn-xs btn-primary">
                                                <i class="glyphicon glyphicon-comment" aria-hidden="true"></i> Reply
                                            </button>
                                        </a>

                                    </td>
                                </tr>
                                <?php $stt++;?>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            
        </div>
        

    </section>
    <!-- /.content -->
</div>
@stop
