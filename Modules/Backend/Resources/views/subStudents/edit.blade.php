@extends('backend::layouts.master')

@section('title')
Quản lý sinh viên
@stop
@section('stylesheet')
@endsection
@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<ul class="breadcrumb">
		<li><a href=""><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
		<li><a href="{!! route('student_index') !!}" style="color: red">Sinh viên</a></li>
	</ul>
	<div class="page-title">
        <div class="title_right">
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('student_index') !!}">
                    <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                </a>
            </div>
        </div>
    </div>
	<section class="content container" id="lecturers">
		<div class="row">
			<div class="col-xs-12">
				<div class="col-md-3"></div>
				<div class="col-md-11 add-user">
					<form action="" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="col-md-6">
							<div class="form-group {{ $errors->has('msv') ? 'has-error' : '' }}" >
								<label for="exampleInputEmail1">Mã sinh viên</label>
								<input type="number" class="form-control" name="msv"  value="{{ old('msv', $custormer->msv) }}">
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('msv') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('fullname') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Họ và tên</label>
								<input type="text" class="form-control" name="fullname" value="{{ old('fullname', $custormer->fullname) }}">
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('fullname') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('phone') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Số điện thoại</label>
								<input type="text" class="form-control" name="phone" value="{{ old('phone', $custormer->phone) }}">
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('phone') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('birthday') ? 'has-error' : '' }}" style="margin-top: 26px">
								<label for="exampleInputPassword1">Ngày sinh</label>
								<input type="date" class="form-control" name="birthday" maxlength="11" value="{{ old('birthday', $custormer->birthday) }}" >
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('birthday') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('address') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Địa chỉ</label>
								<input type="text" class="form-control" name="address" value="{{ old('address', $custormer->address) }}">
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('address') }}</span>
							</div>

						</div>
						<div class="col-md-6">
							<div class="form-group  {{ $errors->has('project_name') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Tên đồ án</label>
								@if(strlen($custormer->project_name) > 2)
								<input name="project_name" id="inputInfo" class="form-control" rows="3" value="{{old('project_name', $custormer->project_name)}}">
								@else()
								<input name="project_name" id="inputInfo" class="form-control" rows="3" value="{{old('project_name', $custormer->name)}}">
								@endif()
								
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('project_name') }}</span>
							</div>

							<div class="form-group  {{ $errors->has('c_id') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Lớp chuyên ngành</label>
								<select name="c_id" id="inputGender" class="form-control"  >
									<option value="">--Chọn lớp chuyên ngành--</option>
									@foreach($className as $class)
									<option value="{!! $class->id !!}" {!! $class->id == old('c_id',$custormer->c_id)  ? "selected" : "" !!}>{!! $class->c_name !!}</option>
									@endforeach
								</select>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('c_id') }}</span>
							</div>
							
							<div class="form-group  {{ $errors->has('s_id') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Khóa học</label>
								<select name="s_id" id="inputGender" class="form-control">
									<option value="">--Chọn khóa--</option>

									@foreach($school as $school)
									<option value="{!! $school->id !!}" {!! $school->id == old('s_id',$custormer->s_id)  ? "selected" : "" !!}>{!! $school->s_key !!}</option>
									@endforeach()
									
								</select>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('s_id') }}</span>
							</div>

                            <div class="form-group  {{ $errors->has('l_id') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Giảng viên hướng dẫn</label>
								<select name="l_id" id="inputGender" class="form-control">
									<option value="">--Chọn GVHD--</option>

									@foreach($lec as $lec)
									<option value="{!! $lec->id !!}" {!! $lec->id == old('
										l_id',$custormer->l_id)  ? "selected" : "" !!}>{!! $lec->l_fullname !!}</option>
									@endforeach()
									
								</select>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_id') }}</span>
							</div>
							<div class="user-action" style="margin-left: 20px">
								<button type="submit" class="btn btn-success">
									<span class="glyphicon glyphicon-ok"></span> Cập nhập</button>
								<button type="reset" class="btn btn-danger">
									<span class="glyphicon glyphicon-refresh"></span> Nhập lại</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
	@stop
