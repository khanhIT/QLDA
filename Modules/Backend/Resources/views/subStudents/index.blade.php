@extends('backend::layouts.master')

@section('title')
Quản lý Sinh Viên
@stop
@section('styleSheet')
 <link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       
        <ul class="breadcrumb">
            <li><a href=""><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="#" style="color: red">Sinh viên</a></li>
        </ul>
        @if (session('info'))
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('info')}}
            </div>
        @endif            
        <section class="content container" id="users">
            <div class="row">
                <div class="col-md-12">                    
                    <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th>STT</th>
                                <th class="no-sorting">MSSV</th>
                                <th class="no-sorting">Họ và Tên</th>
                                <th class="no-sorting">Số điện thoại</th>
                                <th class="no-sorting">Ngày sinh</th>
                                <th class="no-sorting">Địa chỉ</th>
                                <th class="no-sorting">Tên đồ án</th>
                                <th class="no-sorting">Lớp Chuyên ngành</th>
                                <th class="no-sorting">Khóa học</th>
                                <th class="no-sorting">GVHD</th>
                                <th class="no-sorting">Hành động</th>
                                
                            </tr>
                        </thead>
                        <?php $stt = 1; ?>
                        <tbody>
                            @foreach($custormer as $cus)
                            <tr>
                                <td>{!! $stt !!}</td>
                                <td>{!! $cus->msv !!}</td>
                                <td>{!! $cus->fullname !!}</td>
                                <td>{!! $cus->phone !!}</td>
                                <td>{!! $cus->birthday !!}</td>
                                <td>{!! $cus->address !!}</td>
                                 @if(strlen($cus->project_name) > 2)
                                   <td>{!! $cus->project_name !!}</td>
                                @else
                                   <td>{!! $cus->name !!}</td>
                                @endif
                                <td>{!! $cus->c_name !!}</td>
                                <td>K{!! $cus->s_key !!}</td>
                                <td>{!! $cus->l_fullname !!}</td>
                                <td>
                                    <a href="{!! route('student_show',['id' => $cus->id]) !!}">
                                        <button type="button" id="delete" class="btn btn-xs btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i> xem
                                        </button>
                                    </a>
                                   <a href="{!! route('student_edit',['id' => $cus->id]) !!}">
                                        <button type="button" id="delete" class="btn btn-xs btn-warning">
                                            <i class="fa fa-edit" aria-hidden="true"></i> sửa
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            <?php $stt++; ?>
                            @endforeach()
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
@stop
