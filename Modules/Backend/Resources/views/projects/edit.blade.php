  @extends('backend::layouts.master')

@section('title')
    Quản lý đồ án
@stop
@section('styleSheet')
   <style type="text/css">
         
         .toggle input[type="radio"]{
            display: none;
         }

        .toggle input[type="radio"] + .label-text:before{
          content: "\f204";
          font-family: "FontAwesome";
          speak: none;
          font-style: normal;
          font-weight: normal;
          font-variant: normal;
          text-transform: none;
          line-height: 1;
          -webkit-font-smoothing:antialiased;
          width: 1em;
          display: inline-block;
          margin-right: 10px;
        }

        .toggle input[type="radio"]:checked + .label-text:before{
          content: "\f205";
          color: #16a085;
          animation: effect 250ms ease-in;
        }

        .toggle input[type="radio"]:disabled + .label-text{
          color: #aaa;
        }

        .toggle input[type="radio"]:disabled + .label-text:before{
          content: "\f204";
          color: #ccc;
        }


        @keyframes effect{
          0%{transform: scale(0);}
          25%{transform: scale(1.3);}
          75%{transform: scale(1.4);}
          100%{transform: scale(1);}
        }

   </style>
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{!! route('project-index') !!}">Đồ án</a></li>
             <li><a href="#" style="color: red">Chỉnh sửa</a></li>
        </ul>
                    
         <div class="page-title">
            <div class="title_right">
                
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <a href="{!! route('project-index') !!}">
                        <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                    </a>
                </div>
            </div>
        </div>
        <section class="content container" id="lecturers">
            <div class="row">
                <div class="col-xs-12"> 
                    <div class="col-md-3"></div>
                   		<div class="col-md-6 add-user">
                   			<form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate="">
                          {{ csrf_field() }}
                   				<div class="form-group">
          							    <label for="exampleInputEmail1">Họ và Tên</label>
          							    <input type="text" class="form-control" name="l_fullname" value="{!! $project->fullname !!}" readonly="">
          							  </div>

          							  <div class="form-group">
          							    <label for="exampleInputPassword1">Tên Đồ Án</label>
          							    
                            @if(strlen($project->name_project) > 2)
                             <input type="text" class="form-control" name="name_project" value="{!! $project->name_project !!}" readonly="">
                            @else()
                            <input type="text" class="form-control" name="name_project" value="{!! $project->name !!}" readonly="">
                            @endif()
          							  </div>

          							  <div class="form-group {{ $errors->has('file_upload_word') ? 'has-error' : '' }}">
          							    <label for="exampleInputPassword1">Upload File Word</label>
          							    <input type="file" class="form-control" name="file_upload_word" value="{!! $project->file_upload_word !!}">
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('file_upload_word') }}</span>
          							  </div>

                         	<div class="form-group {{ $errors->has('upload_source_code') ? 'has-error' : '' }}">
        								    <label for="exampleInputPassword1">Upload File Code</label>
        								    <input type="file" class="form-control" name="upload_source_code" value="{!! $project->upload_source_code !!}">
                            <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('upload_source_code') }}</span>
        							  	</div>
                          
                          <div class="form-group">
                            <label for="exampleInputPassword1">Cán bộ đọc chấm</label>
                             <input type="text" class="form-control" name="cadres_read" value="{!! old('cadres_read', $project->cadres_read) !!}">
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Điểm</label>
                             <input type="number" min="0" max="10" class="form-control" name="point" value="{!! old('point', $project->point) !!}">
                          </div>
                          
                          <div class="form-group">
                            <label for="exampleFormControlSelect1">Hội đồng</label>
                            <select name="group" class="form-control" id="exampleFormControlSelect1">
                              <option>-- Chọn hội đồng --</option>
                              <option value="1"  {{$project->group == '1' ? "selected" : "" }} {{ old('group')=="1" ? 'selected='.'"'.'selected'.'"' : '' }}>1</option>
                              <option value="2" {{$project->group == '2' ? "selected" : "" }} {{ old('group')=="2" ? 'selected='.'"'.'selected'.'"' : '' }}>2</option>
                              <option value="3" {{$project->group == '3' ? "selected" : "" }} {{ old('group')=="3" ? 'selected='.'"'.'selected'.'"' : '' }}>3</option>
                              <option value="4" {{$project->group == '4' ? "selected" : "" }} {{ old('group')=="4" ? 'selected='.'"'.'selected'.'"' : '' }}>4</option>
                            </select>
                          </div>

                          <div class="form-group">
                            <label for="exampleInputPassword1">Trạng thái</label>
                            <div class="form-check">
                                <label class="toggle">
                                  <input type="radio" name="status" value="0" {{$project->status == '0' ? "checked" : "" }} {{ old('status')=="0" ? 'checked='.'"'.'checked'.'"' : '' }}> 
                                  <span class="label-text">Chưa bảo vệ</span>
                                </label>
                              </div>
                              <div class="form-check">
                                <label class="toggle">
                                  <input type="radio" name="status" value="1" {{$project->status == '1' ? "checked" : "" }} {{ old('status')=="1" ? 'checked='.'"'.'checked'.'"' : '' }}> 
                                  <span class="label-text">Đã bảo vệ</span>
                                </label>
                              </div>      
                          </div>

                     			<div class="user-action">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>  Cập nhât</button>
  	                   			
                     			</div>
                   		  </form>
                      </div>
                   		<div class="col-md-3"></div>
                </div>
            </div>
        </section>
    </div>
@stop
