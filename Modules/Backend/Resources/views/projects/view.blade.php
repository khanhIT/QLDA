@extends('backend::layouts.master')

@section('title')
   Quản lý đồ án
@stop
@section('styleSheet')
<link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
<style type="text/css">
	.table-detail-01 tr th:first-child{
		width: 50%
	}
	.table-detail-01{
		width: 100%;
	}
	iframe{
		margin-top: 20px;
		width: 200%; 
		height: 400px
	}
	.project-detail{
        color: #448AFF
	}
</style>
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
	    <ul class="breadcrumb">
	        <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
	        <li><a href="{!! route('project-index') !!}">Đồ án</a></li>
	         <li><a href="#"  style="color: red">Xem chi tiết</a></li>
	    </ul>        
        <section class="content container" id="users">
            <div class="row">
                <div class="col-xs-12">                     
                      <div class="col-md-11">
                      	<div class="card">
                      		<div class="card-body item-detail-01">
	                      		<div class="main_title">
				                    <h3 class="title-03 fleft">
				                        Thông tin đồ án sinh viên
				                    </h3>
				                </div>
	                            <div class="card-body">
			                      <table class="table-detail-01">
			                      	<tbody>
			                      		<tr>
			                      			<th>Mã số sinh viên</th>
			                      			<td>{!! $project->msv !!}</td>
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr class="project-detail">
			                      			<th>Sinh viên</th>
			                      			<td>{!! $project->fullname !!}</td>
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr>
			                      			<th>Giáo viên hướng dẫn</th>
			                      			<td>{!! $project->l_fullname !!}</td>
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr>
			                      			<th>Giáo viên phản viên</th>
			                      			<td>{!! $project->cadres_read !!}</td>
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr>
			                      			<th>Lớp chuyên ngành</th>
			                      			<td>{!! $project->c_name !!}</td>
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr>
			                      			<th>Khóa học</th>
			                      			<td>K{!! $project->s_key !!}</td>
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr class="project-detail">
			                      			<th>Điểm</th>
			                      			<td>{!! $project->point !!}/100</td>
			                      			@if($project->point >= 50)
			                      			<td>
			                      				<button type="button" class="btn btn-outline-success">Đỗ</button>
			                      			</td>
			                      			@elseif($project->point < 50)
			                      			<td>
			                      				<button type="button" class="btn btn-outline-success">Trượt</button>
			                      			</td>
			                      			@else
			                      			@endif()
			                      			<td></td>
			                      		</tr>
			                      		<tr class="project-detail">
			                      			<th>Tên đồ án</th>
			                      			
			                      			@if(strlen($project->name_project) > 5)
			                      			<td>{!! $project->name_project !!}</td>
			                      			@else()
			                      			<td>{!! $project->name !!}</td>
			                      			@endif()
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr class="project-detail">
			                      			<th>Trạng thái</th>
			                      			
			                      			@if($project->status == 1)
			                      			<td>
			                      				<button type="button" class="btn btn-outline-success">Đã bảo vệ</button>
			                      			</td>
			                      			@else()
			                      			<td>
			                      				<button type="button" class="btn btn-outline-secondary">Chưa bảo vệ</button>
			                      			</td>
			                      			@endif()
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr class="project-detail">
			                      			<th>Hội đồng</th>
			                      			<td>{!! $project->group !!}</td>
			                      			<td></td>
			                      			<td></td>
			                      		</tr>
			                      		<tr>
			                      			<th>File word</th>

			                      			<td>
			                      				<p>{!! $project->file_upload_word !!}</p>
			                      			</td>
			                      			@if(empty($project->file_upload_word) == '')
			                      			<td style="cursor: pointer;">
			                      				<a onclick="viewCV('.cv_{{ $project->id }}', this)">
			                      				   Xem file
			                      			    </a>
			                      			</td>
			                      			<td style="cursor: pointer;">
			                      				<a href="{{ route('downloadFile',['id'=> $project->id]) }}">   Download
			                      				</a>
			                      			</td>
			                      			@else
			                      			<td style="display: none;">
			                      				<a onclick="viewCV('.cv_{{ $project->id }}', this)">
			                      				   Xem file
			                      			    </a>
			                      			</td>
			                      			<td style="display: none;">
			                      				<a href="{{ route('downloadFile',['id'=> $project->id]) }}">   Download
			                      				</a>
			                      			</td>
			                      			@endif
			                      		</tr>
			                      		<tr>
			                      			<td class="view-cv cv_{{$project->id}}" style="display: none;">
			                      				<!-- <iframe src="https://docs.google.com/viewer?embedded=true&url=http%3A%2F%2Fhomepages.inf.ed.ac.uk%2Fneilb%2FTestWordDoc.doc" width="100%" height="600"></iframe> -->

			                      				<iframe class="doc" src="https://docs.google.com/gview?embedded=true&amp;url={{ url('/') }}/upload/files/file_word/{{ $project->file_upload_word }}"></iframe>
			                      			</td>
			                      		</tr>
			                      	</tbody>
			                      </table>
			                   </div>
			               </div>
		                </div>
	                      <a href="{!! route('project-index') !!}" class="btn btn-primary">
                              <span class="glyphicon glyphicon-arrow-left"></span> Trở lại
                            </a>
                      </div>
                </div>
            </div>
        </section>
    </div>
@stop
