@extends('backend::layouts.master')

@section('title')
Quản lý đồ án
@stop
@section('styleSheet')
 <link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="#" style="color: red">Danh sách đồ án</a></li>
        </ul>
        <div class="clearfix"></div>
        @if (session('info'))
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('info')}}
            </div>
        @endif
        @if(!empty($projectHĐ1))
        <div style="float: right; margin-right: 24px">
            <a href="{{ route('exportHĐ1') }}" class="btn btn-sm" style="background: #77b315; color: #fff" data-toggle="tooltip" data-placement="top" title="Export to Excel HĐ 1">
              <span class="glyphicon glyphicon-arrow-down"></span> Export 1
            </a>
        </div>
        @endif
        @if(!empty($projectHĐ2))
        <div style="float: right; margin-right: 24px">
            <a href="{{ route('exportHĐ2') }}" class="btn btn-sm" style="background: #77b315; color: #fff" data-toggle="tooltip" data-placement="top" title="Export to Excel HĐ 2">
              <span class="glyphicon glyphicon-arrow-down"></span> Export 2
            </a>
        </div>
        @endif
        @if(!empty($projectHĐ3))
        <div style="float: right; margin-right: 24px">
            <a href="{{ route('exportHĐ3') }}" class="btn btn-sm" style="background: #77b315; color: #fff" data-toggle="tooltip" data-placement="top" title="Export to Excel HĐ 3">
                <span class="glyphicon glyphicon-arrow-down"></span> Export 3
            </a>
        </div>
        @endif
        @if(!empty($projectHĐ4))
        <div style="float: right; margin-right: 24px">
            <a href="{{ route('exportHĐ4') }}" class="btn btn-sm" style="background: #77b315; color: #fff" data-toggle="tooltip" data-placement="top" title="Export to Excel HĐ 4">
                <span class="glyphicon glyphicon-arrow-down"></span> Export 4
            </a>
        </div>
        @endif
        @if(!empty($projectHĐ5))
        <div style="float: right; margin-right: 24px">
            <a href="{{ route('exportHĐ5') }}" class="btn btn-sm" style="background: #77b315; color: #fff" data-toggle="tooltip" data-placement="top" title="Export to Excel HĐ 5">
                <span class="glyphicon glyphicon-arrow-down"></span> Export 5
            </a>
        </div>
        @endif
        <section class="content container" id="users">
            <div class="row">
                <div class="col-md-12">                    
                    <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th width="30px;">STT</th>
                                <th class="no-sorting">Tên Sinh Viên</th>
                                <th class="no-sorting">GVHD</th>
                                <th class="no-sorting">Tên Đồ Án</th>
                                <th class="no-sorting">File Word</th>
                                <th class="no-sorting">File Code</th>
                                <th class="no-sorting">Lớp Chuyên ngành</th>
                                <th class="no-sorting">Điểm</th>
                                <th class="no-sorting">Trạng thái</th>
                                <th class="no-sorting">Hàng động</th>
                            </tr>
                        </thead>
                        <?php $stt = 1; ?>
                        <tbody>
                        @foreach($project as $project)    
                            <tr>
                                <td>{!! $stt !!}</td>
                                <td>{!! $project->fullname !!}</td>
                                <td>{!! $project->l_fullname !!}</td>
                                @if(strlen($project->name_project) > 2)
                                   <td>{!! $project->name_project !!}</td>
                                @else
                                   <td>{!! $project->name !!}</td>
                                @endif
                                <td>{!! $project->file_upload_word !!}</td>
                                <td>{!! $project->upload_source_code !!}</td>
                                <td>{!! $project->c_name !!}</td>
                                <td>{!! $project->point !!}/10</td>
                                @if($project->status == 0)
                                <td>
                                    <button class="btn btn-xs btn-warning">Chưa bảo vệ</button>
                                </td>
                                @else
                                <td>
                                    <button class="btn btn-xs btn-success">Đã bảo vệ</button>
                                </td>  
                                @endif
                                <td>
                                    <a href="{{route('project-show',['id'=>$project->id])}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-eye-open"></span> View</a>
                                    <a href="{{route('project-edit',['id'=>$project->id])}}" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span> Sửa</a>
                                </td>
                            </tr>
                            <?php $stt++; ?>
                        @endforeach()    
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
@stop
