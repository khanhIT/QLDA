<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo" style="height: 55px;">
        <!-- mini logo for sidebar mini 50http://qlda.app:8080/adminx50 pixels -->
        <span class="logo-mini"><b>DA</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>
            <img src="{{ url('/') }}/images/logo/logo DG-01.svg" style="width: 100%; height: 100%" title="Logo">
        </b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" style="height: 55px;">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(!empty(Auth::user()->avatar))
                          <img src="{{ url('/') }}/upload/users/{!! Auth::user()->avatar !!}" class="img-circle" alt="User Image" style="height: 25px; width: 25px">
                        @else
                          <img id="image" src="/backend/dist/img/no-logo.png" style="height: 25px; width: 25px">
                        @endif
                        <span class="hidden-xs">{!! Auth::user()->u_fullname !!}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if(!empty(Auth::user()->avatar))
                              <img src="{{ url('/') }}/upload/users/{!! Auth::user()->avatar !!}" class="img-circle" alt="User Image" style="height: 90px; width: 90px;">
                            @else
                              <img id="image" src="/backend/dist/img/no-logo.png" style="height: 90px; width: 90px">
                            @endif

                            <p>
                                {!! Auth::user()->u_fullname !!}
                                <small>Web Developer</small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{!! route('users-show',Auth::user()->id) !!}" class="btn btn-default btn-flat">Trang cá nhân</a>
                            </div>
                            <div class="pull-right">
                                <a href="@if(Auth::user()->role_id ==1) {{route('admin-logout')}} @endif @if(Auth::user()->role_id ==2) {{route('logout-assist')}} @endif" class="btn btn-default btn-flat">Đăng xuất</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>