<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(!empty(Auth::user()->avatar))
                  <img src="{{ url('/') }}/upload/users/{!! Auth::user()->avatar !!}" class="img-circle" alt="User Image">
                @else
                  <img id="image" src="/backend/dist/img/no-logo.png"/>
                @endif
            </div>
            <div class="pull-left info">
                <p>{!! Auth::user()->u_fullname !!}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{!! route('dashboard') !!}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            <li >
                <a href="{!! route('users-index') !!}">
                    <i class="fa fa-user"></i>
                    <span>Quản lý users</span>
                </a>
            </li>

            <li >
                <a href="{!! route('project-index') !!}">
                    <i class="fa fa-pie-chart"></i>
                    <span>Quản lý đồ án</span>
                </a>
            </li>
            <li >
                <a href="{!! route('lecturers-index') !!}">
                    <i class="fa fa-laptop"></i>
                    <span>Quản lý giảng viên</span>
                </a>
            </li>
            <li >
                <a href="{!! route('ListProjects_index') !!}">
                    <span class="glyphicon glyphicon-th-list"></span>
                    <span>Danh sách đề tài</span>
                </a>
            </li>
            <li >
                <a href="{!! route('course-index') !!}">
                    <i class="fa fa-edit"></i> 
                    <span>Quản lý khóa</span>
                </a>
            </li>
            <li >
                <a href="{!! route('classroom-index') !!}">
                    <i class="fa fa-table"></i> 
                    <span>Quản lý lớp</span>
                </a>
            </li>
            <li >
                <a href="{!! route('position-index') !!}">
                    <i class="fa fa-table"></i> 
                    <span>Quản lý chức vụ</span>
                </a>
            </li>
            <li >
                <a href="{!! route('degree-index') !!}">
                    <i class="fa fa-table"></i> 
                    <span>Quản lý bằng cấp</span>
                </a>
            </li>
            <li >
                <a href="{!! route('students-index') !!}">
                    <span class="glyphicon glyphicon-user"></span>
                    <span>Quản lý sinh viên</span>
                </a>
            </li>
            <li>
                <a href="{!! route('listEmail') !!}">
                    <i class="fa fa-calendar"></i> 
                    <span>Quản lý email đăng ký</span>
                </a>
            </li>
            <li >
                <a href="{!! route('listSlide') !!}">
                    <span class="glyphicon glyphicon-film"></span>
                    <span>Quản lý slide</span>
                </a>
            </li>
            <li >
                <a href="{!! route('listTypicalStudent') !!}">
                    <span class="glyphicon glyphicon-user"></span>
                    <span>Hình ảnh sinh viên tiêu biểu</span>
                </a>
            </li>
            <li >
                <a href="{!! route('listMemorialPhoto') !!}">
                    <span class="glyphicon glyphicon-picture"></span>
                    <span>Quản lý hình ảnh kỷ niệm</span>
                </a>
            </li>
            <li >
                <a href="{!! route('news-index') !!}">
                    <span class="glyphicon glyphicon-book"></span>
                    <span>Quản lý tin tức</span>
                </a>
            </li>
            <li >
                <a href="{!! route('listContact') !!}">
                    <span class="glyphicon glyphicon-envelope"></span>
                    <span>Quản lý liên hệ</span>
                </a>
            </li>
            <li >
                <a href="{!! route('listBooks') !!}">
                    <span class="glyphicon glyphicon-book"></span>
                    <span>Quản lý sách</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>