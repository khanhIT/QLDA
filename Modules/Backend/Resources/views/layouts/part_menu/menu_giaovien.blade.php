<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(!empty(Auth::user()->avatar))
                  <img src="{{ url('/') }}/upload/users/{!! Auth::user()->avatar !!}" class="img-circle" alt="User Image">
                @else
                  <img src="{{ url('/') }}/images/user.jpg">
                @endif
            </div>
            <div class="pull-left info">
                <p>{!! Auth::user()->u_fullname !!}</p>
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>
        
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree" style="margin-top: 20px">
            <li class="header">MAIN NAVIGATION</li>
            <li >
                <a href="{!! route('student_index') !!}">
                    <span class="glyphicon glyphicon-user"></span>
                    <span>Quản lý sinh viên</span>
                </a>
            </li>
            <li >
                <a href="{!! route('ListProject-index') !!}">
                    <span class="glyphicon glyphicon-th-list"></span> 
                    <span>Danh sách đề tài</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>