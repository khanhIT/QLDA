<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="{{ asset('backend/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/assets/css/form-elements.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/assets/css/style.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="{{ asset('backend/assets/ico/favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('backend/assets/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('backend/assets/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('backend/assets/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('backend/assets/ico/apple-touch-icon-57-precomposed.png') }}">

</head>

<body>

<!-- Top content -->
<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Đăng nhập vào Admin</h3>
                            <p>Nhập tên đăng nhập và mật khẩu của bạn để đăng nhập</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    
                    <div class="form-bottom">
                        <form role="form" action="" method="post" class="login-form">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('u_name') ? 'has-error' : '' }}">
                                <label class="sr-only" for="form-username">Username</label>
                                <input type="text" name="u_name" placeholder="Username..." class="form-username form-control" value="{{ old('u_name') }}"
                                       @if( $errors->has('u_name')) autofocus
                                       @elseif ($errors->has('password'))
                                       @else autofocus
                                        @endif
                                >
                                <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('u_name') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="sr-only" for="form-password">Password</label>
                                <input type="password" name="password" placeholder="Password..." class="form-password form-control" value="{{ old('password') }}" @if( $errors->has('password')) autofocus @endif>
                                <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('password') }}</span>
                            </div>
                            <span class="text-danger">
                                @if (session('error'))
                                    {{ session('error') }}
                                @endif
                            </span>
                            <div class="checkbox">
                                <label><input type="checkbox" class="remember_token"> Remember me</label>
                            </div>
                            
                            <button type="submit" class="btn">Đăng nhập!</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-sm-6 col-sm-offset-3 social-login">
                    <h3>...hoặc đăng nhập với:</h3>
                    <div class="social-login-buttons">
                        <a class="btn btn-link-1 btn-link-1-facebook" href="#">
                            <i class="fa fa-facebook"></i> Facebook
                        </a>
                        <a class="btn btn-link-1 btn-link-1-twitter" href="#">
                            <i class="fa fa-twitter"></i> Twitter
                        </a>
                        <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                            <i class="fa fa-google-plus"></i> Google Plus
                        </a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>


<!-- Javascript -->
<script src="{{ asset('backend/assets/js/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('backend/assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/jquery.backstretch.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/scripts.js') }}"></script>

<!--[if lt IE 10]>
<script src="{{ asset('backend/assets/js/placeholder.js') }}"></script>
<![endif]-->

</body>

</html>