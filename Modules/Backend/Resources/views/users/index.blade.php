@extends('backend::layouts.master')

@section('title')
Quản lý Người Dùng
@stop
@section('styleSheet')
 <link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
            <div class="title_right">
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right">
                    <a href="{{route('users-create')}}">
                        <button class="btn btn-sm btn-success add"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
                    </a>
                </div>
            </div>
        </div>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="#" style="color: red">Users</a></li>
        </ul>
                   
        <div class="clearfix"></div>
        @if (session('info'))
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('info')}}
            </div>
        @endif
        <section class="content container" id="users">
            <div class="row">
                <div class="col-md-12"> 
                    <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th style="width: 20px">STT</th>
                                <th class="no-sorting">Họ và Tên</th>
                                <th class="no-sorting">Email</th>
                                <th class="no-sorting">Số điện thoại</th>
                                <th class="no-sorting">Skype</th>
                                <th class="no-sorting">Địa chỉ</th>
                                <th class="no-sorting">Quyền</th>
                                <th class="no-sorting">Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                              <?php $n=1;?>
                            @foreach($user_data as $data)
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{$data->u_fullname}}</td>
                                <td>{{$data->u_email}}</td>
                                <td>{{$data->mobile}}</td>
                                <td>{{$data->skype}}</td>
                                <td>{{$data->address}}</td>
                                @if($data->role_id == 1)
                                <td>
                                    <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="Admin">admin</button>
                                </td>
                                @else
                                <td> 
                                    <button class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="Giáo viên">giaovien</button>
                                </td>
                                @endif
                                <td>
                                    <a href="{{route('users-show',['id' =>$data->id])}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-eye-open"></span>Xem</a>
                                    <a href="{{route('users-edit',['id' =>$data->id])}}" class="btn btn-xs btn-warning"><span class="glyphicon glyphicon-pencil"></span> Sửa</a>
                                    <a href="#" class="btn btn-danger btn-xs  delete_data_user delete_{{ $data->id}}" data-action='{{url("admin/users/delete/$data->id")}}' data-id="{{ $data->id}}"  ><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                                </td>

                            </tr>
                           <?php $n++?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scriptAdd')
    <script type="text/javascript">
 $(document).ready(function(){

            // readProducts();  it will load products when document loads

            $(document).on('click', '.delete_data_user', function(e){
                delete_data_userAPI = $(this).data('action');

                swal({
                        title: "Bạn có chắc chắn muốn xóa tài khoản này？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,
                    
                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_userAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('admin/users')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });
        




    </script>
    @endsection


