@extends('backend::layouts.master')

@section('title')
  Quản lý Người Dùng
@stop
@section('styleSheet')
<link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->

	    <ul class="breadcrumb">
	        <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
	        <li><a href="{!! route('users-index') !!}">Users</a></li>
	         <li><a href="#" style="color: red">Thêm mới</a></li>
	    </ul>
                   
        <section class="content container" id="users">
            <div class="row">
                <div class="col-xs-12"> 
                        <div class="text-right col-md-11">
							<a href="{!! route('users-index') !!}">
								<button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
							</a>
						</div>
                   		<div class="col-md-11 add-user">
                   			<form action="" method="POST" enctype="multipart/form-data" name="myForm">
                   				  {{ csrf_field() }}
                   			<div class="col-md-6">
                   				<div class="form-group {{ $errors->has('u_fullname') ? 'has-error' : '' }}">
							    <label for="exampleInputEmail1">Họ và Tên</label>
							    <input type="text" class="form-control" name="u_fullname" placeholder="Nhập Họ và tên" value="{!! old('u_fullname') !!}" @if( $errors->has('u_fullname')) autofocus
                               @elseif ($errors->has('u_name')) @elseif($errors->has('u_email')) @elseif($errors->has('password')) @elseif($errors->has('mobile')) @elseif($errors->has('avatar')) @elseif($errors->has('birthday')) 
                               @elseif($errors->has('gender')) @elseif($errors->has('married'))
                               @elseif($errors->has('skype')) @elseif($errors->has('role_id'))
                               @elseif(session('error-mail')) @else autofocus
                                @endif>
							     <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('u_fullname') }}</span>
							  </div>
							  <div class="form-group {{ $errors->has('u_name') ? 'has-error' : '' }}">
							    <label for="exampleInputPassword1">Tên</label>
							    <input type="text" class="form-control" name="u_name" placeholder="Nhập tên" value="{!! old('u_name') !!}" @if( $errors->has('u_name')) autofocus @endif >
							      <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('u_name') }}</span>
							  </div>
							  <div class="form-group {{ $errors->has('u_email') ? 'has-error' : '' }}">
							    <label for="exampleInputPassword1">Email</label>
							    <input type="email" class="form-control" name="u_email"  placeholder="Nhập Email" value="{!! old('u_email') !!}" @if( $errors->has('u_email')) autofocus @endif>
							     <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('u_email') }}</span>
							  </div>
							  <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
							    <label for="exampleInputPassword1">Mật Khẩu</label>
							    <input type="password" class="form-control"  name="password" placeholder="Nhập Mật Khẩu" value="{!! old('password') !!}" @if( $errors->has('password')) autofocus @endif>
							    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('password') }}</span>
							  </div>
							   <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}" style="margin-top: 26px">
							    <label for="exampleInputPassword1">Số Điện Thoại</label>
							    <input type="number" class="form-control" name="mobile" maxlength="11"  placeholder="Nhập Số điện Thoại" value="{!! old('mobile') !!}" @if( $errors->has('mobile')) autofocus @endif>
							    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('mobile') }}</span>
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Ảnh Đại Diện</label>
							    <input type="file" onchange='openFile(event)' name="avatar" value="{!! old('avatar') !!}" class="form-control" >
                                <img id="output" src="<?php echo url('/')?>/images/web-04.jpg" style="margin-top: 15px; width: 150px; height: 150px" />
							  </div>
							 
                   			</div>
                   			<div class="col-md-6">
                   				<div class="form-group {{ $errors->has('birthday') ? 'has-error' : '' }}">
								    <label for="exampleInputPassword1">Ngày sinh</label>
								    <input type="date" class="form-control"  name="birthday"  placeholder="Nhập ngày sinh" value="{!! old('birthday') !!}" @if( $errors->has('birthday')) autofocus @endif>
								    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('birthday') }}</span>
							  	</div>
							  	<div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
							  		<label for="exampleInputPassword1">Giới Tính</label>
									 <select name="gender"  class="form-control" @if( $errors->has('gender')) autofocus @endif>
									 	<option value="">-- Chọn giới tính --</option>
									 	<option value="0">Nữ</option>
									 	<option value="1">Nam</option>
									 </select>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('gender') }}</span>
								</div>
								 <div class="form-group {{ $errors->has('married') ? 'has-error' : '' }}">
							  		<label for="exampleInputPassword1">Hôn nhân</label>
									 <select name="married"  class="form-control" @if( $errors->has('married')) autofocus @endif>
									 	<option value="">-- Chọn tình trạng hôn nhân --</option>
									 	<option value="0">Độc Thân</option>
									 	<option value="1">Đã Kết Hôn</option>
									 </select>
									 <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('married') }}</span>
								</div>
								<div class="form-group {{ $errors->has('married') ? 'has-error' : '' }}">
								    <label for="exampleInputPassword1">Địa Chỉ</label>
								    <input type="text" class="form-control" name="address"  placeholder="Nhập địa chỉ" @if( $errors->has('address')) autofocus @endif>
								     <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('address') }}</span>
							  	</div>
							  	<div class="form-group {{ $errors->has('skype') ? 'has-error' : '' }}">
								    <label for="exampleInputPassword1">Skype</label>
								    <input type="text" class="form-control" name="skype"  placeholder="Nhập Skype" value="{!! old('skype') !!}" @if( $errors->has('skype')) autofocus @endif>
								    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('skype') }}</span>
							  	</div>
							  	 <div class="form-group {{ $errors->has('role_id') ? 'has-error' : '' }}">
							  		<label for="exampleInputPassword1">Quyền</label>
									 <select name="role_id" id="inputGender" class="form-control" @if( $errors->has('role_id')) autofocus @endif>
									 	 <option value="">--Chọn quyền--</option>
                                            @foreach($role as $role_id)
                                                <option {{ $role_id->id == old('id') ? "selected" : "" }}  value="{{$role_id->id}}">
                                                {{$role_id->role_name}}</option>
                                            @endforeach
           							</select>
									  <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('role_id') }}</span>
								</div>
                   			</div> 
                   			<div class="user-action">
	                   			<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Thêm mới</button>
								<button type="reset" class="btn btn-danger"><span class="glyphicon glyphicon-refresh"></span> Nhập lại</button>
                   			</div>
                   		</div>
                   		</form>
                   		 <div class="col-md-1"></div>
                </div>
            </div>
        </section>
    </div>
@stop
