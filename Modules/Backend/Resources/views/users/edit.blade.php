@extends('backend::layouts.master')

@section('title')
  Quản lý Người Dùng
@stop
@section('styleSheet')
 <link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
 <style type="text/css">
   	
      .list{
         margin-left: 85px;
      }

   </style>
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{{route('users-index')}}">Users</a></li>
             <li><a href="#" style="color: red">Chỉnh sửa</a></li>
        </ul>
        <div class="page-title">
	        <div class="title_right">
	            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
	                <a href="{!! route('users-create') !!}">
	                    <button class="btn btn-sm btn-success"><i class="fa fa-plus" aria-hidden="true"></i>Thêm mới</button>
	                </a>
	            </div>
	            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
	                <a href="{!! route('users-index') !!}">
	                    <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
	                </a>
	            </div>
	        </div>
	    </div>		          
        <section class="content container" id="users">
            <div class="row">
                <div class="col-xs-11">
                        <div class="col-md-3"></div>
                   		<div class="col-md-12 add-user">
                   			<form action="" method="POST" enctype="multipart/form-data" name="myForm" >
                   				 {{ csrf_field() }} 	
                   			<div class="col-md-6">
                   				<div class="form-group {{ $errors->has('u_fullname') ? 'has-error' : '' }}">
								    <label for="exampleInputEmail1">Họ và Tên</label>
								    <input type="text" class="form-control" name="u_fullname" value="{{ old('u_fullname', $user_edit->u_fullname) }}" 
								       @if( $errors->has('u_fullname')) autofocus
		                               @elseif ($errors->has('u_name')) @elseif($errors->has('u_email')) @elseif($errors->has('password')) @elseif($errors->has('mobile')) @elseif($errors->has('birthday')) @elseif($errors->has('gender')) @elseif($errors->has('married')) @elseif($errors->has('skype')) @elseif($errors->has('role_id')) @elseif(session('error-mail')) 
		                               @else autofocus
		                               @endif
	                                >
	                                <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('u_fullname') }}</span>
							    </div>
							  <div class="form-group {{ $errors->has('u_name') ? 'has-error' : '' }}">
							    <label for="exampleInputPassword1">Tên</label>
							    <input type="text" class="form-control" name="u_name" value="{{ old('u_name', $user_edit->u_name) }}" @if( $errors->has('u_name')) autofocus @endif>
							    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('u_name') }}</span>
							  </div>
							  <div class="form-group  {{ $errors->has('u_email') ? 'has-error' : '' }}">
							    <label for="exampleInputPassword1">Email</label>
							    <input type="email" class="form-control" name="u_email"  value="{{ old('u_email', $user_edit->u_email) }}" @if( $errors->has('u_email')) autofocus @endif>
							    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('u_email') }}</span>
							  </div>
							  <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
							    <label for="exampleInputPassword1">Mật Khẩu</label>
							    <input type="password" class="form-control" min="6" name="password" value="{{ old('password', $user_edit->password) }}" @if( $errors->has('password')) autofocus @endif>
							    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('password') }}</span>
							  </div>
							   <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}" style="margin-top: 26px">
							    <label for="exampleInputPassword1">Số Điện Thoại</label>
							    <input type="number" class="form-control" name="mobile" maxlength="11"  value="{{ old('mobile', $user_edit->mobile) }}" @if( $errors->has('mobile')) autofocus @endif>
							    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('mobile') }}</span>
							  </div>
							  <div class="form-group">
							    <label for="exampleInputPassword1">Ảnh Đại Diện</label>
							    <input type="file" onchange='showFile(event)' name="avatar" class="form-control">
	                            @if(!empty($user_edit->avatar))
	                                <img id="image" class="" src="<?php echo url('/')?>/upload/users/{!! $user_edit->avatar !!}" style="margin-top: 15px"/>
	                            @else
					                <img id="image" src="/backend/dist/img/no-logo.png" style="margin-top: 15px"/>
			                    @endif	
							  </div>
							 
                   			</div>
                   			<div class="col-md-6">
                   				<div class="form-group {{ $errors->has('birthday') ? 'has-error' : '' }}">
								    <label for="exampleInputPassword1">Ngày sinh</label>
								    <input type="date" class="form-control" name="birthday" value="{{ old('birthday', $user_edit->birthday->format('d/m/Y')) }}" @if( $errors->has('birthday')) autofocus @endif>
								    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('birthday') }}</span>
							  	</div>
							  	<div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
							  		<label for="exampleInputPassword1">Giới Tính</label>
									 <select name="gender" class="form-control" @if( $errors->has('gender')) autofocus @endif>
                                       <option>--Chọn giới tính --</option>
									 	<option value="0" {{$user_edit->gender ==0 ? 'selected' :''}}>Nữ</option>
									 	<option value="1" {{$user_edit->gender ==1 ? 'selected' :''}}>Nam</option>
									 </select>
									 <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('gender') }}</span>
								</div>
								 <div class="form-group {{ $errors->has('married') ? 'has-error' : '' }}">
							  		<label for="exampleInputPassword1">Hôn nhân</label>
									 <select name="married" class="form-control" @if( $errors->has('married')) autofocus @endif>
									 	<option value="">--Chọn tình trạng hôn nhân--</option>
									 	<option value="0" {{$user_edit->married ==0 ? 'selected' :''}}>Độc Thân</option>
									 	<option value="1" {{$user_edit->married ==1 ? 'selected' :''}}>Đã Kết Hôn</option>
									 </select>
									 <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('married') }}</span>
								</div>
								<div class="form-group {{ $errors->has('married') ? 'has-error' : '' }}">
								    <label for="exampleInputPassword1">Địa Chỉ</label>
								    <input type="text" class="form-control" name="address" value="{{ old('address', $user_edit->address) }}" @if( $errors->has('address')) autofocus @endif>
								    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('address') }}</span>
							  	</div>
							  	<div class="form-group {{ $errors->has('skype') ? 'has-error' : '' }}">
								    <label for="exampleInputPassword1">Skype</label>
								    <input type="text" class="form-control" name="skype"  value="{{ old('skype', $user_edit->skype) }}" @if( $errors->has('skype')) autofocus @endif>
								    <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('skype') }}</span>
							  	</div>
							  	 <div class="form-group {{ $errors->has('role_id') ? 'has-error' : '' }}">
							  		<label for="exampleInputPassword1">Quyền</label>
									 <select name="role_id" class="form-control" @if( $errors->has('role_id')) autofocus @endif>
									 	 @foreach($role as $role_data)
                                            @if(!empty(trim($role_data['id'])))
                                                <option value="{{$role_data['id']}}" {{ ($role_data['id'] == old('vr_category', $user_edit->role_id)) ? "selected" : "" }}>{{$role_data['role_name']}}</option>
                                            @endif
                                        @endforeach
									 </select>
									 <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('role_id') }}</span>
								</div>
                   			</div> 

                   			<div class="user-action">
	                   			<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Cập nhật</button>
								  <button type="reset" class="btn btn-danger"><span class="glyphicon glyphicon-refresh"></span> Nhập lại</button>
                   			</div>
						</div>  
                   		
                   		</form>
                   		 <div class="col-md-1"></div>
                </div>
            </div>
        </section>
    </div>
@stop


