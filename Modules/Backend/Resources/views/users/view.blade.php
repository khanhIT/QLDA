@extends('backend::layouts.master')

@section('title')
   Quản lý Người Dùng
@stop
@section('styleSheet')
<link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
	    <ul class="breadcrumb">
	        <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
	        <li><a href="{{route('users-index')}}">Users</a></li>
	         <li><a href="#"  style="color: red">Xem chi tiết</a></li>
	    </ul>
                    
        <section class="content container" id="users">
            <div class="row">
                <div class="col-xs-12">                     
                      <div class="col-xs-6 col-md-4">
                      	<div class="card">
                      	 	<div class="avatar">
		                      @if($user_show->avatar != '')
		                      	<img src="{!! url('/') !!}/upload/users/{!! $user_show->avatar !!}" width="100%" height="100%">
		                      @else
				                <img id="image" src="/backend/dist/img/no-logo.png"/>
		                      @endif	

		                    <p class="users-detail">{{ $user_show->u_fullname }}</p>

			                <div class="col-xs-6 col-md-4 item-avatar">
			                    <p>Sinh nhật</p>
			                    <strong>{{ $user_show->birthday->format('d-m-Y') }}</strong>
			                </div>
			                <div class="col-xs-6 col-md-4 item-avatar">
			                    <p>Giới tính</p>
			                    <strong>{{ $user_show->gender ==1 ? 'Nam':'Nữ' }}</strong>
			                </div>
			                <div class="col-xs-6 col-md-4 item-avatar">
			                    <p>Nơi ở</p>
			                    <strong>{{ $user_show->address }}</strong>
			                </div>
		                    </div>
                      	</div>
                      </div>
                      <div class="col-xs-12 col-sm-6 col-md-8">
                      	<div class="card">
                      		<div class="card-body item-detail-01">
				                <div class="main_title">
				                    <h3 class="title-03 fleft">
				                        Thông tin cá nhân
				                    </h3>
				                </div>
				                <div class="main-item container-fluid">
                                    <div class="col-xs-6 ">
			                          <table class="table-detail-01">
					                       <tbody>
					                      		<tr>
					                      			<th>Họ và Tên</th>
					                      			<td>{{$user_show->u_fullname}}</td>
					                      		</tr>
					                      		<tr>
					                      			<th>Tên</th>
					                      			<td>{{$user_show->u_name}}</td>
					                      		</tr>
					                      		<tr>
					                      			<th>Email</th>
					                      			<td>{{$user_show->u_email}}</td>
					                      		</tr>
					                      		<tr>
					                      			<th>Số điện thoại</th>
					                      			<td>{{$user_show->mobile}}</td>
					                      		</tr>
					                      	</tbody>
					                      </table>
					                </div>
					                 <div class="col-xs-6 ">
			                          <table class="table-detail-01">
					                       <tbody>
					                      		<tr>
					                      			<th>Hôn Nhân</th>
					                      			<td>
					                      				{{ $user_show->married==0 ? 'Độc thân':'Đã kết hôn' }}
					                      			</td>
					                      		</tr>
					                      		<tr>
					                      			<th>Skype</th>
					                      			<td>{{$user_show->skype}}</td>
					                      		</tr>
					                      		<tr>
					                      			<th>Quyền</th>
					                      			<td>
					                      				@if($user_show->role_id==1)
					                      				  <button class="btn btn-xs btn-success">admin</button>
					                      				@else
					                      				  <button class="btn btn-xs btn-primary">giáo viên</button>
					                      				@endif
					                      			</td>
					                      		</tr>
					                       </tbody>
			                          </table>
					              </div>
					            </div>
			                </div>
	                    </div>
	                      <a href="{!! route('users-index') !!}" class="btn btn-success">
                              <span class="glyphicon glyphicon-arrow-left"></span> Trở lại
                           </a>
                      </div>
                </div>
            </div>
        </section>
    </div>
@stop
