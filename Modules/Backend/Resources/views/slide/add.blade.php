@extends('backend::layouts.master')
@section('title')
    Thêm mới slide
@stop
@section('styleSheet')
  <style type="text/css">
      form{
        margin-top: 80px;
      }
      .list{
        margin-top: 60px;
      }
  </style>
@stop
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="page-title">
        <div class="title_right">
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('listSlide') !!}">
                    <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                </a>
            </div>
        </div>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
      <li class="breadcrumb-item"><a href="{!! route('listSlide') !!}">Danh sách slide</a></li>
      <li class="breadcrumb-item active" style="color: red">Thêm mới</li>
    </ol>
    <!-- Main content -->
    <section class="content">
        <div class="container">
        	<div class="row">
	            <form action="" method="post" enctype="multipart/form-data" id="demo-form2"  class="form-horizontal form-label-left" novalidate="">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Tên </font>
                            </font>
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="name" class="form-control col-md-7 col-xs-12">
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('name') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Hình ảnh </font>
                            </font>
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" onchange='openFile(event)' name="image" class="form-control col-md-7 col-xs-12">
                            <img id="output" src="<?php echo url('/')?>/images/web-04.jpg" style="margin-top: 15px" width="565px" height="200px" />
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('image') }}</span>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button class="btn btn-primary" type="reset"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><span class="glyphicon glyphicon-refresh"></span> Nhập lại</font></font></button>
                            <button type="submit" class="btn btn-success"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</font></font></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="row">
            
        </div>
        

    </section>
    <!-- /.content -->
</div>
@stop
