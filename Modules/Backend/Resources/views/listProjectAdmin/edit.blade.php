@extends('backend::layouts.master')
@section('title')
   Chỉnh sửa tên đề tài
@stop
@section('styleSheet')
  <style type="text/css">
      form{
        margin-top: 80px;
      }
      .list, .add{
        margin-top: 60px;
      }
      .list{
        margin-left: 95px
      }
  </style>
@stop
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="page-title">
        <div class="title_right">
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('ListProjects_show') !!}">
                    <button class="btn btn-sm btn-info add"><i class="fa fa-plus" aria-hidden="true"></i>Thêm mới</button>
                </a>
            </div>
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('ListProjects_index') !!}">
                    <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                </a>
            </div>
        </div>
    </div>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
      <li class="breadcrumb-item"><a href="{!! route('ListProjects_index') !!}">Danh sách đề tài </a></li>
      <li class="breadcrumb-item active" style="color: red">Chỉnh sửa</li>
    </ol>
    <!-- Main content -->
    <section class="content">
        <div class="container">
        	<div class="row">
	            <form action="" method="post" enctype="multipart/form-data" id="demo-form2"  class="form-horizontal form-label-left" novalidate="">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Tên đồ án</font>
                            </font>
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="name" class="form-control col-md-7 col-xs-12" value="{{ $list->name }}" autofocus>
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('name') }}</span>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Mô tả</font>
                            </font>
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="description" class="form-control col-md-7 col-xs-12">{{ $list->description }}</textarea>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Tên giảng viên</font>
                            </font>
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="l_id" id="inputGender" class="form-control"  value="{!! old('l_id')!!}" @if( $errors->has('l_id')) autofocus @endif>
                                <option value="">--Chọn giảng viên--</option>
                                @foreach($lec as $le)
                                <option value="{!! $le->id !!}" {{ $le->id == old('l_id', $list->l_id) ? "selected" : "" }}>{!! $le->l_fullname !!}</option>   
                                @endforeach()
                            </select>
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_id') }}</span>
                    </div>

                    <div class="form-group {{ $errors->has('expried_at') ? 'has-error' : '' }}">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Ngày hết hạn</font>
                            </font>
                            <span class="required">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="date" name="expried_at" class="form-control col-md-7 col-xs-12" value="{!! $list->expried_at !!}">
                        </div>
                        <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('expried_at') }}</span>
                    </div> 

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                           
                            <button type="submit" class="btn btn-success"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><span class="glyphicon glyphicon-ok"></span> Cập nhập</font></font></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="row">
            
        </div>
        

    </section>
    <!-- /.content -->
</div>
@stop

@section('scriptAdd')
    <script>
        var openFile = function(event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('output');
                output.src = dataURL;
            };
            reader.onload = function (e) {
                $('#output')
                    .attr('src', e.target.result)
                    .width(565)
                    .height(200);
            };
            reader.readAsDataURL(input.files[0]);
        };
    </script>
@stop
