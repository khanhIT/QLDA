@extends('backend::layouts.master')

@section('title')
  Danh sách đề tài
@stop
@section('styleSheet')
 <link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
	        <div class="title_right">
	            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right">
	                <a href="{!! route('ListProjects_show') !!}">
	                    <button class="btn btn-sm btn-success add"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
	                </a>
	            </div>
	        </div>
	    </div>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{!! route('ListProjects_index') !!}" style="color: red">Danh sách đồ án</a></li>
        </ul>
        <div class="clearfix"></div>
	    @if (session('info'))
	        <div class="alert alert-info">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            {{session('info')}}
	        </div>
	    @endif            
        <section class="content container" id="users">
            <div class="row">
                <div class="col-md-12">                    
                    <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th>STT</th>
                                <th class="no-sorting">Tên đồ án</th>
                                <th class="no-sorting">Tên giảng viên</th>
                                <th class="no-sorting">Mô tả</th>
                                <th class="no-sorting">Hành động</th>
                            </tr>
                        </thead>
                        <?php $stt = 1; ?>
                        <tbody>
                        	@foreach($list as $list)
                            <tr>
                                <td>{!! $stt !!}</td>
                                <td>{!! $list->name !!}</td>
                                <td>{!! $list->l_fullname !!}</td>
                                <td>{!! $list->description !!}</td>
                                <td>

                                    @if(strtotime(date('d-m-Y')) <= strtotime(date_format(new DateTime($list->expried_at),'d-m-Y')))
                                        <a href="{!! route('ListProjects_edit',['id' => $list->id]) !!}">
                                            <button type="button" id="delete" class="btn btn-xs btn-info">
                                                <i class="fa fa-edit" aria-hidden="true"></i> sửa
                                            </button>
                                        </a>
                                        <a href="#" class="btn btn-danger btn-xs  delete_data_list delete_{{ $list->id}}" data-action='{{url("admin/danh-sach-de-tai/delete/$list->id")}}' data-id="{{ $list->id}}"  ><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                                    @endif
                                </td>
                            </tr>
                            <?php $stt++; ?>
                            @endforeach()
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scriptAdd')
    <script type="text/javascript">

        //xóa 
        $(document).ready(function(){

            $(document).on('click', '.delete_data_list', function(e){
                delete_data_userAPI = $(this).data('action');

                swal({
                        title: "Bạn có chắc chắn muốn xóa đề tài này？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,
                    
                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_userAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('admin/danh-sach-de-tai')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });

    </script>
@endsection
