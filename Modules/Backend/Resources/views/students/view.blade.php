@extends('backend::layouts.master')

@section('title')
   Quản lý sinh viên
@stop
@section('styleSheet')
<link rel="stylesheet" href="{{asset('css/backend/custormer/style.css')}}">
<link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
<style type="text/css">
	.table-detail-01 tr th:first-child{
		width: 50%
	}
	.table-detail-01{
		width: 100%;
	}
	.custormer-detail{
        color: #448AFF
	}
	.title-03{
		font-family: pro-bold;
	    font-size: 18px;
	    text-transform: uppercase;
	    font-weight: 700;
	}
</style>
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{{route('students-index')}}">Sinh viên</a></li>
             <li><a href="#"  style="color: red">Xem chi tiết</a></li>
        </ul>
        <section class="content container" id="custormer">
            <div class="row">
                <div class="col-xs-12">                     
                      <div class="col-md-11">
                      	<div class="card">
                      		<div class="card-body item-detail-01">
	                      		<div class="main_title">
				                    <h3 class="title-03 fleft">
				                        Thông tin đăng ký đồ án sinh viên
				                    </h3>
				                </div>
	                            <div class="card-body">
			                      <table class="table-detail-01">
			                      	<tbody>
			                      		<tr class="custormer-detail">
			                      			<th>Mã sinh viên</th>
			                      			<td>{{$custormer->msv}}</td>
			                      		</tr>
			                      		<tr class="custormer-detail">
			                      			<th>Họ và tên</th>
			                      			<td>{{$custormer->fullname}}</td>
			                      		</tr>
			                      		<tr>
			                      			<th>Email</th>
			                      			<td>{{$custormer->email}}</td>
			                      		</tr>
			                      		<tr>
			                      			<th>Số điện thoại</th>
			                      			<td>{{$custormer->phone}}</td>
			                      		</tr>
			                      		<tr>
			                      			<th>Ngày sinh</th>
			                      			<td> 
			                      				{{ !empty($custormer->birthday) ? $custormer->birthday : '' }}</td>
			                      		</tr>
			                      		<tr>
			                      			<th>Địa chỉ</th>
			                      			<td>
												{{$custormer->address}}
											</td>
			                      		</tr>
			                      		<tr class="custormer-detail">
			                      			<th>Tên đồ án</th>
			                      			
											@if(strlen($custormer->project_name) > 2)
		                                        <td>
													{{$custormer->project_name}}
												</td>
											@else()
												<td>
													{{$custormer->name}}
												</td>
											@endif()
			                      		</tr>
			                      		<tr>
			                      			<th>Lớp chuyên ngành</th>
			                      			<td>{{$custormer->c_name}}</td>
			                      		</tr>
			                      		<tr>
			                      			<th>Khóa học</th>
			                      			<td> 
			                      				K{{$custormer->s_key}}
											</td>
			                      		</tr>
										<tr class="custormer-detail">
											<th>Giảng viên hướng dẫn</th>
											<td>
												{{$custormer->l_fullname}}
											</td>
										</tr>
			                      		<tr class="custormer-detail">
			                      			<th>Trạng thái</th>
			                      			
			                      			@if($custormer->status == 1)
			                      			<td>
			                      				<button type="button" class="btn btn-outline-success">Đã đăng ký</button>
			                      			</td>
			                      			@else()
			                      			<td>
			                      				<button type="button" class="btn btn-outline-secondary">Chưa đăng ký</button>
			                      			</td>
			                      			@endif()
			                      		</tr>
			                      	</tbody>
			                      </table>
				                </div>
				            </div>
				        </div>
						  <a href="{{route('students-index')}}" class="btn btn-sm btn-success">
						  	<span class="glyphicon glyphicon-arrow-left"></span>Trở lại</a>
                      </div>
                </div>
            </div>
        </section>
    </div>
@stop
