@extends('backend::layouts.master')

@section('title')
Quản lý sinh viên
@stop
@section('styleSheet')
 <link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="#" style="color: red">Sinh viên</a></li>
        </ul>
        @if (session('info'))
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('info')}}
            </div>
        @endif            

        <div style="float: right; margin-right: 24px">
            <a href="{{ route('student-export') }}" class="btn btn-sm" style="background: #77b315; color: #fff" data-toggle="tooltip" data-placement="top" title="Export file excel"> 
                <span class="glyphicon glyphicon-arrow-down"></span> Export
            </a>
        </div>

        <div style="float: right; margin-right: 24px">
            <button type="button" class="btn btn-sm" style="background: #77b315; color: #fff" data-toggle="modal" data-target="#exampleModal">
                <span class="glyphicon glyphicon-arrow-up"></span> Import file excel
            </button>
        </div>

        <section class="content container" id="users">
            <div class="row">
                <div class="col-md-12">                    
                    <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th>STT</th>
                                <th class="no-sorting">MSSV</th>
                                <th class="no-sorting">Họ và Tên</th>
                                <th class="no-sorting">SĐT</th>
                                <th class="no-sorting">Ngày sinh</th>
                                <th class="no-sorting">Địa chỉ</th>
                                <th class="no-sorting">Tên đồ án</th>
                                <th class="no-sorting">Lớp Chuyên ngành</th>
                                <th class="no-sorting">Khóa học</th>
                                <th class="no-sorting">GVHD</th>
                                <th class="no-sorting">Hành động</th>
                                
                            </tr>
                        </thead>
                        <?php $stt = 1; ?>
                        <tbody>
                            @foreach($custormer as $cus)
                            <tr>
                                <td>{!! $stt !!}</td>
                                <td>{!! $cus->msv !!}</td>
                                <td>{!! $cus->fullname !!}</td>
                                <td>{!! $cus->phone !!}</td>
                                <td>{!! $cus->birthday !!}</td>
                                <td>{!! $cus->address !!}</td>
                                @if(strlen($cus->project_name) > 2)
                                   <td>{!! $cus->project_name !!}</td>
                                @else
                                   <td>{!! $cus->name !!}</td>
                                @endif
                                <td>{!! $cus->c_name !!}</td>
                                <td>K{!! $cus->s_key !!}</td>
                                <td>{!! $cus->l_fullname !!}</td>
                                <td>
                                    <a href="{!! route('students-show',['id' => $cus->id]) !!}">
                                        <button type="button" id="delete" class="btn btn-xs btn-success">
                                            <i class="fa fa-eye" aria-hidden="true"></i> xem
                                        </button>
                                    </a>
                                   <a href="{!! route('students-edit',['id' => $cus->id]) !!}">
                                        <button type="button" id="delete" class="btn btn-xs btn-warning">
                                            <i class="fa fa-edit" aria-hidden="true"></i> sửa
                                        </button>
                                    </a>
                                </td>
                            </tr>
                            <?php $stt++; ?>
                            @endforeach()
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

    <!-- modal import file excel -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel">Import danh sách sinh viên từ file excel</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="cart">
                <form method="post" action="{!! route('importStudent') !!}" id="form-register" name="registerForm" novalidate class="form-validate form-edit" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="cart-body">
                        <div class="tab-content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="mda-form-group mb">
                                        <div class="mda-form-control">
                                             <input type="file" name="import">
                                            <div class="mda-form-control-line"></div>
                                            <label>chọn file import</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                      </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
        
@stop

@section('scriptAdd')
 
@stop
