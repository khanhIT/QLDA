@extends('backend::layouts.master')

@section('title')
    Quản lý khóa học
@stop
@section('styleSheet')
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{!! route('course-index') !!}">Khóa học</a></li>
             <li><a href="#" style="color: red">Thêm mới</a></li>
        </ul>
                   
        <section class="content container" id="course">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-right col-md-11">
                        <a href="{!! route('course-index') !!}">
                            <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                        </a>
                    </div>
                        <div class="col-md-3"></div>
                   		<div class="col-md-6 add-user">
                   			<form action="" method="POST" >
                                {{ csrf_field() }}
                   				<div class="form-group {{ $errors->has('s_key') ? 'has-error' : '' }}">
            							    <label for="exampleInputEmail1">Khóa học</label>
            							    <input type="number" class="form-control" name="s_key" placeholder="Nhập Khóa Học" autofocus value="{!! old('s_key') !!}" >
                              <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('s_key') }}</span>
							            </div>

                          <div class="form-group {{ $errors->has('year_start') ? 'has-error' : '' }}">
                              <label for="exampleInputEmail1">Năm bắt đầu</label>
                              <input type="date" class="form-control" name="year_start" autofocus value="{!! old('year_start') !!}" >
                              <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('year_start') }}</span>
                          </div>

                          <div class="form-group {{ $errors->has('year_end') ? 'has-error' : '' }}">
                              <label for="exampleInputEmail1">Năm kết thúc</label>
                              <input type="date" class="form-control" name="year_end" autofocus value="{!! old('year_end') !!}" >
                              <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('year_end') }}</span>
                          </div>

                          <div class="form-group {{ $errors->has('total_student') ? 'has-error' : '' }}">
                              <label for="exampleInputEmail1">Tổng sinh viên</label>
                              <input type="number" class="form-control" name="total_student" placeholder="Tổng sinh viên" autofocus value="{!! old('total_student') !!}" >
                              <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('total_student') }}</span>
                          </div>
                   			<div class="user-action">
                          <button type="reset" class="btn btn-primary">
                            <span class="glyphicon glyphicon-refresh"></span> Nhập lại</button>
	                   			<button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-plus"></span> Thêm mới</button>
                   			</div>
							  
                   		</div>
                   		</form>
                   		 <div class="col-md-3"></div>
                </div>
            </div>
        </section>
    </div>
@stop
