@extends('backend::layouts.master')

@section('title')
Quản lý Khóa học
@stop
@section('styleSheet')
  <style type="text/css">
       
       .add{
  
           margin-top: 60px;

       }
       div.dataTables_wrapper div.dataTables_filter{
           text-align: left;
           margin-left: -575px;
       }
       div.dataTables_wrapper div.dataTables_paginate{
           margin: 0;
           white-space: nowrap;
           text-align: right;
           margin-right: 64px;
       }
  </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
            <div class="title_right">
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right">
                    <a href="{{route('course-create')}}">
                        <button class="btn btn-sm btn-success add"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
                    </a>
                </div>
            </div>
        </div>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="#" style="color:red">Khóa Học</a></li>
        </ul>
                    
        <div class="clearfix"></div>
        @if (session('info'))
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{session('info')}}
            </div>
        @endif
        <section class="content container" id="course">
            <div class="row">
                <div class="col-xs-12">
                        	 <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th width="30px;">STT</th>
                                <th class="no-sorting">Khóa Học</th>
                                <th class="no-sorting">Năm bắt đầu</th>
                                <th class="no-sorting">Năm kết thúc</th>
                                <th class="no-sorting">Tổng sinh viên</th>
                                <th class="no-sorting">Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $n=1?>
                        @foreach($list_course as $list)
                            <tr>
                                <td>{{$n}}</td>
                                <td>DCCTTD{{$list->s_key}}</td>
                                <td>{{$list->year_start}}</td>
                                <td>{{$list->year_end}}</td>
                                <td>{{$list->total_student}} sv</td>
                                <td>
                                    <a href="{{route('course-edit',['id'=>$list->id])}}" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-pencil"></span> sửa</a>
                                    <a href="#" class="btn btn-danger btn-xs  delete_data_course delete_{{ $list->id}}" data-action='{{url("admin/course/delete/$list->id")}}' data-id="{{ $list->id}}"  ><span class="glyphicon glyphicon-trash"></span> xóa</a>
                                </td>
                            </tr>
                            <?php $n++?>
                            @endforeach
                        </tbody>
                    </table>
                       
                   
                </div>
            </div>
        </section>
    </div>
@section('scriptAdd')
    <script type="text/javascript">
        $(document).ready(function(){

            // readProducts();  it will load products when document loads

            $(document).on('click', '.delete_data_course', function(e){
                delete_data_courseAPI = $(this).data('action');

                swal({
                        title: "Bạn có chắc chắn muốn xóa？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,

                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_courseAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('admin/course')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });



    </script>
@endsection
@stop
