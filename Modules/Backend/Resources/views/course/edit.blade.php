@extends('backend::layouts.master')

@section('title')
    Quản lý Khóa học
@stop
@section('styleSheet')
   <style type="text/css">
      .list{
        margin-left: 85px
      }
   </style>
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{!! route('course-index') !!}">Khóa học</a></li>
             <li><a href="#" style="color: red">Chỉnh sửa</a></li>
        </ul>

        <div class="page-title">
            <div class="title_right">
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <a href="{!! route('course-create') !!}">
                        <button class="btn btn-sm btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
                    </a>
                </div>
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <a href="{!! route('course-index') !!}">
                        <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                    </a>
                </div>
            </div>
        </div>
                    
        <section class="content container" id="course">
            <div class="row">
                <div class="col-xs-12">                    
                        <div class="col-md-3"></div>
                   		<div class="col-md-6 add-user">
                   			<form action="" method="POST" >
                                {{ csrf_field() }}
                   				<div class="form-group {{ $errors->has('s_key') ? 'has-error' : '' }}">
                            <label for="exampleInputEmail1">Khóa học</label>
                            <input type="text" class="form-control" name="s_key" value="{{ old('s_key', $edit_course->s_key) }}">
                             <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('s_key') }}</span>
                          </div>

                          <div class="form-group {{ $errors->has('year_start') ? 'has-error' : '' }}">
                            <label for="exampleInputEmail1">Năm bắt đầu</label>
                            <input type="date" class="form-control" name="year_start" value="{{ old('year_start', $edit_course->year_start) }}">
                             <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('year_start') }}</span>
                          </div>
                          <div class="form-group {{ $errors->has('year_end') ? 'has-error' : '' }}">
                            <label for="exampleInputEmail1">Năm kết thúc</label>
                            <input type="date" class="form-control" name="year_end" value="{{ old('year_end', $edit_course->year_end) }}">
                             <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('year_end') }}</span>
                          </div>

                          <div class="form-group {{ $errors->has('total_student') ? 'has-error' : '' }}">
                            <label for="exampleInputEmail1">Tổng sinh viên</label>
                            <input type="text" class="form-control" name="total_student" value="{{ old('total_student', $edit_course->total_student) }}">
                             <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('total_student') }}</span>
                          </div>
                   			<div class="user-action">
	                   			<button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok"></span> Cập nhật</button>
                   			</div>

                   		</form>
                   		 <div class="col-md-3"></div>
                </div>
            </div>
        </section>
    </div>
@stop
