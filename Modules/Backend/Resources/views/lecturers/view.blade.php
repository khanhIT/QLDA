@extends('backend::layouts.master')

@section('title')
   Quản lý Giảng Viên
@stop
@section('styleSheet')
<link rel="stylesheet" href="{{asset('css/backend/lecturers/style.css')}}">
<link rel="stylesheet" href="{{asset('css/backend/users/style.css')}}">
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
	    <ul class="breadcrumb">
	        <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
	        <li><a href="{{route('lecturers-index')}}">Giảng viên</a></li>
	         <li><a href="#"  style="color: red">Xem chi tiết</a></li>
	    </ul>
                   
        <section class="content container" id="users">
            <div class="row">
                <div class="col-xs-12">                     
                	<div class="col-xs-6 col-md-4">
                		<div class="card">
                      	 	<div class="avatar" style="min-height: 420px;">
								@if($show_lecturers->image != '')
									<img src="{!! url('/') !!}/upload/lecturers/{!! $show_lecturers->image !!}" width="100%" height="100%">
								@else
					                @if(intval($show_lecturers->gender) == 1)
					                <img src="/backend/dist/img/avatar5.png"/>
					                @else
					                <img src="/backend/dist/img/avatar3.png"/>
					                @endif
								@endif

								<p class="users-detail">{{ $show_lecturers->l_fullname }}</p>

				                <div class="col-xs-6 col-md-4 item-avatar">
				                    <p>Sinh nhật</p>
				                    <strong>{{ $show_lecturers->l_birthday }}</strong>
				                </div>
				                <div class="col-xs-6 col-md-4 item-avatar">
				                    <p>Giới tính</p>
				                    <strong>{{ $show_lecturers->gender ==1 ? 'Nam':'Nữ' }}</strong>
				                </div>
				                <div class="col-xs-6 col-md-4 item-avatar">
				                    <p>SĐT</p>
				                    <strong>{{ $show_lecturers->l_phone }}</strong>
				                </div>
				                <div class="col-xs-12">
				                	<p class="lecturers-website">
				                      <i class="ion-android-globe"></i> {{ $show_lecturers->link_url }}
				                    </p>
				                </div>

							</div>
						</div>
					</div>
                      <div class="col-xs-12 col-sm-6 col-md-8">
                      	<div class="card">
                      		<div class="card-body item-detail-01">
				                <div class="main_title">
				                    <h3 class="title-03 fleft">
				                        Thông tin giảng viên
				                    </h3>
				                </div>
				                <div class="main-item container-fluid">
				                	<div class="">
				                      <table class="table-detail-01">
				                      	<tbody>
				                      		<tr>
				                      			<th>Họ và Tên</th>
				                      			<td>{{$show_lecturers->l_fullname}}</td>
				                      		</tr>
				                      		<tr>
				                      			<th>Tên</th>
				                      			<td>{{$show_lecturers->l_username}}</td>
				                      		</tr>
				                      		<tr>
				                      			<th>Chức vụ</th>
				                      			<td> 
				                      				{{$show_lecturers->p_name}}
												</td>
				                      		</tr>
											<tr>
												<th>Bằng cấp</th>
												<td>
													{{$show_lecturers->d_name}}
												</td>
											</tr>
				                      		<tr>
				                      			<th>Email</th>
				                      			<td>{{$show_lecturers->l_email}}</td>
				                      		</tr>
				                      		<tr>
				                      			<th>Hôn nhân</th>
				                      			<td>
													@if($show_lecturers->marride==1)
														Đã kết hôn
													@else
														Độc thân
													@endif
												</td>
				                      		</tr>
				                      		<tr>
				                      			<th>Địa chỉ</th>
				                      			<td>{!! $show_lecturers->l_address !!}</td>
				                      		</tr>
				                      		
											<tr>
												<th>Thông tin cơ bản</th>
												<td style="word-wrap: break-word">
													{!! $show_lecturers->info !!}
												</td>
											</tr>
				                      		
				                      	</tbody>
				                      </table>
				                    </div>
			                    </div>
			                </div>
			            </div>
						  <a href="{{route('lecturers-index')}}" class="btn btn-sm btn-success">
						  	<span class="glyphicon glyphicon-arrow-left"></span> Trở lại</a>
                      </div>

                </div>
            </div>
        </section>
    </div>
@stop
