@extends('backend::layouts.master')

@section('title')
Quản lý Giảng viên
@stop
@section('styleSheet')
   <style type="text/css">
       
       .add{

           margin-top: 60px

       }

       .title_lec{

          color: red

       }
       div.dataTables_wrapper div.dataTables_filter{
           text-align: left;
           margin-left: -575px;
       }
       div.dataTables_wrapper div.dataTables_paginate{
           margin: 0;
           white-space: nowrap;
           text-align: right;
           margin-right: 64px;
       }
       .img-responsive{
            height: auto;
       }
       .img-responsive img{
           width: 100%;
       }

   </style>
   <link rel="stylesheet" href="{{asset('css/backend/lecturers/style.css')}}">
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="page-title">
            <div class="title_right">
                <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right">
                    <a href="{{route('lecturers-create')}}">
                        <button class="btn btn-sm btn-success add"><i class="fa fa-plus" aria-hidden="true"></i> Thêm mới</button>
                    </a>
                </div>
            </div>
        </div>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="#" class="title_lec" style="color: red">Giảng Viên</a></li>
        </ul>
                    
        <section class="content container" id="users">
            <div class="row">
                <div class="col-md-12">
                    <div class="clearfix"></div>
                    @if (session('info'))
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{session('info')}}
                        </div>
                    @endif
                    <table class="table table-hover" id="data-table2">
                        <thead>
                            <tr style="background: #77b315 none repeat scroll 0 0; color: #fff;height: 45px;">
                                <th width="30px">STT</th>
                                <th class="no-sorting">Họ và Tên</th>
                                <th class="no-sorting" width="6%;">Avatar</th>
                                <th class="no-sorting">Email</th>
                                <th class="no-sorting col-md-1">SĐT</th>
                                <th class="no-sorting col-md-1">Chức vụ</th>
                                <th class="no-sorting col-md-1">Bằng cấp</th>
                                <th class="no-sorting">Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $n=1;?>
                        @foreach($list_lecturers as $lecturer)
                            <tr>
                                <td>{{$n}}</td>
                                <td>{{$lecturer->l_fullname}}</td>
                                <td class="img-responsive">
                                    @if(!empty($lecturer->image))
                                    <img src="<?php echo url('/')?>/upload/lecturers/{{$lecturer->image}}" alt="{{$lecturer->l_fullname}}" title="{{$lecturer->l_fullname}}" />
                                    @else
                                       @if(intval($lecturer->gender) == 1)
                                        <img src="/backend/dist/img/avatar5.png" alt="{{$lecturer->l_fullname}}" title="{{$lecturer->l_fullname}}" />
                                        @else
                                        <img src="/backend/dist/img/avatar3.png" alt="{{$lecturer->l_fullname}}" title="{{$lecturer->l_fullname}}" />
                                        @endif
                                    @endif
                                </td>
                                <td>{{$lecturer->l_email}}</td>
                                <td>{{$lecturer->l_phone}}</td>
                                <td>
                                    @if($lecturer->p_id==1)
                                        Trưởng bộ môn
                                    @elseif($lecturer->p_id==2)
                                        Phó trưởng bộ môn
                                    @elseif($lecturer->p_id==3)
                                        Giảng Viên
                                    @else 
                                        Trợ giảng   
                                    @endif
                                </td>
                                <td>
                                    @if($lecturer->d_id==1)
                                        NSƯT.PGS.TS
                                    @elseif($lecturer->d_id==2)
                                        GV
                                    @elseif($lecturer->d_id==3)
                                        GV.TS
                                    @elseif($lecturer->d_id==4)
                                        Tiến Sĩ
                                    @elseif($lecturer->d_id==5)
                                        Thạc Sĩ
                                    @else
                                        Kỹ Sư
                                    @endif

                                </td>
                                <td>
                                    <a href="{{route('lecturers-show',['id'=>$lecturer->id])}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-eye-open"></span> Xem</a>
                                    <a href="{{route('lecturers-edit',['id'=>$lecturer->id])}}" class="btn btn-xs btn-warning"><span class="glyphicon glyphicon-pencil"></span> Sửa</a>
                                    <a href="#" class="btn btn-danger btn-xs  delete_data_lecturers delete_{{ $lecturer->id}}" data-action='{{url("admin/giang-vien/delete/$lecturer->id")}}' data-id="{{ $lecturer->id}}"  ><span class="glyphicon glyphicon-trash"></span> Xóa</a>
                                </td>
                            </tr>
                            <?php $n++?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
@section('scriptAdd')
    <script type="text/javascript">
        $(document).ready(function(){

            // readProducts();  it will load products when document loads

            $(document).on('click', '.delete_data_lecturers', function(e){
                delete_data_lecturersAPI = $(this).data('action');

                swal({
                        title: "Bạn có chắc chắn muốn xóa giảng viên này？",
                        text: "Nếu bạn xóa sẽ không khôi phục lại được",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Xóa",
                        closeOnConfirm: false,

                    },
                    function(){


                        $.ajax({
                            type: "get",
                            url: delete_data_lecturersAPI,
                            success: function (data) {
                                if (data == "true") {
                                    swal("Oke", "", "success");
                                    setTimeout(function(){
                                        swal.close();

                                        window.location.href = "{{url('admin/giang-vien')}}";
                                    }, 2000);

                                }


                            },
                            error: function (error) {

                            }
                        });

                    });

            });
        });



    </script>
@endsection
@stop
