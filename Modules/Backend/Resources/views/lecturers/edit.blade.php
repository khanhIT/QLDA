@extends('backend::layouts.master')

@section('title')
Quản lý Giảng Viên
@stop
@section('styleSheet')
   <style type="text/css">
   	
      .list{
         margin-left: 85px;
      }

   </style>
@endsection
@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<ul class="breadcrumb">
		<li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
		<li><a href="{!! route('lecturers-index') !!}">Giảng Viên</a></li>
		<li><a href="#" style="color: red">Thêm mới</a></li>
	</ul>
	<div class="page-title">
        <div class="title_right">
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('lecturers-create') !!}">
                    <button class="btn btn-sm btn-success"><i class="fa fa-plus" aria-hidden="true"></i>Thêm mới</button>
                </a>
            </div>
            <div class="col-md-2 col-sm-5 col-xs-12 form-group pull-right top_search">
                <a href="{!! route('lecturers-index') !!}">
                    <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                </a>
            </div>
        </div>
    </div>			
	<section class="content container" id="lecturers">
		<div class="row">
			<div class="col-xs-12">    
				<div class="col-md-3"></div>
				<div class="col-md-11 add-user">
					<form action="" method="POST" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="col-md-6">
							<div class="form-group {{ $errors->has('l_fullname') ? 'has-error' : '' }}" >
								<label for="exampleInputEmail1">Họ và Tên</label>
								<input type="text" class="form-control" name="l_fullname"  value="{{ old('l_fullname', $edit_lecturers->l_fullname) }}">
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_fullname') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('l_username') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Tên</label>
								<input type="text" class="form-control" name="l_username" value="{{ old('l_username', $edit_lecturers->l_username) }}">
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_username') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('l_email') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Email</label>
								<input type="email" class="form-control" name="l_email" value="{{ old('l_email', $edit_lecturers->l_email) }}">
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_email') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('l_phone') ? 'has-error' : '' }}" style="margin-top: 26px">
								<label for="exampleInputPassword1">Số Điện Thoại</label>
								<input type="number" class="form-control" name="l_phone" maxlength="11" value="{{ old('l_phone', $edit_lecturers->l_phone) }}" >
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_phone') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('l_birthday') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Ngày sinh</label>
								<input type="date" maxlength="6" class="form-control" name="l_birthday" value="{{ old('l_birthday', $edit_lecturers->l_birthday) }}">
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_birthday') }}</span>
							</div>

							<div class="form-group  {{ $errors->has('marride') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Hôn nhân</label>
								<select name="marride" id="inputGender" class="form-control"  >
									<option value="">--Chọn Married--</option>
									<option value="0" {{$edit_lecturers->marride ==0 ? 'selected' :''}}>Độc Thân</option>
									<option value="1" {{$edit_lecturers->marride ==1 ? 'selected' :''}}>Đã Kết Hôn</option>
								</select>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('marride') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('info') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Thông tin cơ bản</label>
								<textarea name="info" id="inputInfo" class="form-control" rows="3" > {{old('info', $edit_lecturers->info)}}</textarea>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('info') }}</span>
							</div>

							<div class="form-group  {{ $errors->has('link_url') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Link URL</label>
								<input type="url" name="link_url" class="form-control" rows="3" placeholder="Nhập link liên kết" value="{{ old('link_url', $edit_lecturers->link_url) }}>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('link_url') }}</span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Ảnh </label>
								<input type="file" name="image" onchange='showFile(event)' class="form-control"  value="{!! old('image')!!}">
								@if(!empty($edit_lecturers->image))
								<img id="image" class="" src="<?php echo url('/')?>/upload/lecturers/{!! $edit_lecturers->image !!}" width="200" height="200" style="margin-top: 15px"/>
								@else
								   @if(intval($edit_lecturers->gender) == 1)
					                <img src="/backend/dist/img/avatar5.png" width="200" height="200" style="margin-top: 15px"/>
					                @else
					                <img src="/backend/dist/img/avatar3.png" width="200" height="200" style="margin-top: 15px"/>
					                @endif
								@endif
							</div>
							<div class="form-group  {{ $errors->has('gender') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Giới Tính</label>
								<select name="gender" id="inputGender"   class="form-control" >
									<option value="" >--Chọn Gender--</option>
									<option value="0" {{$edit_lecturers->gender ==0 ? 'selected' :''}}>Nữ</option>
									<option value="1" {{$edit_lecturers->gender ==1 ? 'selected' :''}}>Nam</option>
								</select>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('gender') }}</span>
							</div>
							<div class="form-group  {{ $errors->has('l_address') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Địa Chỉ</label>
								<textarea type="text" class="form-control" name="l_address" placeholder="Nhập địa chỉ" @if( $errors->has('l_address')) autofocus @endif >{!! old('l_address',$edit_lecturers->l_address)!!}</textarea>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_address') }}</span>
							</div>

							<div class="form-group  {{ $errors->has('d_id') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Bằng Cấp</label>
								<select name="d_id" id="inputGender" class="form-control">
									<option value="">--Chọn Bằng cấp--</option>

									@foreach($degree as $de)
									<option value="{!! $de->id !!}" {!! $de->id == old('id',$edit_lecturers->d_id)  ? "selected" : "" !!}>{!! $de->name !!}</option>
									@endforeach()
									
								</select>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('d_id') }}</span>
							</div>

							<div class="form-group  {{ $errors->has('p_id') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Chức vụ</label>
								<select name="p_id" id="inputGender" class="form-control"   @if( $errors->has('p_id')) autofocus @endif>
									<option value="">--Chọn chức vụ--</option>

									@foreach($position as $po)
									<option value="{!! $po->id !!}" {!! $po->id == old('id',$edit_lecturers->p_id)  ? "selected" : "" !!}>{!! $po->name !!}</option>	
									@endforeach()

								</select>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('p_id') }}</span>
							</div>
							
							<div class="form-group  {{ $errors->has('staff') ? 'has-error' : '' }}">
								<label for="exampleInputPassword1">Hướng dẫn</label>
								<select name="staff" id="inputGender" class="form-control"  value="{!! old('staff')!!}" @if( $errors->has('staff')) autofocus @endif>
									<option value="">--Vui lòng chọn--</option>
									<option value="0" {{$edit_lecturers->staff ==0 ? 'selected' :''}}>Không hướng dẫn</option>
									<option value="1" {{$edit_lecturers->staff ==1 ? 'selected' :''}}>Hướng dẫn</option>	
								</select>
								<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('staff') }}</span>
							</div>

							<div class="user-action" >
								<button type="submit" class="btn btn-success">
								   <span class="glyphicon glyphicon-ok"></span> Cập nhật</button>
								<button type="reset" class="btn btn-danger">
								   <span class="glyphicon glyphicon-refresh"></span> Nhập lại</button>
							</div>
						</div>
					</form>
					<div class="col-md-3"></div>
				</div>
			</div>
		</section>
	</div>
	@stop
