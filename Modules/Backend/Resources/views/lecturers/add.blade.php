@extends('backend::layouts.master')

@section('title')
Quản lý Giảng Viên
@stop
@section('styleSheet')
	<style type="text/css">
		.form-horizontal .form-group{
			margin-left: 0px;
			margin-right: 0px;
		}
	</style>
@endsection
@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->

	<ul class="breadcrumb">
		<li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
		<li><a href="{{route('lecturers-index')}}">Giảng Viên</a></li>
		<li><a href="#" style="color: red">Thêm mới</a></li>
	</ul>

	<section class="content container" id="lecturers">
		<div class="row">
			<div class="col-xs-12">                    
				<div class="text-right col-md-11">
					<a href="{!! route('lecturers-index') !!}">
						<button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
					</a>
				</div>

				<div class="col-md-11 add-user">
					<div class="row">
						<form action="" method="post" enctype="multipart/form-data" id="demo-form2"  class="form-horizontal form-label-left" novalidate="">
							{{ csrf_field() }}
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('l_fullname') ? 'has-error' : '' }}" >
									<label for="exampleInputEmail1">Họ và Tên</label>
									<input type="text" class="form-control" name="l_fullname" placeholder="Nhập Họ và tên" value="{!! old('l_fullname') !!}" @if( $errors->has('u_fullname')) autofocus
										   @elseif ($errors->has('l_username')) @elseif($errors->has('l_email')) @elseif($errors->has('l_phone')) @elseif($errors->has('l_birthday')) @elseif($errors->has('info')) @elseif($errors->has('image'))
										   @elseif($errors->has('marride')) @elseif($errors->has('gender'))
										   @elseif($errors->has('p_id')) @elseif($errors->has('l_address'))
										   @elseif(session('error-mail')) @elseif($errors->has('d_id')) @else autofocus
											@endif>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_fullname') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('l_username') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Tên</label>
									<input type="text" class="form-control" name="l_username" value="{!! old('l_username') !!}" placeholder="Nhập tên" @if( $errors->has('l_username')) autofocus @endif>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_username') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('l_email') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Email</label>
									<input type="email" class="form-control" name="l_email" value="{!! old('l_email')!!}"  placeholder="Nhập Email" @if( $errors->has('l_email')) autofocus @endif>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_email') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('l_phone') ? 'has-error' : '' }}" style="margin-top: 26px">
									<label for="exampleInputPassword1">Số Điện Thoại</label>
									<input type="number" class="form-control" name="l_phone" maxlength="11" value="{!! old('l_phone')!!}" placeholder="Nhập Số điện Thoại" @if( $errors->has('l_phone')) autofocus @endif >
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_phone') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('l_birthday') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Ngày sinh</label>
									<input type="date" maxlength="6" class="form-control" name="l_birthday"   value="{!! old('l_birthday')!!}" placeholder="Nhập ngày sinh" @if( $errors->has('l_birthday')) autofocus @endif>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_birthday') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('info') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Thông tin cơ bản</label>
									<textarea name="info" id="inputInfo" class="form-control" rows="3" placeholder="Nhập thông tin cơ bản" @if( $errors->has('info')) autofocus @endif>{!! old('info')!!}</textarea>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('info') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('info') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Link URL</label>
									<input type="url" name="link_url" class="form-control" rows="3" placeholder="Nhập link liên kết" value="{{ old('link_url') }}">
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('link_url') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('p_id') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Chức vụ</label>
									<select name="p_id" id="inputGender" class="form-control"  value="{!! old('status')!!}" @if( $errors->has('p_id')) autofocus @endif>
										<option value="">--Chọn chức vụ--</option>
										@foreach($position as $po)
											<option value="{!! $po->id !!}" {{ $po->id == old('p_id') ? "selected" : "" }}>{!! $po->name !!}</option>
										@endforeach()
									</select>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('p_id') }}</span>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group  {{ $errors->has('image') ? 'has-error' : '' }}">
									<label>Ảnh </label>
									<input type="file" accept="images/*" onchange='openFile(event)' name="image" value="{!! old('image') !!}" class="form-control" @if( $errors->has('image')) autofocus @endif>
                                   <img id="output" src="<?php echo url('/')?>/images/web-04.jpg" style="margin-top: 15px; width: 150px; height: 150px" />
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('image') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('gender') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Giới Tính</label>
									<select name="gender" id="inputGender"   class="form-control" @if( $errors->has('gender')) autofocus @endif>
										<option value="" >--Chọn Gender--</option>
										<option value="0" {{ old('gender') == 0 ? 'selected' : '' }}>Nữ</option>
										<option value="1" {{ old('gender') == 1 ? 'selected' : '' }}>Nam</option>
									</select>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('gender') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('marride') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Hôn nhân</label>
									<select name="marride" id="inputGender" class="form-control"  value="{!! old('marride')!!}" @if( $errors->has('marride')) autofocus @endif >
										<option value="">--Chọn Married--</option>
										<option value="0" {{ old('marride') == 0 ? 'selected' : '' }}>Độc Thân</option>
										<option value="1" {{ old('marride') == 1 ? 'selected' : '' }}>Đã Kết Hôn</option>
									</select>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('marride') }}</span>
								</div>
								<div class="form-group  {{ $errors->has('l_address') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Địa Chỉ</label>
									<textarea type="text" class="form-control" name="l_address"   value="{!! old('l_address')!!}" placeholder="Nhập địa chỉ" @if( $errors->has('l_address')) autofocus @endif ></textarea>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('l_address') }}</span>
								</div>

								<div class="form-group  {{ $errors->has('d_id') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Bằng Cấp</label>
									<select name="d_id" id="inputGender" class="form-control"  value="{!! old('d_id')!!}" @if( $errors->has('d_id')) autofocus @endif>
										<option value="">--Chọn Bằng cấp--</option>
										@foreach($degree as $de)
											<option value="{!! $de->id !!}"{{ $de->id == old('d_id') ? "selected" : "" }}>{!! $de->name !!}</option>
										@endforeach()
									</select>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('d_id') }}</span>
								</div>
								

								<div class="form-group  {{ $errors->has('staff') ? 'has-error' : '' }}">
									<label for="exampleInputPassword1">Hướng dẫn</label>
									<select name="staff" id="inputGender" class="form-control"  value="{!! old('staff')!!}" @if( $errors->has('staff')) autofocus @endif>
										<option value="">--Vui lòng chọn--</option>
										<option value="0" {{ old('staff') == 0 ? 'selected' : '' }}>Không hướng dẫn</option>
										<option value="1" {{ old('staff') == 1 ? 'selected' : '' }}>Hướng dẫn</option>
									</select>
									<span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('staff') }}</span>
								</div>

								<div class="user-action" >
									<button type="submit" class="btn btn-success">
										<span class="glyphicon glyphicon-plus"></span> Thêm mới</button>
									<button type="reset" class="btn btn-danger">
										<span class="glyphicon glyphicon-refresh"></span> Nhập lại</button>
								</div>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div>
	</section>
</div>
@stop
