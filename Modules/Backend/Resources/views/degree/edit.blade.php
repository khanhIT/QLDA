@extends('backend::layouts.master')

@section('title')
    Quản lý bằng cấp
@stop
@section('styleSheet')
@endsection
@section('content')
   <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
            <li><a href="{!! route('degree-index') !!}">Bằng cấp</a></li>
             <li><a href="#" style="color: red">Thêm mới</a></li>
        </ul>
                   
        <section class="content container" id="classRoom">
            <div class="row">
                <div class="col-xs-12"></div>
                <div class="text-right col-md-11">
                    <a href="{!! route('degree-index') !!}">
                        <button class="btn btn-sm btn-info list"><i class="fa fa-list" aria-hidden="true"></i> Danh sách</button>
                    </a>
                </div>
                <div class="col-md-3"></div>
                   	<div class="col-md-6 add-user">
                 			<form action="" method="POST" >
                        {{ csrf_field() }}
                 				<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                          <label for="exampleInputEmail1">Tên bằng cấp</label>
                          <input type="text" class="form-control" name="name" placeholder="Nhập tên"  autofocus value="{!! old('name', $degree->name) !!}">
                          <span class="text-danger">@if (session('error')){{ session('error') }}@endif{{ $errors->first('name') }}</span>
                        </div>

                   			<div class="class-action">
                          <button type="reset" class="btn btn-primary">
                            <span class="glyphicon glyphicon-refresh"></span> Nhập lại</button>
                     			<button type="submit" class="btn btn-success">
                            <span class="glyphicon glyphicon-ok"></span> Cập nhập</button>
                   			</div>
                 		  </form>
                    </div>
                  <div class="col-md-3"></div>
                </div>
            </div>
        </section>
    </div>
@stop
