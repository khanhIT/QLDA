@extends('layouts.master')
@section('content')
    <div style="width:100%;
    float: left;
    font-family: arial;
    background: #eee; ">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="
    margin: auto;">
            <div style="font-family: Arial,Verdana,sans-serif;
            width: 500px;
            float: left;
            background: #fff;
            padding: 10px 19px;
            margin-bottom: 30px;
            border-radius: 0px 0px 5px 5px;
            border-top: 2px #bc002d solid;
            margin-top: 30px;
            margin-left: 297px;"
            >
                <div style="margin-bottom: 20px;">
                    <em>Chào : <b style="color: red; font-weight: bold;">{{$data['name']}}</b></em>
                </div>
                <div>
                    <h2 style="color: #000;font-weight: bold">Chúng tôi đã trả lời liên hệ cho bạn</h2>
                </div>

                <div style="width: 88%;float: left;background: #b6ecff;font-family: Arial,Verdana,sans-serif;padding: 30px;line-height: 40px;border-radius: 5px;">
                  <p><b style="color: Red">Ngày bạn liên hệ:</b>{{date_format(new DateTime($data['created_at']),'d-m-Y')}}</p>
                    <p><b style="color: Red">Nội dung bạn liên hệ:</b>{!! $data['content'] !!}</p>
                    <p><b style="color: #c561bd;font-weight: bold">Trả lời:</b> {!! $data['reply'] !!}</p>
                </div>
            </div>

        </div>
        <div class="col-md-1"></div>

    </div>
@endsection
