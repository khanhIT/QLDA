@extends('layouts.master')
@section('content')
<div style="width: 100%;
    float: left;
    font-family: arial;
    background: #eee; ">
    <div style="width: 500px;
    margin: auto;">
        <div style="    font-family: Arial,Verdana,sans-serif;
    width: 500px;
    float: left;
    background: #fff;
    padding: 10px 19px;
    margin-bottom: 30px;
    border-radius: 0px 0px 5px 5px;
    border-top: 2px #bc002d solid;
    margin-top: 30px;">
            <em>Chào : <b style="color: red">{{$data['l_fullname']}}</b></em>
            <div>
                <h2 style="color: #000;font-weight: bold">Sinh Viên <b style="color: red;">{{$data['fullname']}}</b> Gửi liên hệ đến Thầy (Cô).</h2>
            </div>
            <div>

                <h4>Thông tin sinh viên đã liên hệ</h4>
            </div>
            <div style="width: 80%;float: left;background: #b6ecff;font-family: Arial,Verdana,sans-serif;padding: 30px;line-height: 40px;border-radius: 5px;">
                <table>
                    <tbody>
                    <tr>
                        <th>Tên Sinh viên:</th>
                        <td>{{$data['fullname']}}</td>
                    </tr>
                    <tr>
                        <th>Mã Số Sinh Viên:</th>
                        <td>{{$data['msv']}}</td>
                    </tr>
                    <tr>
                        <th>Tên Đồ án sinh viên :</th>
                        <td> <?php
                            if($data['pro_name']==null){
                                echo $data['project_name'];
                            }else{
                                echo $data['pro_name'];
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <th>Nội dung liên hệ:</th>
                        <td>{{$data1}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
@endsection