@extends('layouts.master')
@section('content')
<div style="width:100%;
    float: left;
    font-family: arial;
    background: #eee; ">
    <div class="col-md-1"></div>
    <div class="col-md-10" style="
    margin: auto;">
        <div style="font-family: Arial,Verdana,sans-serif;
            width: 500px;
            float: left;
            background: #fff;
            padding: 10px 19px;
            margin-bottom: 30px;
            border-radius: 0px 0px 5px 5px;
            border-top: 2px #bc002d solid;
            margin-top: 30px;
            margin-left: 297px;"
        >
            <em>Chào : <b style="color: red; font-weight: bold;">{{$data['fullname']}}</b></em>
            <div>
                <h2 style="color: #000;font-weight: bold">Bạn đã chọn Thầy (Cô)  <b style="color: red;">{{$data['l_fullname']}}</b> là người hướng dẫn đồ án tốt nghiệp.</h2>
            </div>
            <div>
                <h4>Thông tin của bạn đã đăng ký</h4>
            </div>
            <div style="width: 88%;float: left;background: #b6ecff;font-family: Arial,Verdana,sans-serif;padding: 30px;line-height: 40px;border-radius: 5px;">
                <table>
                    <tbody>
                    <tr>
                        <th>Tên Sinh viên:</th>
                        <td>{{$data['fullname']}}</td>
                    </tr>
                    <tr>
                        <th>Mã Số Sinh Viên:</th>
                        <td>{{$data['msv']}}</td>
                    </tr>
                    <tr>
                        <th>Tên Đồ án:</th>
                        <td>
                            <?php
                                if($data['name']==null){
                                    echo $data['project_name'];
                                }else{
                                    echo $data['name'];
                                }
                            ?>
                       </td>
                    </tr>
                    <tr>
                        <th>Tên Giáo Viên Hướng Dẫn:</th>
                        <td>{{$data['l_fullname']}}</td>
                    </tr>
                    <tr>
                        <th>Số điện thoại giáo viên:</th>
                        <td>{{$data['l_phone']}}</td>
                    </tr>
                    <tr>
                        <th>Email giáo viên:</th>
                        <td>{{$data['l_email']}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="col-md-1"></div>

</div>
@endsection
