@extends('layouts.master')
@section('content')
    <div style="width:100%;
    float: left;
    font-family: arial;
    background: #eee; ">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="
    margin: auto;">
            <div style="font-family: Arial,Verdana,sans-serif;
            width: 500px;
            float: left;
            background: #fff;
            padding: 10px 19px;
            margin-bottom: 30px;
            border-radius: 0px 0px 5px 5px;
            border-top: 2px #bc002d solid;
            margin-top: 30px;
            margin-left: 297px;"
            >
              <div style="margin-bottom: 20px;">
                    <em>Chào : <b style="color: red; font-weight: bold;">Admin</b></em>
                </div>
                <div>
                    <h2 style="color: #000;font-weight: bold">Thông Tin Đồ Án Sinh Viên Đăng Ký</h2>
                </div>
              
                <div style="width: 88%;float: left;background: #b6ecff;font-family: Arial,Verdana,sans-serif;padding: 30px;line-height: 40px;border-radius: 5px;">
                    <table>
                        <tbody>
                        <tr>
                            <th>Tên Sinh viên:</th>
                            <td>{{$data['fullname']}}</td>
                        </tr>
                        <tr>
                            <th>Mã Số Sinh Viên:</th>
                            <td>{{$data['msv']}}</td>
                        </tr>
                        <tr>
                            <th>Ngày sinh sinh viên</th>
                            <td>{{date_format(new DateTime($data['birthday']),'d-m-Y')}}</td>
                        </tr>
                        <tr>
                            <th>Số điện thoại sinh viên:</th>
                            <td>{{$data['phone']}}</td>
                        </tr>
                        <tr>
                            <th>Email sinh viên:</th>
                            <td>{{$data['email']}}</td>
                        </tr>
                        <tr>
                            <th>Lớp sinh viên</th>
                            <td>{{$data['c_name']}}</td>
                        </tr>
                        <tr>
                            <th>Khóa sinh viên</th>
                            <td>K{{$data['s_key']}}</td>
                        </tr>
                        <tr>
                            <th>Tên Đồ án:</th>
                            <td> <?php
                                if($data['name']==null){
                                    echo $data['project_name'];
                                }else{
                                    echo $data['name'];
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <th>Tên Giáo Viên Hướng Dẫn:</th>
                            <td>{{$data['l_fullname']}}</td>
                        </tr>
                        <tr>
                            <th>Số điện thoại giáo viên:</th>
                            <td>{{$data['l_phone']}}</td>
                        </tr>
                        <tr>
                            <th>Email giáo viên:</th>
                            <td>{{$data['l_email']}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="col-md-1"></div>

    </div>
@endsection
