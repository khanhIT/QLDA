<?php

namespace App\Jobs;

use App\Mail\Information;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendMailCustomer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $title;
    public $content;
    public function __construct($title,$content)
    {
        $this->title=$title;
        $this->content=$content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            $getCustomerLimitSendMail = \DB::table('email_register')
                ->get();

            if ( !empty($getCustomerLimitSendMail) ) {
                foreach ( $getCustomerLimitSendMail as $customerSendMail) {
                    \Mail::to($customerSendMail->email)
                        ->queue(new Information($this->title, $this->content));
                }
            }

    }
}
