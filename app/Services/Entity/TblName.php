<?php

namespace App\Services\Entity;

class TblName {

    const CLASS_NAME = 'class_name';
    const SCHOOL_YEAR = 'school_year';
    const LECTURERS = 'lecturers';
}
