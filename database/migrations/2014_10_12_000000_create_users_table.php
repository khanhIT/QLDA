<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->collation = 'utf8_unicode_ci';
            $table->charset = 'utf8';

            $table->increments('id');
            $table->integer('role_id')->unsigned()->nullable();
            $table->string('u_fullname')->nullable();
            $table->string('u_name')->nullable();
            $table->string('u_email')->nullable();
            $table->string('u_password')->nullable();
            $table->string('avatar')->nullable();
            $table->string('feature_image')->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('skype', 100)->nullable();
            $table->date('birthday')->nullable();
            $table->tinyInteger('gender')->nullable()->comment('0: Female, 1: Male');
            $table->tinyInteger('married')->nullable()->comment('0: un-married, 1: married');
            $table->string('address')->nullable();
            $table->rememberToken();
            $table->timestamps();


        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
