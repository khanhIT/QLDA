<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->collation = 'utf8_unicode_ci';
            $table->charset =  'utf8';
            $table->increments('id');
            $table->integer('cus_id')->unsigned()->nullable();
            $table->integer('c_id')->unsigned()->nullable();
            $table->integer('s_id')->unsigned()->nullable();
            $table->integer('l_id')->unsigned()->nullable();
            $table->string('name_project')->nullable();
            $table->string('file_upload_word')->nullable();
            $table->string('upload_source_code')->nullable();
            $table->tinyInteger('status')->nullable()->default(0)->comment('1: Đã bảo vệ, 0: Chưa bảo vệ, 2: Mới upload file');

            $table->timestamps();

//            $table->foreign('cus_id')->references('cus_id')->on('customer');
//
//            $table->foreign('c_id')->references('c_id')->on('class');
//
//            $table->foreign('s_id')->references('s_id')->on('school_year');
//
//            $table->foreign('l_id')->references('l_id')->on('lecturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
