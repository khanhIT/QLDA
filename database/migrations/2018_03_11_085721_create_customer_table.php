<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->collation = 'utf8_unicode_ci';
            $table->charset =  'utf8';
            $table->increments('cus_id');
            $table->integer('c_id')->unsigned()->nullable();
            $table->integer('s_id')->unsigned()->nullable();
            $table->integer('l_id')->unsigned()->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('birthday')->nullable();
            $table->string('address')->nullable();
            $table->string('project_name')->nullable();
            $table->tinyInteger('status')->nullable()->default(0)->comment('0: chưa đăng ký, 1: đã đăng ký');

            $table->timestamps();


//            $table->foreign('c_id')->references('c_id')->on('class');
//
//            $table->foreign('s_id')->references('s_id')->on('school_year');
//
//            $table->foreign('l_id')->references('l_id')->on('lecturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}
