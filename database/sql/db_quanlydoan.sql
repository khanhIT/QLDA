-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 09, 2018 lúc 08:58 SA
-- Phiên bản máy phục vụ: 10.1.21-MariaDB
-- Phiên bản PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_quanlydoan`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banner`
--

CREATE TABLE `banner` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cadres_read`
--

CREATE TABLE `cadres_read` (
  `id` int(11) NOT NULL,
  `cus_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `class_name`
--

CREATE TABLE `class_name` (
  `id` int(10) UNSIGNED NOT NULL,
  `c_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `class_name`
--

INSERT INTO `class_name` (`id`, `c_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'DCCTTD59_1', 'Đại học-Tin trắc địa-Công nghệ thông tin-K59', '2018-04-02 02:26:27', '2018-04-02 02:26:27'),
(2, 'DCCTTD59_2', 'Đại học-Tin trắc địa-K59', '2018-04-02 02:27:20', '2018-04-02 02:27:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `address`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Hoàng Minh Khánh', 'khanhhoang220596@gmail.com', 'Hà Nội', 'Liên hệ', '2018-04-08 17:08:58', '2018-04-08 17:08:58');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `custormer`
--

CREATE TABLE `custormer` (
  `id` int(10) UNSIGNED NOT NULL,
  `msv` int(11) NOT NULL,
  `c_id` int(10) UNSIGNED DEFAULT NULL,
  `s_id` int(10) UNSIGNED DEFAULT NULL,
  `l_id` int(10) UNSIGNED DEFAULT NULL,
  `fullname` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '0: chưa đăng ký, 1: đã đăng ký',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `custormer`
--

INSERT INTO `custormer` (`id`, `msv`, `c_id`, `s_id`, `l_id`, `fullname`, `email`, `phone`, `birthday`, `address`, `project_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1421050095, 1, 1, 1, 'Hoàng Minh Khánh', 'anhkhanh220596@gmail.com', '0973605319', '1996-05-22', 'Hà Nội', '3', 1, '2018-04-07 15:05:51', '2018-04-07 15:05:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `degree`
--

CREATE TABLE `degree` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `degree`
--

INSERT INTO `degree` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'NSƯT.PGS.TS', '2018-04-02 15:48:07', '2018-04-02 15:48:07'),
(2, 'GV', '2018-04-02 15:49:04', '2018-04-02 15:49:04'),
(3, 'GV.TS', '2018-04-02 15:49:16', '2018-04-02 15:49:16'),
(4, 'Tiến sĩ', '2018-04-02 15:49:42', '2018-04-02 15:50:25'),
(5, 'Thạc sĩ', '2018-04-02 15:49:54', '2018-04-02 15:50:34'),
(6, 'Kỹ sư', '2018-04-02 15:50:13', '2018-04-02 15:50:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `examiner`
--

CREATE TABLE `examiner` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `lecturers`
--

CREATE TABLE `lecturers` (
  `id` int(10) UNSIGNED NOT NULL,
  `p_id` int(11) DEFAULT NULL,
  `d_id` int(11) DEFAULT NULL,
  `l_fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_birthday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `staff` int(11) DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `link_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marride` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '0: đọc thân 1: đã kết hôn',
  `l_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: cán bộ 1: lãnh đạo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `lecturers`
--

INSERT INTO `lecturers` (`id`, `p_id`, `d_id`, `l_fullname`, `l_username`, `l_email`, `l_birthday`, `l_phone`, `staff`, `info`, `link_url`, `image`, `gender`, `marride`, `l_address`, `status`, `created_at`, `updated_at`) VALUES
(1, 3, 5, 'Nguyễn Tuấn Anh', 'Tuấn Anh', 'khanhhoang220596@gmail.com', '1976-04-01', '0988896936', 1, 'KS Toán - Tin (ĐHDL Thăng Long, 2000) CN Ngôn ngữ Anh (ĐH Hà Nội, 2015), ThS Công nghệ thông tin (HVKT Quân sự, 2008). Chủ trì xây dựng “Hệ thống phần mềm quản lý đào tạo Sau đại học theo hệ tín chỉ của Trường Đại học Mỏ-Địa chất - HumgSDHSoft (2012-2015)”. Hướng nghiên cứu chính: Khoa học máy tính, phần mềm ứng dụng và quản lý.', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5317>								    <span class=', '5ac0f1ae2af5c.JPG', '1', '1', 'Cổ Nhuế', 0, '2018-04-01 14:50:22', '2018-04-02 17:25:51'),
(2, 3, 4, 'Nguyễn Thị Mai Dung', 'Mai Dung', 'nguyenthimaidung@humg.edu.vn', '2018-04-01', '0985276806', 0, 'Tốt nghiệp Kỹ thuật trắc địa của ĐH Mỏ - Địa chất, học Thạc sĩ Địa tin học tại Học viện Kỹ Thuật Châu Á - AIT, Bangkok,Thái Lan. Học Tiến sĩ tại Đại học TOHOKU, Nhật Bản. Hướng nghiên cứu chính: Xử lý ảnh, Viễn thám và GIS', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5661', '5ac0f2d6ee6a7.JPG', '0', '1', 'Cổ Nhuế', 0, '2018-04-01 14:55:18', '2018-04-01 14:55:18'),
(3, 3, 5, 'Đoàn Khánh Hoàng', 'Khánh Hoàng', 'doankhanhhoang@humg.edu.vn', '1981-04-01', '0904744590', 0, 'Từng là sinh viên tài năng và lấy bằng Thạc sĩ Công nghệ thông tin tại Đại học Bách Khoa. Đam mê lập trình và có nhiều sản phẩm phần mềm ứng dụng. Hiện tại đang làm NCS ở Đức', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5670', '5ac0f3560dab1.JPG', '1', '1', 'Cổ Nhuế', 0, '2018-04-01 14:57:26', '2018-04-01 14:57:26'),
(4, 3, 5, 'Trần Mai Hương', 'Mai Hương', 'tranmaihuong@humg.edu.vn', '1980-04-01', '0904629269', 1, 'Tốt nghiệp Kỹ thuật trắc địa của ĐH Mỏ - Địa chất, lấy bằng Kỹ sư Công nghệ thông tin tại Học viện Kỹ thuật quân sự năm 2007. Hướng nghiên cứu chính: Đo ảnh viễn thám và GIS', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5663', '5ac0f3b839a0f.JPG', '0', '1', 'Cầu Diễn', 0, '2018-04-01 14:59:04', '2018-04-01 14:59:04'),
(5, 3, 5, 'Ngô Thị Phương Thảo', 'Phương Thảo', 'ngothiphuongthao@humg.edu.vn', '2018-04-01', '0982198688', 0, 'Thạc sĩ Kỹ thuật trắc địa của ĐH Mỏ - Địa chất, lấy bằng Kỹ sư Công nghệ thông tin tại ĐH Bách khoa năm 2004. Hướng nghiên cứu chính: Bản đồ viễn thám và GIS', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5662', '5ac0f44f50989.JPG', '0', '1', 'Cổ Nhuế', 0, '2018-04-01 15:01:35', '2018-04-01 15:01:35'),
(6, 4, 5, 'Diêm Thị Thùy', 'Diêm Thùy', 'diemthithuy@humg.edu.vn', '2018-04-01', '0984290010', 0, 'Tốt nghiệp Kỹ thuật Trắc địa bản đồ, lấy bằng thạc sĩ Bản đồ viễn thám và GIS tại ĐH Mỏ - Địa chất, lấy bằng kỹ sư Công nghệ thông tin tại Viện ĐH Mở. Hướng nghiên cứu chính: Bản đồ viễn thám và GIS', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5665', '5ac0f518b15f4.JPG', '0', '1', 'Cổ Nhuế', 0, '2018-04-01 15:04:56', '2018-04-01 15:04:56'),
(7, NULL, 6, 'Nguyễn Thị Phấn', 'Nguyễn Phấn', 'nguyenthiphan@humg.edu.vn', '2018-04-01', '0946946968', 0, 'Tốt nghiệp Kỹ thuật trắc địa tại ĐH Mỏ - Địa chất', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5666', '5ac0f578a7870.JPG', '0', '1', 'Cổ Nhuế', 0, '2018-04-01 15:06:32', '2018-04-01 15:06:32'),
(8, 3, 5, 'Nguyễn Hoàng Long', 'Hoàng Long', 'nguyenhoanglong@humg.edu.vn', '2018-04-01', '0916666384', 0, 'Là cựu sinh viên khóa 47 được giữ lại làm giảng viên. Lấy bằng Thạc sĩ Địa tin học ở ITC Hà Lan. Hiện tại đang làm NCS tại Úc.', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5667', '5ac0f5d960c20.JPG', '1', '0', 'Cổ Nhuế', 0, '2018-04-01 15:08:09', '2018-04-01 15:08:09'),
(9, 3, 5, 'Trần Trường Giang', 'Trường Giang', 'trantruonggiang@humg.edu.vn', '2018-04-27', '123456786', 0, 'Cựu sinh viên khóa 48, được giữ lại làm giảng viên. Lấy bằng Thạc sĩ Địa tin học ở ITC Hà Lan. Tháng 9 năm 2015 bắt đầu làm NCS ở ĐH Vũ Hán Trung Quốc', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5671', '5ac0f638c975f.JPG', '1', '0', 'Cổ Nhuế', 0, '2018-04-01 15:09:44', '2018-04-01 15:09:44'),
(10, 3, 5, 'Trần Thị Hòa', 'Trần Hòa', 'tranthihoa@humg.edu.vn', '2018-04-01', '0979090687', 0, 'Từng là sinh viên xuất sắc, tốt nghiệp thủ khoa, lấy bằng Thạc sĩ Bản đồ viễn thám và GIS ở ĐH Mỏ - Địa chất. Hiện đang làm NCS ở Mỹ', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5668', '5ac0f69d5838f.JPG', '0', '0', 'Nhà riêng: Số H1-5, Khu B Đại học Mỏ - Địa Chất  Cổ Nhuế 2, Bắc Từ Liêm, Hà Nội', 0, '2018-04-01 15:11:25', '2018-04-01 15:11:25'),
(11, 3, 6, 'Đinh Ngọc Bảo', 'Ngọc Bảo', 'dinhbaongoc@humg.edu.vn', '2018-04-01', '0975275118', 0, 'Là cựu sinh viên khóa 52 được giữ lại làm giảng viên.', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5664', '5ac0f755640ba.JPG', '1', '0', 'Cổ Nhuế', 0, '2018-04-01 15:14:29', '2018-04-01 15:14:29'),
(12, 3, 5, 'Nguyễn Trường Linh', 'Trường Linh', 'nguyentruonglinh@humg.edu.vn', '2018-04-01', '123456786', 0, 'Cựu sinh viên khóa 52, tốt nghiệp loại giỏi và được giữ lại làm giảng viên. Hiện đang học thạc sĩ tại Hàn Quốc', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=6474', '5ac0f7aceec1a.JPG', '1', '0', 'Cổ Nhuế', 0, '2018-04-01 15:15:56', '2018-04-01 15:15:56'),
(13, 1, 1, 'Nguyễn Trường Xuân', 'Trường Xuân', 'nguyentruongxuan@humg.edu.vn', '1953-04-01', '0982896936', 0, '“Là người sáng lập Khoa Công nghệ thông tin của Đại học Mỏ - Địa chất, người thầy có sức ảnh hưởng sâu rộng đến nhiều thế hệ học trò. Hầu hết các cán bộ của bộ môn là học trò của thầy Nguyễn Trường Xuân”', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5286', '5ac0faf618d9f.jpg', '1', '1', 'Số 34, ngõ 1002, Đường Láng, Đống Đa, Hà Nội', 1, '2018-04-01 15:29:58', '2018-04-01 15:29:58'),
(14, 2, 2, 'Trần Trung Chuyên', 'Trung Chuyên', 'trantrungchuyen@humg.edu.vn', '1977-04-01', '0983448779', 1, '“Là một giảng viên đầy tâm huyết, đam mê lập trình, có nhiều đóng góp cho cộng đồng bằng việc tạo ra các phần mềm hữu ích. Năm 2000, tốt nghiệp Kỹ thuật trắc địa - bản đồ từ Đại học Mỏ - Địa chất. Năm 2007, lấy bằng kỹ sư Công nghệ thông tin từ Học viện Kỹ thuật quân sự. Ngoài ra, còn là một nhà phát triển phần mềm trên nền tảng iOS và đã có nhiều phần mềm hữu ích có mặt trên Apple Store”', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5659', '5ac0fbea2f3da.jpg', '1', '1', '7.09, Nhà C, Trường ĐH Mỏ - Địa chất, Số 18, phố Viên, phường Đức Thắng, quận Bắc Từ Liêm, Hà Nội', 1, '2018-04-01 15:34:02', '2018-04-01 15:34:02'),
(15, 2, 3, 'Nguyễn Quang Khánh', 'Quang Khánh', 'nguyenquangkhanh@humg.edu.vn', '2018-04-01', '0912189981', 0, '“Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum”', 'http://humg.edu.vn/gioi-thieu/co-cau-to-chuc/cac-khoa/Pages/khoa-cong-nghe-thong-tin.aspx?ItemID=5329', '5ac0fc622e1a7.jpg', '1', '1', 'Tầng 7, nhà C12 tầng, Trường Đại học Mỏ - Địa chất, số 18 Phố Viên, phường Đức Thắng, quận Bắc Từ Liêm, Hà Nội', 1, '2018-04-01 15:36:02', '2018-04-01 15:36:02');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `list_project`
--

CREATE TABLE `list_project` (
  `id` int(11) NOT NULL,
  `l_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `del_flag` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `list_project`
--

INSERT INTO `list_project` (`id`, `l_id`, `name`, `del_flag`, `created_at`, `updated_at`) VALUES
(1, 4, 'Web bán hàng', 0, '2018-04-03 04:38:59', '2018-04-03 04:38:59'),
(2, 14, 'Quản lý đồ án tốt nghiệp', 0, '2018-04-03 04:45:34', '2018-04-03 06:50:44'),
(3, 1, 'Web bán hàng trực tuyến', 0, '2018-04-03 04:46:07', '2018-04-03 04:46:07'),
(4, 14, 'Quản lý đất đai', 0, '2018-04-08 15:52:35', '2018-04-08 15:52:35');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `memorial_photo`
--

CREATE TABLE `memorial_photo` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `memorial_photo`
--

INSERT INTO `memorial_photo` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'name1', '5ac1088f69f04.JPG', '2018-04-01 16:27:59', '2018-04-01 16:27:59'),
(2, 'name2', '5ac108a1d46ad.JPG', '2018-04-01 16:28:17', '2018-04-01 16:28:17'),
(3, 'name3', '5ac108afa4547.JPG', '2018-04-01 16:28:31', '2018-04-01 16:28:31'),
(4, 'name4', '5ac108bb25138.JPG', '2018-04-01 16:28:43', '2018-04-01 16:28:43'),
(5, 'name5', '5ac108c803e7a.JPG', '2018-04-01 16:28:56', '2018-04-01 16:28:56'),
(6, 'name6', '5ac108d8a4c28.JPG', '2018-04-01 16:29:12', '2018-04-01 16:29:12'),
(7, 'name7', '5ac108e8a4548.JPG', '2018-04-01 16:29:28', '2018-04-01 16:29:28'),
(8, 'name8', '5ac108f61455e.JPG', '2018-04-01 16:29:42', '2018-04-01 16:29:42'),
(9, 'name9', '5ac1092e8233f.JPG', '2018-04-01 16:30:38', '2018-04-01 16:30:38'),
(10, 'name10', '5ac1093d6022f.JPG', '2018-04-01 16:30:53', '2018-04-01 16:30:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_11_085721_create_customer_table', 1),
(4, '2018_03_11_085927_create_class_table', 1),
(5, '2018_03_11_090032_create_school_year_table', 1),
(6, '2018_03_11_090113_create_project_table', 1),
(7, '2018_03_11_090152_create_lecturers_table', 1),
(8, '2018_03_11_090213_create_role_table', 1),
(9, '2018_03_11_090234_create_slide_table', 1),
(10, '2018_03_11_090306_create_contact_table', 1),
(11, '2018_03_11_090335_create_banner_table', 1),
(12, '2018_03_11_090409_create_typical_student_table', 1),
(13, '2018_03_11_090448_create_memorial_photo_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_vietnamese_ci,
  `image` varchar(255) DEFAULT NULL,
  `image_content` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0:cũ 1: mới',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `image`, `image_content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Bộ môn đón tiếp các cựu sinh viên về dự Lễ kỷ niệm 50 năm ngày thành lập Trường', 'Thân gửi các Cựu sinh viên của Bộ môn Tin học trắc địa,\r\n\r\nLễ kỷ niệm 50 năm ngày thành lập Trường Đại học Mỏ - Địa chất (1966 - 2016) và đón nhận Huân chương Độc lập hạng Ba của trường ta đã diễn ra trong không khí trang trọng, ấm cúng và tràn ngập niềm vui.\r\n\r\nBộ môn Tin học trắc địa đã rất vui mừng được đón tiếp các thế hệ cựu sinh viên và sinh viên của Khoa và của Bộ môn về thăm Trường và tri ân thầy cô nhân dịp 20/11. Cảm ơn tình cảm chân thành của các bạn đã giành cho Trường Đại học Mỏ - Địa chất nói chung và cho Bộ môn Tin học trắc địa nói riêng. Điều đó đã làm cho thầy cô cảm thấy ấm cúng và được động viên rất lớn. Cảm ơn sự sự đóng góp to lớn của các bạn vì đã không ngừng nỗ lực trong học tập và rèn luyện cũng như sự nhiệt huyết với công việc để thành công trong cuộc sống.\r\n\r\nChúc các bạn luôn mạnh khỏe, hạnh phúc và thành công hơn nữa. Trường Đại học Mỏ Địa chất cùng các thầy cô giáo luôn chào đón các em!', '5aca48f8f3095.JPG', '5aca48f8f3834.JPG', 0, '2018-04-08 15:29:32', '2018-04-08 16:53:13'),
(2, '[Hội thảo] Ứng dụng công nghệ Mobile trong lĩnh vực Trắc địa – Bản đồ', 'Thực hiện biên bản ghi nhớ về hợp tác đào tạo và chuyển giao công nghệ giữa Trường Đại học Mỏ - Địa chất và Cục Bản đồ - Bộ tổng tham mưu, ngày 26/05/2015, Cục bản đồ - Bộ tổng tham mưu phối hợp với Khoa Công nghệ Thông tin và Bộ môn Đo ảnh – Viễn thám (Khoa Trắc địa), Trường đại học Mỏ - địa chất tổ chức hội thảo chuyên đề “Ứng dụng công nghệ Mobile trong lĩnh vực Trắc địa - Bản đồ”.\r\n\r\nTham dự hôi thảo về phía Cục Bản đồ - Bộ Tổng tham mưu có Thiếu tướng Trần Văn Thắng – Cục trưởng; Đại tá, TS. Vũ Văn Chất – Phó Cục trưởng; Thượng tá Hoàng Minh Ngọc – Phó Cục trưởng và đông đảo các đồng chí đại diện cho các phòng ban chuyên môn của Cục bản đồ, Bộ tổng tham mưu. Về phía Trường Đại học Mỏ - Địa chất có PGS. TS. Nguyễn Trường Xuân - Giám đốc Trung tâm hỗ trợ phát triển khoa học kỹ thuật Mỏ - Địa chất, Trưởng bộ môn Tin học Trắc địa; PGS. TS. Trần Xuân Trường, Trưởng phòng Hành chính - Tổng hợp, Trưởng bộ môn Đo ảnh – Viễn thám; TS. Nguyễn Quang Khánh, Trưởng khoa Công nghệ Thông tin cùng nhóm nghiên cứu, các thầy, cô giáo khoa Công nghệ thông tin và Bộ môn Đo ảnh & Viễn thám.\r\n\r\nTại buổi hội thảo, các đại biểu đã được nghe NCS. Trần Trung Chuyên - Trưởng nhóm nghiên cứu trình bày hai sản phẩm phần mềm iGeoTrans và ViGIS. Theo đó phần mềm iGeoTrans cung cấp giải pháp để thay thế các thiết bị GPS cầm tay chuyên dụng cho người dùng các thiết bị iOS của Apple, hỗ trợ việc khảo sát, lập bản đồ, công tác thực địa và các lĩnh vực liên quan cho bất cứ vùng nào trên thế giới. Sản phẩm iGeoTrans đã được đưa lên App Store và nhận được những phải hồi tích cực của người dùng trên khắp thế giới, với hơn 100 nghìn lượt tải trên 110 quốc gia sử dụng. Phần mềm ViGIS là một sản phẩm mobile GIS có thể áp dụng vào thực tiễn để hỗ trợ một phần trong công tác Trắc địa – Bản đồ nội nghiệp và ngoại nghiệp.\r\n\r\nBuổi hội thảo diễn ra trong không khí cởi mở, nhiều chuyên gia Cục Bản đồ cũng đã có các đánh giá và trao đổi thẳng thắn liên quan đến hai phần mềm này. Về cơ bản, phía Cục Bản đồ đánh giá cao các tính năng và hiệu quả mà hai phần mềm mang lại. Tuy nhiên, quân đội là lĩnh vực có nhiều yêu cầu khắt khe, các chuyên gia đã nêu ra một số vấn đề mà nhóm nghiên cứu cần tập trung giải quyết để phần mềm này có thể áp dụng vào trong công tác biên tập bản đồ, đặc biệt công tác ngoại nghiệp.\r\n\r\nĐánh giá về buổi hôi thảo, Thiếu tướng Trần Văn Thắng – Cục Trưởng, Cục Bản đồ, Bộ Tổng Tham mưu cho rằng: Đây là cơ hội mở ra sự hợp tác giữa đơn vị đào tạo và đơn vị sản xuất, tạo tiền đề thúc đẩy hợp tác giữa Cục bản đồ - Bộ tổng tham mưu và Trường đại học Mỏ - Địa chất trong thời gian tới. Đồng chí Cục trưởng cũng đã giao cho các đơn vị chức năng chuyên môn tiếp tục phối hợp với nhóm nghiên cứu hoàn thiện thêm sản phẩm để có thể triển khai áp dụng cho quá trình hoạt động của Cục.\r\n\r\nĐại diện cho Trường Đại học Mỏ - Địa chất, TS Nguyễn Quang Khánh – Trưởng Khoa Công nghệ thông tin phát biểu cảm ơn lãnh đạo Cục bản đồ đã tạo điều kiện để tổ chức thành công buổi Hội thảo. Khoa Công nghệ thông tin sẽ tích cực phối hợp cùng với lãnh đạo Cục và các phòng ban chuyên môn tăng cường hợp tác, từng bước nâng cao mối quan hệ giữa Trường Đại học Mỏ - Địa chất và Cục Bản đồ Bộ Tổng tham mưu trên nhiều lĩnh vực.\r\n\r\nMột số hình ảnh trong buổi hội thảo:\r\nNCS. Trần Trung Chuyên thay mặt nhóm nghiên cứu giới thiệu iGeoTran và ViGIS\r\nĐại tá, TS. Vũ Văn Chất đánh giá cao kết quả của 2 phần mềm và đề xuất một số hướng mà phần mềm nên tập trung để có thể phù hợp hơn trong lĩnh vực quân đội\r\nTS. Nguyễn Quang Khánh, Trưởng khoa Công nghệ thông tin - Trường Đại học Mỏ - Địa chất phát biểu tại Hội thảo\r\nCục trưởng Trần Văn Thắng phát biểu đánh giá và tổng kết hội thảo', '5aca48a35f59c.JPG', '5aca48a3600e4.JPG', 0, '2018-04-08 15:33:56', '2018-04-08 16:51:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `position`
--

CREATE TABLE `position` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `position`
--

INSERT INTO `position` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Trưởng bộ môn', '2018-04-02 15:20:06', '2018-04-02 15:20:06'),
(2, 'Phó trưởng bộ môn', '2018-04-02 15:20:47', '2018-04-02 15:27:57'),
(3, 'Giảng viên', '2018-04-02 15:21:23', '2018-04-02 15:21:23'),
(4, 'Trợ giảng', '2018-04-02 15:21:34', '2018-04-02 15:21:34');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `project`
--

CREATE TABLE `project` (
  `id` int(10) UNSIGNED NOT NULL,
  `cus_id` int(10) UNSIGNED DEFAULT NULL,
  `c_id` int(10) UNSIGNED DEFAULT NULL,
  `s_id` int(10) UNSIGNED DEFAULT NULL,
  `l_id` int(10) UNSIGNED DEFAULT NULL,
  `name_project` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_upload_word` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_source_code` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `point` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '1: Đã bảo vệ, 0: Chưa bảo vệ, 2: Mới upload file',
  `group` int(11) DEFAULT NULL,
  `examiner_id` int(11) DEFAULT NULL,
  `expried_at` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `project`
--

INSERT INTO `project` (`id`, `cus_id`, `c_id`, `s_id`, `l_id`, `name_project`, `file_upload_word`, `upload_source_code`, `point`, `status`, `group`, `examiner_id`, `expried_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, '3', 'CSDL_02_04.docx', '3_1421050095_Hoàng Minh Khánh.rar', NULL, 1, NULL, NULL, NULL, '2018-04-07 15:05:51', '2018-04-08 11:34:49');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role`
--

CREATE TABLE `role` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role`
--

INSERT INTO `role` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'giáo viên', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `school_year`
--

CREATE TABLE `school_year` (
  `id` int(10) UNSIGNED NOT NULL,
  `s_key` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `year_start` date DEFAULT NULL,
  `year_end` date DEFAULT NULL,
  `total_student` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `school_year`
--

INSERT INTO `school_year` (`id`, `s_key`, `year_start`, `year_end`, `total_student`, `created_at`, `updated_at`) VALUES
(1, '59', '2014-09-05', '2019-06-30', 33, '2018-04-02 02:43:26', '2018-04-02 02:59:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'slide 1', '5ac9ca268b620.jpg', '2018-04-03 17:01:17', '2018-04-08 07:52:06'),
(2, 'slide 2', '5ac9ca3bb2d38.jpg', '2018-04-03 17:09:14', '2018-04-08 07:52:27'),
(3, 'slide 3', '5ac9ca4d2f005.jpg', '2018-04-03 17:09:56', '2018-04-08 07:52:45');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `typical_student`
--

CREATE TABLE `typical_student` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `typical_student`
--

INSERT INTO `typical_student` (`id`, `name`, `info`, `image`, `link_url`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Mai Phương', 'Thủ khoa đầu vào và tốt nghiệp, luôn dẫn đầu về thành tích học tập, đam mê nghiên cứu khoa học nhằm hỗ trợ phát triển bền vững, hiện đang làm việc tại Tổ chức nông lâm thế giới (ICRAF).  Mai Phương là cựu sinh viên khóa 47', '5ac103fba76ca.JPG', 'https://www.facebook.com/mpsunflower', '2018-04-01 16:08:27', '2018-04-01 16:08:27'),
(2, 'Nguyễn Ngọc Hạnh', 'Project Manager / Team Leader tại Fpt Software, kiêm đào tạo lập trình iOS, Android cho các khóa ở công ty.  Hạnh là cựu sinh viên khóa 52', '5ac104d0830a1.JPG', 'https://www.facebook.com/hanh.k.52', '2018-04-01 16:12:00', '2018-04-01 16:12:00'),
(3, 'Trần Hải Quân', 'Web Developer tại Niteco Việt Nam. Quân là cựu sinh viên khóa 50', '5ac105b00b45d.JPG', 'https://www.facebook.com/quanth0209', '2018-04-01 16:15:44', '2018-04-01 16:15:44'),
(4, 'Nguyễn Xuân Kha', 'Phó giám đốc trung tâm ứng dụng và chuyển giao địa chính phía nam. Tham gia thiết kế, xây dựng, hoàn thiện phần mềm ViLIS 2.0; triển khai ViLIS 2.0 cho các tỉnh phía Nam và các tỉnh thuộc dự án VLAP; phụ trách kỹ thuật cho các dự án của Trung tâm về xây dựng cơ sở dữ liệu đất đai tại các tỉnh phía Nam. Kha là cựu sinh viên khóa 47', '5ac1062559c9f.JPG', 'https://www.facebook.com/khanx.it', '2018-04-01 16:17:41', '2018-04-01 16:17:41'),
(5, 'Trần Xuân Đức', 'Cựu sinh viên khóa 55: \"...để tìm đến thành công còn phải nỗ lực rất nhiều để vượt qua những thất bại. Và điều quan trọng đó là đừng bao giờ lùi bước khi gặp khó khăn\"', '5ac10653ef0e2.JPG', 'https://www.facebook.com/DucSatThuTTD', '2018-04-01 16:18:27', '2018-04-01 16:18:27'),
(6, 'Nguyễn Thanh Hà', 'Khi còn là sinh viên luôn tích cực tham gia các hoạt động tình nguyện, đoàn thanh niên, hội sinh viên. Ngoài ra cũng đạt được một số giải thưởng về nghiên cứu khoa học tiêu biểu cấp trường. Yêu thích du lịch và giao tiếp với con người trên khắp thế giới. Hiện tại đang làm việc tại đất nước mặt trời mọc Nhật Bản', '5ac1069433474.JPG', 'https://www.facebook.com/thanhhattd?fref=ts', '2018-04-01 16:19:32', '2018-04-01 16:19:32'),
(7, 'Phạm Mạnh Đạm', 'Trưởng nhóm phát triển ứng dụng Mobile, Web cho các dự án lâm nghiệp, dự án của các tổ chức phi chính phủ, kiêm quản lý dự án và đào tạo tập huấn, các khóa ứng dụng GIS cho ngành Lâm Nghiệp tại Green Field Consulting & Development Co., Ltd.  Đạm là cựu sinh viên khóa 52', '5ac106cf88471.JPG', 'https://www.facebook.com/dulyyeu', '2018-04-01 16:20:31', '2018-04-01 16:20:31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `u_fullname` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `u_email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `skype` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL COMMENT '0: Female, 1: Male',
  `married` tinyint(4) DEFAULT NULL COMMENT '0: un-married, 1: married',
  `address` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `role_id`, `u_fullname`, `u_name`, `u_email`, `password`, `avatar`, `mobile`, `skype`, `birthday`, `gender`, `married`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hoàng Minh Khánh', 'Khánh', 'khanhhoang220596@gmail.com', '$2y$10$gzQibwU7YmuaSLJsysaC4e5ovwOotfQGPsUntCkd/Myy4667NakQO', '5aaeaf3ce679b.jpg', '0973605319', 'Khánh Hoàng', '1996-05-22', 1, 0, 'Cổ Nhuế', 'cGU76cy7SayaOkKhQvHk9b1oTOmNszPOoBUyVouGUW3cB1ctKjmb31WKCiEJ', '2018-03-18 04:24:26', '2018-03-18 18:26:04'),
(2, 2, 'Khánh Hoàng', 'Hoàng', 'khanh@gmail.com', '$2y$10$3Z168C12CsxeOdYvDPxcHOF1zPM6vLqY0LU1Umts25H4m7iN6vrte', '5ab3e201e51d9.png', '123456789', 'hoàng khánh', '2018-03-18', 1, 0, 'Cổ Nhuế', '3iCFgFauNqroCNeELjI8XCoYG63JjUUHaiVPGafsTZ4UZzbrYXT8lAcU5yb2', '2018-03-18 08:29:39', '2018-03-22 17:04:01');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `cadres_read`
--
ALTER TABLE `cadres_read`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `class_name`
--
ALTER TABLE `class_name`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `custormer`
--
ALTER TABLE `custormer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `s_id` (`s_id`),
  ADD KEY `l_id` (`l_id`),
  ADD KEY `c_id` (`c_id`) USING BTREE;

--
-- Chỉ mục cho bảng `degree`
--
ALTER TABLE `degree`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `examiner`
--
ALTER TABLE `examiner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `lecturers`
--
ALTER TABLE `lecturers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `list_project`
--
ALTER TABLE `list_project`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `memorial_photo`
--
ALTER TABLE `memorial_photo`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cus_id` (`cus_id`),
  ADD KEY `c_id` (`c_id`),
  ADD KEY `s_id` (`s_id`),
  ADD KEY `l_id` (`l_id`);

--
-- Chỉ mục cho bảng `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `school_year`
--
ALTER TABLE `school_year`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `typical_student`
--
ALTER TABLE `typical_student`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `cadres_read`
--
ALTER TABLE `cadres_read`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `class_name`
--
ALTER TABLE `class_name`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `custormer`
--
ALTER TABLE `custormer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `degree`
--
ALTER TABLE `degree`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT cho bảng `examiner`
--
ALTER TABLE `examiner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT cho bảng `lecturers`
--
ALTER TABLE `lecturers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT cho bảng `list_project`
--
ALTER TABLE `list_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `memorial_photo`
--
ALTER TABLE `memorial_photo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `position`
--
ALTER TABLE `position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `project`
--
ALTER TABLE `project`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `role`
--
ALTER TABLE `role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `school_year`
--
ALTER TABLE `school_year`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `typical_student`
--
ALTER TABLE `typical_student`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
