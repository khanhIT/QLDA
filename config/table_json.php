<?php

//define emailRegister table json
define('BANNER_TABLE_JSON', 'emailRegister.json');

//define users table json
define('USERS_TABLE_JSON', 'users.json');

//define slide table json
define('SLIDE_TABLE_JSON', 'slide.json');

//define TypicalStudent table json
define('TypicalStudent_TABLE_JSON', 'typicalStudent.json');

//define MemorialPhoto table json
define('MemorialPhoto_TABLE_JSON', 'memorialPhoto.json');

//define Login table json
define('LOGIN_TABLE_JSON', 'login.json');
//define lecturers table json
define('LECTURERS_TABLE_JSON', 'lecturers.json');

//define register table json
define('REGISTER_TABLE_JSON', 'register.json');

//define listProject table json
define('ListProject_TABLE_JSON', 'listProject.json');

//define Project table json
define('Project_TABLE_JSON', 'project.json');

//define Position table json
define('Position_TABLE_JSON', 'position.json');

//define Degree table json
define('Degree_TABLE_JSON', 'degree.json');

//define Custormer table json
define('CUSTORMER_TABLE_JSON', 'custormer.json');
//define Custormer table json
define('CONTACT_TABLE_JSON', 'contact.json');
//define Custormer table json
define('NEWS_TABLE_JSON', 'news.json');

//define email register table json
define('EMAILREGISTER_TABLE_JSON', 'emailRegister.json');