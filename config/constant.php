<?php


//define the number of pagination
define('RECORD_DEFAULT_PAGINATE', 30);

//define path to folder store json file to validate
define('PATH_TO_VALIDATE_FOLDER', dirname(dirname(__FILE__)) . '/Common/Config/Validation/');

//define folder download
define('DOWNLOAD_FOLDER', dirname(dirname(__FILE__)) . '/public/download/');

//define folder upload
define('UPLOAD_FOLDER', dirname(dirname(__FILE__)) . '/public/upload/');
define('BANNER_UPLOAD_PATH', 'emailRegister/');
define('SLIDE_UPLOAD_PATH', 'slide/');
define('BOOK_UPLOAD_PATH', 'books/images/');
define('BOOK_INSIDE_UPLOAD_PATH', 'books/inside/');
define('TYPICALSTUDENT_UPLOAD_PATH', 'typicalStudent/');
define('MEMORIALPHOTO_UPLOAD_PATH', 'memorialPhoto/');
define('USERS_UPLOAD_PATH', 'users/');
define('LECTURERS_UPLOAD_PATH', 'lecturers/');
define('WORD_UPLOAD_PATH', 'files/file_word');
define('SOURCE_CODE_UPLOAD_PATH', 'files/source_code');
define('NEWS_UPLOAD_PATH', 'news/');
//define so luong gui gmail
define("CRONJOB_SEND_MAIL_USER_LIMIT",4);
