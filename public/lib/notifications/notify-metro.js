$.notify.addStyle("metro", {
    html:
        "<div>" +
            "<div class='image' data-notify-html='image'/>" +
            "<div class='text-wrapper'>" +
                "<div class='title' data-notify-html='title'/>" +
                "<div class='text' data-notify-html='text'/>" +
            "</div>" +
        "</div>",
    classes: {
        default: {
            "color": "#ffffff !important",
            "background-color": "#3f51b5",
            "border": "1px solid #3f51b5"
        },
        error: {
            "color": "#ffffff !important",
            "background-color": "#ee6e73",
            "border": "1px solid #ee6e73"
        },
        custom: {
            "color": "#ffffff !important",
            "background-color": "#3f51b5",
            "border": "1px solid #3f51b5"
        },
        success: {
            "color": "#ffffff !important",
            "background-color": "#66bb6a",
            "border": "1px solid #66bb6a"
        },
        info: {
            "color": "#ffffff !important",
            "background-color": "#29b6f6",
            "border": "1px solid #29b6f6"
        },
        warning: {
            "color": "#ffffff !important",
            "background-color": "#ffc107",
            "border": "1px solid #ffc107"
        },
        black: {
            "color": "#ffffff !important",
            "background-color": "#4c5667",
            "border": "1px solid #4c5667"
        },
        white: {
            "background-color": "#ffffff",
            "border": "1px solid #ddd"
        }
    }
});

!function($) {
    "use strict";

    var Notification = function() {};

    //simple notificaiton
    Notification.prototype.notify = function(style,position, title, text) {
        var icon = 'fa fa-adjust';
        if(style == "error"){
            icon = "fa fa-exclamation";
        }else if(style == "warning"){
            icon = "fa fa-warning";
        }else if(style == "success"){
            icon = "fa fa-check";
        }else if(style == "custom"){
            icon = "fa fa-free-code-camp";
        }else if(style == "info"){
            icon = "fa fa-question";
        }else{
            icon = "fa fa-adjust";
        }
        $.notify({
            title: title,
            text: text,
            image: "<i class='"+icon+"'></i>"
        }, {
            style: 'metro',
            className: style,
            globalPosition:position,
            showAnimation: "slideDown",
            showDuration: 400,
            hideDuration: 200,
            autoHide: true,
            clickToHide: true
        });
    },

    //auto hide notification
    Notification.prototype.autoHideNotify = function (style,position, title, text) {
        var icon = "fa fa-adjust";
        if(style == "error"){
            icon = "fa fa-exclamation";
        }else if(style == "warning"){
            icon = "fa fa-warning";
        }else if(style == "success"){
            icon = "fa fa-check";
        }else if(style == "custom"){
            icon = "fa fa-free-code-camp";
        }else if(style == "info"){
            icon = "fa fa-question";
        }else{
            icon = "fa fa-adjust";
        }
        $.notify({
            title: title,
            text: text,
            image: "<i class='"+icon+"'></i>"
        }, {
            style: 'metro',
            className: style,
            globalPosition:position,
            showAnimation: "slideDown",
            showDuration: 400,
            hideDuration: 200,
            autoHideDelay: 50000,
            autoHide: true,
            clickToHide: true
        });
    },
    //confirmation notification
    Notification.prototype.confirm = function(style,position, title) {
        var icon = "fa fa-adjust";
        if(style == "error"){
            icon = "fa fa-exclamation";
        }else if(style == "warning"){
            icon = "fa fa-warning";
        }else if(style == "success"){
            icon = "fa fa-check";
        }else if(style == "custom"){
            icon = "fa fa-free-code-camp";
        }else if(style == "info"){
            icon = "fa fa-question";
        }else{
            icon = "fa fa-adjust";
        }
        $.notify({
            title: title,
            text: 'Are you sure you want to do nothing?<div class="clearfix"></div><br><a class="btn btn-sm btn-white yes">Yes</a> <a class="btn btn-sm btn-danger no">No</a>',
            image: "<i class='"+icon+"'></i>"
        }, {
            style: 'metro',
            className: style,
            globalPosition:position,
            showAnimation: "slideDown",
            showDuration: 400,
            hideDuration: 200,
            autoHide: false,
            clickToHide: false
        });
        //listen for click events from this style
        $(document).on('click', '.notifyjs-metro-base .no', function() {
          //programmatically trigger propogating hide event
          $(this).trigger('notify-hide');
        });
        $(document).on('click', '.notifyjs-metro-base .yes', function() {
          //show button text
          alert($(this).text() + " clicked!");
          //hide notification
          $(this).trigger('notify-hide');
        });
    },
    //init - examples
    Notification.prototype.init = function() {

    },
    //init
    $.Notification = new Notification, $.Notification.Constructor = Notification;
}(window.jQuery);