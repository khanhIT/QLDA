
//show add image
 var openFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
        var dataURL = reader.result;
        var output = document.getElementById('output');
        output.src = dataURL;
    };
    reader.onload = function (e) {
        $('#output')
            .attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
};



//edit image
var showFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
        var dataURL = reader.result;
        var output = document.getElementById('output');
        output.src = dataURL;
    };
    reader.onload = function (e) {
        $('#image')
            .attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
};


//news edit image
 var openFileContent = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
        var dataURL = reader.result;
        var output = document.getElementById('output');
        output.src = dataURL;
    };
    reader.onload = function (e) {
        $('#image_content')
            .attr('src', e.target.result)
            .width(570)
            .height(200);
    };
    reader.readAsDataURL(input.files[0]);
};


//tinymce

tinymce.init({ 
  selector:'textarea',
  height: 200,
  theme: 'modern',
  plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]

});


//xem file word đồ án
function viewCV(classShow, _this) {
    if ($(_this).hasClass('viewding') === true) {
        $(classShow).hide();
        $(_this).html('Xem file');
        $(_this).removeClass("viewding");
    } else {
        $(classShow).show();
        $(_this).html('Đóng');
        $(_this).addClass("viewding");
    }
}