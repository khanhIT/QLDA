var pageLength = 10;
var language = {
        processing: "Đang xử lý ...",
        previous: "",
        emptyTable: '',
        infoEmpty: "",
        zeroRecords: "",
        info: "",
        infoFiltered: "",
        paginate: {
            first: "first",
            last: "last",
            next: "next",
            previous: "previous"
        }
    };
   $('#data-table2').dataTable({
    "buttons": [
           'copy', 'excel', 'pdf'
       ],

    "columnDefs": [ {
        "targets": 'no-sorting',
        "orderable": false
    } ],
    'paging': true,
    'lengthChange': false,
    "lengthMenu": [30],
    'searching': true,
    'language': language,
    'ordering': true,
    'info': true,
    'autoWidth': false,
    'pageLength': pageLength,
    "language": {
        "search": "Tìm Kiếm",
        "searchPlaceholder":"Tìm Kiếm...."
    }
    
});